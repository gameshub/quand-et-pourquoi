// constantes couleurs et noms de calendriers
const WITHE_COLOR = 0xffffff;
const GREEN_COLOR = 0x67eb34;
const BLUE_COLOR = 0x007bff;
const GREY_COLOR = 0x808080;
const GRID3DAYS = "grid3days";
const GRID1WEEK = "grid1week";
const GRID2WEEKS = "grid2weeks";
const GRID4WEEKS = "grid4weeks";



// référence au json
const nameBtnThree = "btnThreeDays";
const nameBtnOneWeek = "btnOneWeek";
const nameBtnTwoWeeks = "btnTwoWeeks";
const nameBtnOneMonth = "btnOneMonth";
const nameBtnValider = "btnValider"
const btnHarmosLess = "btnHarmosLess"
const btnHarmosMore = "btnHarmosMore";
const BTNHeight = 35;
const isDev = true;

//const nameBtnOneWeek ="";
//const nameBtnOneWeek
class Create extends Interface {
    constructor(refGame) {
        super(refGame, "create");
    }

    /**
     * cette méthode initialise les variables globales
     */
    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        //========================================================================================================
        //********************************************************************************************************
        //********************************************************************************************************
        //********************************************************************************************************
        //========================================================================================================
        for (let i = 0; i < this.elements.length; i++) {
            if (typeof this.elements[i].origin !== "undefined") {
                this.elements[i].reset();
            }
        }
        /*=========================================*/
        //var Importante//
        //ces variables vont nous servir aau formattagqe JSON
        this.spriteToMoveToJson;
        this.elementsInTabToJson
        this.difficultyToJson;

        this.nbWeek;
        this.levelHarmos;
        this.imgCalendar;
        this.imggreen;
        //liste des évènements qu'on met dans le tableau (Sans justification)

        //liste des justification, des phrase d'indice ainsi que de la forme correspondante

        this.imgEventXAndY = [];
        this.listJustification = []
        this.mainSentence = []
        this.mapJustification = new Map();
        /*=========================================*/
        //liste de toutes activité du jeu
        let ybtnFirstPage = 50
        this.rendom = true;
        this.listEvent = [];
        this.img = new Shape();
        this.index = -1;
        this.indexImg = -1;
        this.indexHarmos = 0;
        this.elements = [];
        this.cells = [];
        this.imggreen = null;
        this.correctionX = 0;
        this.correctionY = 0
        this.eventNb = 0;
        this.eventNbJustification = 0;
        this.exercice = this.refGame.global.resources.getScenario();
        this.exercice = JSON.parse(this.exercice);
        this.listHarmos = [];
        this.harmosText = " Harmos";
        for (let i = 4; i <= 6; i++) {
            let txt = i + " Harmos";
            this.listHarmos.push(new String(txt));
        }
        this.cbxRendomEvent = new CheckBox(400, 530, "aléatoire", 1);
        this.cbxRendomEvent.select(false);
        let txtCreatMode = new PIXI.Text(this.refGame.global.resources.getOtherText('creatMode'), {
            // fontFamily: 'Arial',
            fontSize: 24,
            fill: 0x000000,
            align: 'left',
            wordWrap: true,
            wordWrapWidth: 580,
            fontWeight: "bolder"
        });
        txtCreatMode.x = 135;
        txtCreatMode.y = 150;
        this.elements.push(txtCreatMode)
        this.refGame.global.util.hideTutorialInputs('btnResetTr', 'btnAudioGame', 'next', 'btnReset');
        //la ligne si dessous doit être documenté pour le problème 4
        //this.cmboxHarmos =  new ComboBox(450, 450,this.listHarmos);
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial1'))
        //this.refGame.global.util.hideTutorialInputs(this.refGame.global.resources.getOtherText('trainMode'))
        this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('btncreat'), BLUE_COLOR, WITHE_COLOR, true);
        this.startButton.setOnClick(this.chooseHarmosAndCalendar.bind(this));
        this.elements.push(this.startButton);
        //this.nextHarmos = new Button(400, 50, this.refGame.global.resources.getOtherText(btnHarmosMore), BLUE_COLOR, WITHE_COLOR, true, 35);
        //this.nextHarmos.setOnClick(this.nextHarmosFun.bind(this));
        //this.nextHarmos.setOnClick(this.chooseHarmosAndCalendar.bind());
        //this.previousHarmos = new Button(200, 50, this.refGame.global.resources.getOtherText(btnHarmosLess), BLUE_COLOR, WITHE_COLOR, true, 35);
        //this.previousHarmos.setOnClick(this.previousHarmosFun.bind(this));

        //btn pour le calendrier
        this.treeDays = new Button(150, ybtnFirstPage, this.refGame.global.resources.getOtherText(nameBtnThree), BLUE_COLOR, WITHE_COLOR, true, 50);
        this.treeDays.setOnClick(this.changeImgCalendar.bind(this, 0));

        this.oneWeek = new Button(250, ybtnFirstPage, this.refGame.global.resources.getOtherText(nameBtnOneWeek), BLUE_COLOR, WITHE_COLOR, true, 50);
        this.oneWeek.setOnClick(this.changeImgCalendar.bind(this, 1));

        this.towWeeks = new Button(400, ybtnFirstPage, this.refGame.global.resources.getOtherText(nameBtnTwoWeeks), BLUE_COLOR, WITHE_COLOR, true, 50);
        this.towWeeks.setOnClick(this.changeImgCalendar.bind(this, 2));

        this.oneMonth = new Button(500, ybtnFirstPage, this.refGame.global.resources.getOtherText(nameBtnOneMonth), BLUE_COLOR, WITHE_COLOR, true, 50);
        this.oneMonth.setOnClick(this.changeImgCalendar.bind(this, 3));

        this.btnValidation = new Button(300, 550, this.refGame.global.resources.getOtherText(nameBtnValider), BLUE_COLOR, WITHE_COLOR, true);
        this.btnValidation.setHeight(35);
        this.btnRetour = new Button(250, 550, this.refGame.global.resources.getOtherText(nameBtnValider), BLUE_COLOR, WITHE_COLOR, true);
        this.btnRetour.setHeight(35);
        //this.btnValidation.setWidth(150)
        this.btnValidation.setOnClick(this.validateHarmosAndCalendar.bind(this));
        //btn start
        this.refGame.global.util.showTutorialInputs("startBtn");
        this.texteHarmos = new PIXI.Text(this.listHarmos[0], {
            // fontFamily: 'Arial',
            fontSize: 16,
            fill: 0x000000,
            align: 'left',
            wordWrap: true,
            wordWrapWidth: 580,
            fontWeight: "bolder"
        });
        this.texteHarmos.x = 268;
        this.texteHarmos.y = 45;
        //========================================================================================================
        //********************************************************************************************************
        //********************************************************************************************************
        //========================================================================================================
        //récupération de toutes les images
        this.getAllEventFromDB();
        this.init();

        //this.clear();
    }

    /**
     * cette méthode va afficher les boutons
     */
    chooseHarmosAndCalendar() {
        this.clear();
        this.elements = []
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial1'));

        //this.elements.push(this.formatteTxt(this.listHarmos[0]))
        this.elements.push(this.treeDays);
        this.elements.push(this.oneWeek);
        this.elements.push(this.towWeeks);
        this.elements.push(this.oneMonth);
        //this.elements.push(this.texteHarmos);
        this.elements.push(this.btnValidation);
        this.init();
        this.changeImgCalendar(0);

    }

    nextHarmosFun(e) {
        this.indexHarmos++;
        this.changeTxtHarmos();
    }

    previousHarmosFun(e) {
        this.indexHarmos--;
        this.changeTxtHarmos();
    }

    /**
     * Cette méthode va changer le texte du label pour la séléction du niveau Harmos
     */
    changeTxtHarmos() {
        if (this.indexHarmos < 0) {
            //on recommence à la dernière case
            this.indexHarmos = 0
        } else if (this.indexHarmos == this.listHarmos.length) {
            //on recommence à 0
            this.indexHarmos = this.listHarmos.length - 1;
        }

        this.levelHarmos = this.listHarmos[this.indexHarmos];
        this.texteHarmos.text = this.listHarmos[this.indexHarmos];
        this.elements.push(this.texteHarmos);
        this.init();
    }


    /**
     * Cette méthode va changer l'image du calendrier sélectionné
     * @param {int} imgCalendar est un numéro indiquant le calendrier choisi
     */
    changeImgCalendar(imgCalendar) {
        /*
        calendarImg nous permttra de choisir l'image.
        0 = 3 jours
        1 = 1 semaine
        2 = 2 semaines
        3 = 1 mois (4 semaines)
        */
        this.treeDays.setbgcolor(BLUE_COLOR);
        this.oneWeek.setbgcolor(BLUE_COLOR);
        this.towWeeks.setbgcolor(BLUE_COLOR);
        this.oneMonth.setbgcolor(BLUE_COLOR);
        let imgToChoose = "";
        switch (imgCalendar) {
            case 0:
                imgToChoose = GRID3DAYS;
                this.treeDays.setbgcolor(GREEN_COLOR);
                break;
            case 1:
                imgToChoose = GRID1WEEK;
                this.oneWeek.setbgcolor(GREEN_COLOR);
                this.correctionY = -6;
                break;
            case 2:
                imgToChoose = GRID2WEEKS;
                this.towWeeks.setbgcolor(GREEN_COLOR);
                this.correctionY = -6;
                break;
            case 3:
                imgToChoose = GRID4WEEKS;
                this.oneMonth.setbgcolor(GREEN_COLOR);
                this.correctionY = -2;
                break;
            default:
        }
        let x = 0;
        let y = 0;
        let i = 1;
        this.exercice = this.refGame.global.resources.getScenario();
        this.exercice = JSON.parse(this.exercice);
        for (let key in this.exercice.creat.grid) {
            if (this.exercice.creat.grid[i]) {
                if (this.exercice.creat.grid[i].data.name == imgToChoose) {
                    x = this.exercice.creat.grid[i].data.x;
                    y = this.exercice.creat.grid[i].data.y;
                    break;
                }
            }
            i++;
        }
        //réduire la taille de l'image 

        this.nbWeek = imgToChoose;
        if (imgToChoose === GRID4WEEKS) {
            x = x / 1.5;
            y = y / 1.5;

        } else {
            x = x * 1.188;
            y = y * 1.188;
        }
        let placeXY = 300
        this.img.setImage(this.refGame.global.resources.getImage(imgToChoose));
        this.img.setWidth(x);
        this.img.setHeight(y);
        this.img.setX(placeXY);
        this.img.setY(placeXY);
        this.elements.push(this.img);
        this.init();
    }

    /**
     * cette méthode va remplacer les élements du canvas par l'image du calendrier.
     */
    validateHarmosAndCalendar() {
        if (this.nbWeek) {
            this.clear();
            this.elements = [];
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial2'));
            /* if(isDev){
                console.log(this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial2')));
            } */
            this.btnValidation.setOnClick(this.validateCurrentTime.bind(this));
            this.elements.push(this.btnValidation);

            this.exercice = this.refGame.global.resources.getScenario();
            this.exercice = JSON.parse(this.exercice);
            /* if(isDev){
                console.log(JSON.parse(this.exercice));
            } */

            let count = 1;
            for (let key in this.exercice.creat.grid) {
                if (this.exercice.creat.grid[count].data) {
                    if (this.exercice.creat.grid[count].data.name == this.nbWeek) {
                        this.currentGrid = this.exercice.creat.grid[count];
                        console.log(this.currentGrid);
                        break;
                    }
                }
                count++
            }

            if (this.nbWeek == GRID4WEEKS) {
                this.btnValidationX = 500;
                this.btnValidationY = 300;


                this.btnValidation.setX(this.btnValidationX);
                this.btnValidation.setY(this.btnValidationY);
            } else {
                this.btnValidationX = 300;
                this.btnValidationY = 550;
                this.btnValidation.setX(this.btnValidationX);
                this.btnValidation.setY(this.btnValidationY);
            }

            this.backImg = this.refGame.global.resources.getImage(this.currentGrid.data.name).image.src;
            this.insererBack = new ImgBack(this.currentGrid.data.locationx, this.currentGrid.data.locationy, this.currentGrid.data.x, this.currentGrid.data.y, this.backImg, 'background');
            this.elements.push(this.insererBack);
            this.putButton();
            if (isDev) {
                console.log(this.backImg);
            }
        }
    }

    /**
     * cette méthode va initaliser les boutons sur le quadrillage.
     */
    putButton() {
        let i = 1;
        let size = 24;
        let sizeMonth = 18;
        let autofit = false;
        let label = null;
        let magnets = this.currentGrid.magnets;

        for (let key in magnets) {
            //on retire 10 au x et y pour mieux centrer le bouton
            let caseToPress = new Button(magnets[i].x - this.correctionX, magnets[i].y, label, WITHE_COLOR, WITHE_COLOR, autofit);
            //let caseToPress = new Button(magnets[i].x , magnets[i].y  , label, WITHE_COLOR, WITHE_COLOR, autofit);

            //Si l'image vert a déja été posé on place les btns pour la 2eme étape 
            if (this.imggreen) {
                //si les x et y sont les mêmes 
                //if (magnets[i].x == this.imggreen.getX() && magnets[i].y == this.imggreen.getY() + this.correctionY) {
                if (magnets[i].x - this.correctionX == this.imggreen.getX() && magnets[i].y - this.correctionY == this.imggreen.getY()) {
                    caseToPress = null;
                } else {
                    caseToPress.setOnClick(this.popupEvent.bind(this, caseToPress));
                }
                //Si l'image vert n'a pas été posé on place les btn pour la 1er étape
            } else {
                caseToPress.setOnClick(this.putCurrentTime.bind(this, caseToPress));
            }

            //on set des données générale du bouton (taille)
            if (caseToPress) {
                caseToPress.setAlpha(0);
                caseToPress.setWidth(size + 6);
                caseToPress.setHeight(size);
                this.cells.push(caseToPress);
                this.elements.push(caseToPress);
            }

            i++;
        }
        //on charge les modifications
        this.init();
    }

    /**
     * cette méthode va placer l'image du temps présent (verte) dans la case que le joueur a sélectionnée.
     * @param {shape} e
     */
    putCurrentTime(e) {
        let x = e.x;
        let y = e.y;
        let size = 32;
        if (this.currentGrid.data.name == GRID4WEEKS) {
            size = 32;
        } else if (this.currentGrid.data.name == GRID2WEEKS) {
            size = 37;
        } else if (this.currentGrid.data.name == GRID1WEEK) {
            size = 47;
        } else if (this.currentGrid.data.name == GRID3DAYS) {
            size = 45;
        }
        if (!this.imggreen) {
            //création de l'image
            //this.imggreen = new Shape(x + this.correctionX, y, size, size, 1, this.refGame.global.resources.getImage(this.exercice.creat.currentTime), "greenFace", 1, true);

            this.imggreen = new Shape(x, y, size, size, 1, this.refGame.global.resources.getImage(this.exercice.creat.currentTime), "greenFace", 1, true);
        } else {
            //modification de x et y
            this.imggreen.setX(x);
            this.imggreen.setY(y)
        }
        this.elements.push(this.imggreen);
        this.init();
    }

    /**
     * Cette méthode va nous faire passer à la page suivante. En retirant les élements du tableau.
     */
    validateCurrentTime() {
        if (this.imggreen) {

            this.refGame.global.util.hideTutorialInputs('btnValider');

            if (this.nbWeek == GRID4WEEKS) {
                this.btnValidationX = 500;
                this.btnValidationY = 300;


                this.btnValidation.setX(this.btnValidationX);
                this.btnValidation.setY(this.btnValidationY);
            } else {
                this.btnValidationX = 300;
                this.btnValidationY = 550;
                this.btnValidation.setX(this.btnValidationX);
                this.btnValidation.setY(this.btnValidationY);
            }


            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial3'));
            this.clear();
            this.elements = [];

            this.elements.push(this.insererBack);
            this.elements.push(this.imggreen)
            this.btnValidation.setX(this.btnValidationX)
            this.btnValidation.setY(this.btnValidationY)
            this.btnValidation.setVisible(true);

            this.btnValidation.setOnClick(this.validateEventPlace.bind(this));

            this.elements.push(this.btnValidation);
            this.elements.push(this.cbxRendomEvent);

            let i = 0;
            //on parcourt la liste des activités
            for (let key in this.imgEventXAndY) {
                let tempShape = this.imgEventXAndY[i];

                //on leur ajoute un event


                this.elements.push(tempShape)
                i++;
            }


            this.init();
            this.putButton();
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'Attention',
                text: 'tu dois placer le moment',
                confirmButtonText: "Ok"
                //footer: '<a href="">Why do I have this issue?</a>'
            })
        }

    }


    /**
     * Cette méthode va placer les "formes" dans le tableau et y assigner un event.
     */
    setShape() {
        let i = 1;
        let magnets = this.currentGrid.magnets;
        let tailleSprite;
         if (this.currentGrid.data.name == GRID4WEEKS) {
            tailleSprite = 32;
        } else if (this.currentGrid.data.name == GRID2WEEKS) {
            tailleSprite = 37;
        } else if (this.currentGrid.data.name == GRID1WEEK) {
            tailleSprite = 47;
        } else if (this.currentGrid.data.name == GRID3DAYS) {
            tailleSprite = 45;
        }
        for (let key in magnets) {
            let shapeToPress
            if (magnets[i].x != this.imggreen.getX() && magnets[i].y != this.imggreen.getY) {
                console.log(magnets[i].x);
                console.log(magnets[i].y);
                shapeToPress = new Shape(magnets[i].x, magnets[i].y, tailleSprite, tailleSprite, 1, null, null, 1, true);
                shapeToPress.setOnClick(this.popupEvent.bind(this, shapeToPress));
                shapeToPress.setImage(this.refGame.global.resources.getImage(this.exercice.creat.currentTime))
                this.cells.push(shapeToPress);
                this.elements.push(shapeToPress);
            }
            i++;
        }
        this.init();
    }

    /**
     * cette méthode va récupérer toutes les images des activités dans la db.
     */
    getAllEventFromDB() {
        //récupération des images 
        let count = 0;
        this.exercice = this.refGame.global.resources.getScenario();
        this.exercice = JSON.parse(this.exercice);
        //est-ce vraiment util ?? je ne le sais pas !
        for (let key in this.exercice.creat.events) {
            this.listEvent.push(this.exercice.creat.events[count]);
            count++;
        }
    }


    /**
     * cette méthode va générer la popup avec laquelle l'utilisateur va choisir le type d'activité.
     * Si il y a déja une forme dans la case elle va suprimer l'activitée.
     * Et si la case aléatoire est checkée alors on prend une activité aléatoire.
     * @param {shape} e
     */
    async popupEvent(e) { 

        let sameId = 0;




        let sameEvent = false;
        let i = 0;
        let eX = 0;
        let eY = 0;


        for (let key in this.imgEventXAndY) {
            console.log(key);

            let tempShape = this.imgEventXAndY[i];


            if ((e.getX() == tempShape.x) && (e.getY() == (tempShape.y))) {
                sameEvent = true;
                sameId = i;
                eX = e.getX();
                eY = e.getY();
                break;
            }
            i++;

        }


        if (!sameEvent) {
            let i = 0;


            if (!this.cbxRendomEvent.isChecked()) {
                const { value: logoselected } = await Swal.fire({
                    title: "Sélectionnez un évenement.",
                    html: this.addLogo(),
                    width: 500,
                    focusConfirm: false,
                    preConfirm: () => {
                        if (document.querySelector("input[name='logo']:checked") !== null) {
                            return [
                                document.querySelector("input[name='logo']:checked").id
                            ]
                        } else {
                            return [null]
                        }
                    }
                });



                if (logoselected != "") {
                    let size;
                     if (this.currentGrid.data.name == GRID4WEEKS) {
                        size = 32;
                    } else if (this.currentGrid.data.name == GRID2WEEKS) {
                        size = 37;
                    } else if (this.currentGrid.data.name == GRID1WEEK) {
                        size = 47;
                    } else if (this.currentGrid.data.name == GRID3DAYS) {
                        size = 45;
                    }
                    let count = 0;
                    let htmlToInsertInPopup;
                    let btnx = e.getX();
                    let btny = e.getY();
                    console.log(btnx);
                    console.log(btny);

                    let shape = new Shape();
                    let that = this;
                    let select;
                    let icons = [];
                    let name = logoselected;

                    shape.setImage(that.refGame.global.resources.getImage(name));
                    shape.setWidth(size);
                    shape.setHeight(size);
                    //shape.setX(btnx);
                    //shape.setY(btny);
                    shape.setX(btnx);
                    shape.setY(btny);
                    //shape.setX(btnx);
                    //shape.setY(btny);
                    shape.setName(name);
                    //on ajoute la forme dans le tableau
                    this.imgEventXAndY.push(shape);
                    that.eventNb++;
                    that.elements.push(shape);
                    that.init();
                }


            } else {
                console.log("bonjour monsieur");
                let sizeSiAlea;
                if (this.currentGrid.data.name == GRID4WEEKS) {
                    sizeSiAlea = 32;
                } else if (this.currentGrid.data.name == GRID2WEEKS) {
                    sizeSiAlea = 37;
                } else if (this.currentGrid.data.name == GRID1WEEK) {
                    sizeSiAlea = 47;
                } else if (this.currentGrid.data.name == GRID3DAYS) {
                    sizeSiAlea = 45;
                }

                let count = 0;
                let htmlToInsertInPopup;
                let btnx = e.getX();
                let btny = e.getY();
                console.log(btnx);
                console.log(btny);

                let shape = new Shape();
                let that = this;
                let select;
                let icons = [];
                let name = this.rendomEvent();
                console.log(name);

                shape.setImage(that.refGame.global.resources.getImage(name));
                shape.setWidth(sizeSiAlea);
                shape.setHeight(sizeSiAlea);
                //shape.setX(btnx);
                //shape.setY(btny);
                shape.setX(btnx);
                shape.setY(btny);
                //shape.setX(btnx);
                //shape.setY(btny);
                shape.setName(name);
                //on ajoute la forme dans le tableau
                this.imgEventXAndY.push(shape);
                that.eventNb++;
                that.elements.push(shape);
                that.init();
            }

            this.putButton();
        } else {
            this.removeEvent(sameId, eX, eY, 0);
        }
    }


    addLogo() {
        var nbreImageLigne = 3;
        var div = '<div style="overflow:scroll; overflow-x: hidden;  display: block; width: 100%; height:500px; border:#000000 1px solid;">';
        let nbreImage = 0;

        const organisedItems = []
        for (let i = 0; i < this.listEvent.length; i++) {
            organisedItems.push(
                {
                    'name': this.refGame.global.resources.getOtherText(this.listEvent[i]),
                    'key': this.listEvent[i],
                    'value': this.refGame.global.resources.getImage(this.listEvent[i])
                })
        }
        organisedItems.sort((a, b) => a.name.localeCompare(b.name))

        for (let i = 0; i < organisedItems.length; i++) {
            if (nbreImage % nbreImageLigne === 0) {
                div += '<div style="display: flex;"><div style="display: inline;width:30%;height:80px"><img src="' + organisedItems[i].value.image.currentSrc + ' " /></br><span>' + organisedItems[i].name + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div>';
            } else if (nbreImage % nbreImageLigne === nbreImageLigne - 1) {
                div += '<div style="display: inline;width:30%;height:80px"><img src="' + organisedItems[i].value.image.currentSrc + '"/></br><span>' + organisedItems[i].name + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div></div></br>';
            } else {
                div += '<div style="display: inline;width:30%;height:80px"><img src="' + organisedItems[i].value.image.currentSrc + '"/></br><span>' + organisedItems[i].name + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div>';
            }
            nbreImage++;
        }
        div += '</div></div>';

        return div;
    }

    /**
     * Permet de supprimer une activité
     * @param idEvent id de l'activité dans le tableau des activité
     * @param shapeX position en x de la case cliquée
     * @param shapeY position en Y de la case cliquée
     * @param tableDiff différence en Y entre l'activité et la case du tableau
     */
    removeEvent(idEvent, shapeX, shapeY, tableDiff) {
        this.imgEventXAndY.splice(idEvent, 1);
        this.elements.splice(idEvent, 1);
        let a = 0;
        this.clear();
        this.elements = [];
        this.elements.push(this.insererBack);
        this.elements.push(this.imggreen);
        this.btnValidation.setX(this.validerX);
        this.btnValidation.setY(this.validerY);
        this.btnRetour = new Button(this.btnretourX, this.btnretourY, "retour", GREY_COLOR, WITHE_COLOR, true);
        this.btnRetour.setHeight(BTNHeight);
        for (let key in this.imgEventXAndY) {
            let tempShape = this.imgEventXAndY[a];
            this.elements.push(tempShape);
            a++;
            if (!this.listJustification.length == 0) {
                let i = 0;
                for (let key in this.listJustification) {
                    let justX = this.listJustification[i].shape.getX();
                    let justY = this.listJustification[i].shape.getY();
                    if ((justX == shapeX) && (justY + tableDiff == shapeY)) {
                        this.listJustification.splice(i, 1);
                        this.eventNbJustification--;
                    }
                    i++;
                }
            }
        }
        this.btnValidation.setX(this.btnValidationX)
        this.btnValidation.setY(this.btnValidationY)
        this.btnValidation.setVisible(true);
        this.btnValidation.setOnClick(this.validateEventPlace.bind(this));
        this.elements.push(this.btnValidation);
        this.elements.push(this.cbxRendomEvent);
        this.init();
        this.putButton();
    }

    /**
     *  Cette méthode va  nous faire passer à la page suivante en retirant les élements du tableau.
     */
    validateEventPlace() {
        if (this.eventNb != 0) {
            this.clear();
            this.elements = [];

            if (this.nbWeek == GRID4WEEKS) {
                this.validerX = 500;
                this.validerY = 200;

                this.btnretourX = 500;
                this.btnretourY = 250;

            } else {
                this.validerX = 400;
                this.validerY = 550;

                this.btnretourX = 150;
                this.btnretourY = 550;

            }


            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial4'));
            this.btnValidation.setText("valider");
            if (isDev){
                console.log(this.insererBack);
                console.log(this.imggreen);
            }
            this.elements.push(this.insererBack);
            this.elements.push(this.imggreen);
            this.btnValidation.setX(this.validerX);
            this.btnValidation.setY(this.validerY);


            this.btnRetour = new Button(this.btnretourX, this.btnretourY, "retour", GREY_COLOR, WITHE_COLOR, true);
            this.btnRetour.setHeight(35);


            this.btnRetour.setOnClick(this.validateCurrentTime.bind(this));
            this.btnRetour.setVisible(true);

            this.elements.push(this.btnRetour);


            this.btnValidation.setVisible(true);
            this.btnValidation.setOnClick(this.formatToJson.bind(this));
            let i = 0;
            //on parcours la liste des activités
            for (let key in this.imgEventXAndY) {
                let tempShape = this.imgEventXAndY[i];
                //on leur ajoute un event
                tempShape.setOnClick(this.addJustification.bind(this, tempShape));
                this.elements.push(tempShape)
                i++;
            }

            this.elements.push(this.btnValidation);

            this.init();
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'Attention',
                text: 'tu dois placer au moins une activit\u00e9',
                confirmButtonText: "Ok"
                //footer: '<a href="">Why do I have this issue?</a>'
            })
        }
    }

    /**
     * Cette méthode crée une popup dans laquelle l'utilisateur entrera les justifications.
     * @param {shape} e
     */
    addJustification(e) {
        //if (this.eventNbJustification < 5) {

        let that = this;
        let indexShapeGlobal = 0;

        let i = 0;
        let ok = false;
        let isAlreadyJust = false;
        let tempShape;
        let tempList = []
        let ms;
        //on regarde si la forme a déja été justifier
        for (let key in this.listJustification) {
            //on récupère les coordonnées
            let x = this.listJustification[i].shape.getX();
            let y = this.listJustification[i].shape.getY();
            if (y == e.getY() && x == e.getX()) {
                if (this.listJustification[i].data) {
                    isAlreadyJust = true;
                    indexShapeGlobal = i;
                    break;
                }
            }
            i++;
        }
        let htmlToInsertInPopup = `<div> <h3>` + this.refGame.global.resources.getOtherText("popupJustification1") + `</h3><br>  `;
        if (!isAlreadyJust) {
            htmlToInsertInPopup +=
                `<div id="all">
                <div id = "top-pop-pu">
                <label for="fname">` + this.refGame.global.resources.getOtherText("popupJustification2") + `</label><br>
                <input type="text" id="mainSentence"  style="width: 450px"  name="fname"><br>`;

            htmlToInsertInPopup += `<div style="text-align: left; margin-top: 5px" class="position-relative"><br>` + this.refGame.global.resources.getOtherText("popupJustification3") + `<br>`

            for (let i = 0; i < 4; i++) {
                htmlToInsertInPopup += `<input type="radio" class="radiobtn"id="input_btn_rd` + i + `" name="validate_" value="op1"> <input type="text" id="input_txt_` + i + `" name="fname" value=""><br><br>`
            }

            htmlToInsertInPopup += `</div>`
        } else {
            htmlToInsertInPopup +=
                `<div id="all">
                    <div id = "top-pop-pu">
                        <label for="fname">` + this.refGame.global.resources.getOtherText("popupJustification2") + `</label><br>
                        <input type="text" id="mainSentence" style="width: 450px" name="fname" value="` + this.listJustification[indexShapeGlobal].mainSentence + `"><br>
                `;
            htmlToInsertInPopup += `<div style="text-align: left; margin-top: 5px" class="position-relative"><br>` + this.refGame.global.resources.getOtherText("popupJustification3") + `<br>`


            for (let i = 0; i < 4; i++) {
                if (this.listJustification[indexShapeGlobal].data[i].isTrue) {
                    htmlToInsertInPopup += `<input type="radio" class="radiobtn"id="input_btn_rd` + i + `" name="validate_" value="op1" checked>`
                } else {
                    htmlToInsertInPopup += `<input type="radio" class="radiobtn"id="input_btn_rd` + i + `" name="validate_" value="op1">`
                }
                htmlToInsertInPopup += `
                        <input type="text" id="input_txt_` + i + `" name="fname" value="` + this.listJustification[indexShapeGlobal].data[i].sentence + `"> <br><br>`
            }
            htmlToInsertInPopup += `</div>`
        }
        Swal.fire({
            confirmButtonText: 'Validation',
            showCancelButton: true,
            showConfirmButton: true,
            html:
                htmlToInsertInPopup + `
                    </div>
                </div>`,
            preConfirm: (login) => {
                //on parcours pour savoir si un des 4 btn à été coché
                for (let i = 0; i < 4; i++) {
                    if ($('#input_btn_rd' + i).is(':checked')) {
                        ok = true;

                    }
                }
                if (!ok) {
                    Swal.showValidationMessage(`Attention tu n'as rien s\u00e9lectionn\u00e9 de r\u00e9ponse correcte`);
                }
            }
        }).then((result) => {
            if (result.value) {
                let setence = [];
                let okChamp = true;
                for (let i = 0; i < 4; i++) {
                    let s = {
                        sentence: $('#input_txt_' + i).val(),
                        isTrue: $('#input_btn_rd' + i).is(':checked')
                    }
                    if (!$('#input_txt_' + i).val()) {
                        okChamp = false;
                        console.log("attentionn")
                        break;
                    }


                    setence.push(s);
                }
                let data = {
                    shape: e,
                    mainSentence: $("#mainSentence").val(),
                    data: setence
                }

                //vérification des champs

                //récupération des information
                if (isAlreadyJust) {
                    let j = 0;
                    for (let key in this.listJustification) {
                        if (this.listJustification[j]) {
                            let x = this.listJustification[j].shape.getX();
                            let y = this.listJustification[j].shape.getY();
                            if (y == e.getY() && x == e.getX()) {
                                if (ok) {
                                    this.eventNbJustification++;
                                    this.listJustification.splice(j, 1, data);
                                    break;
                                }
                            }
                            j++;
                        }
                    }
                } else {
                    if (okChamp) {
                        e.setAplha(0.5)
                        this.elements.push(e)
                        this.eventNbJustification++;
                        //console.log("x "+x+" \n y "+ y)
                        this.listJustification.push(data)
                    }
                }
            }
        });

        /*} else {
            Swal.fire({
                icon: 'warning',
                title: 'Attention',
                text: 'tu ne peux pas justifier plus de 5 \u00e9v\u00e9ment',
                confirmButtonText: "Ok"
                //footer: '<a href="">Why do I have this issue?</a>'
            })
        }*/
        this.init();
    }

    /**
     * cette méthode va formater l'exercice et l'envoyer au serveur.
     */
    saveExercice() {
        console.log(this.exercice);
        this.refGame.global.resources.saveExercice(JSON.stringify(this.exercice), function (result) {
            Swal.fire(
                result.message,
                undefined,
                result.type
            );

            this.show();
        }.bind(this));
    }

    /**
     *
     */
    formatToJson() {

        if (this.eventNbJustification == 0) {
            Swal.fire({
                icon: 'warning',
                title: 'Attention',
                text: 'Ton dois justifier au moins une activit\u00e9',
                confirmButtonText: "Ok"
                //footer: '<a href="">Why do I have this issue?</a>'
            })
        } else {
            let eits = this.formatEITSoJson();
            let stm = this.formatSTMToJson();
            let exe = {
                difficulty: {
                    //on récupère tous les infos suivante de la grille courante.
                    locationx: this.currentGrid.data.locationx,
                    locationy: this.currentGrid.data.locationy,
                    name: this.currentGrid.data.name,
                    x: this.currentGrid.data.x,
                    y: this.currentGrid.data.y
                },
                elementsInTab: {
                    // eits = les image par défaut dans le tableau
                    sprites: Object.fromEntries(eits),
                    ///on récupère tous les magnets de la grille courante.
                    magnets: this.currentGrid.magnets
                },
                //stm = les images déplaçables 
                spriteToMove: Object.fromEntries(stm)
            }
            //On converti notre exercice en String
            exe = JSON.stringify(exe);
            this.exercice = exe;
            this.newGame(exe);


        }


    }


    /**
     * cette méthode va formater les élements dans le tableau. elle va se charger des "sprites".
     * @returns un string des élements du tableau.
     */
    formatEITSoJson() {
        //--------------------------------------------
        //                  elementsInTab
        //--------------------------------------------
        let eventNotJust = [];
        //ajout de l'image verte
        eventNotJust.push(this.imggreen)
        //compteur
        let i = 0
        let size = 32;
        //forme temporaire
        let tempShape;
        //Map de images par défaut et des informations
        let sprit = new Map();
        //on récupère toutes les event sans justification
        for (let key in this.imgEventXAndY) {
            let j = 0;
            tempShape = this.imgEventXAndY[i];
            let ok = true;
            for (let key in this.listJustification) {
                //Si les x y sont identique
                if (tempShape.getX() == this.listJustification[j].shape.getX() && tempShape.getY() == this.listJustification[j].shape.getY()) {
                    ok = false;
                    break;
                }
                j++;
            }
            if (ok) {
                eventNotJust.push(tempShape)
            }

            i++;
        }
        //on formatte
        i = 0;

        for (let key in eventNotJust) {

            let data;
            if (eventNotJust[i]) {

                data = {
                    nomFichier: eventNotJust[i].getName().toString(),
                    x: eventNotJust[i].getX().toString(),
                    y: eventNotJust[i].getY().toString(),
                    width: size.toString(),
                    height: size.toString()
                }
                let i2 = i + 1
                //formatter en str
                sprit.set(i2, data);
            }

            i++;
        }
        //console.log(JSON.stringify(Object.fromEntries(sprit)))
        return sprit;
    }

    /**
     * Cette méthode va se charger de formater les "spritetomove"
     * @returns un string de tous les sprites to move
     */
    formatSTMToJson() {

        //--------------------------------------------
        //                  spriteToMove
        //--------------------------------------------
        //index
        let i = 0;
        //forme temporaire
        let tempShape;
        //liste a retourné
        let spriteToMove = new Map();
        //this.listJustification est la liste des formes placées et justifiées
        for (let key in this.listJustification) {
            //phrase d'indice
            let txt = this.listJustification[i].mainSentence;
            //la forme
            tempShape = this.listJustification[i].shape;
            //le nom de la forme
            let sprite2 = tempShape.getName();
            //nouvel index
            let j = 0;
            //liste des réponses 
            let answersData = new Map();
            //data = liste des réponses
            for (let key in this.listJustification[i].data) {
                //on controle bien si les X & Y sont semblables
                if (tempShape.getX() == this.listJustification[i].shape.getX() && tempShape.getY() == this.listJustification[i].shape.getY()) {
                    //on ajout un js object
                    // index commence à 1
                    answersData.set(j + 1, {
                        text: this.listJustification[i].data[j].sentence,
                        ok: this.listJustification[i].data[j].isTrue
                    });
                }
                j++;
            }
            //création de l'objet sprite to move
            let data = {
                sprite: sprite2,
                text: txt,
                popup: {
                    answers: Object.fromEntries(answersData)
                },
                verif: {
                    x: this.listJustification[i].shape.x.toString(),
                    y: this.listJustification[i].shape.y.toString()
                }
            };
            i++;
            spriteToMove.set(i, data);
        }
        return spriteToMove;
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic) {

    }


    rendomEvent() {
        let max = this.listEvent.length;

        let rendom = Math.floor(Math.random() * max);
        return this.listEvent[rendom];


    }


    newGame(exe) {
        this.btnValidation.setVisible(false);
        this.exercice = exe
        this.clear();
        this.imgPositioning = [];
        this.validation = [];

        //MODE TRAIN

        this.refGame.global.util.setTutorialText("Test maintenant ton niveau");

        this.elements.unshift(this.startButton);
        this.startButton.setVisible(false);


        this.refGame.global.util.hideTutorialInputs('btnResetTr');
        //json Sélectionner un exercice sauf celui de base

        //console.log(this.exercice)
        this.exercice = JSON.parse(this.exercice);


        //console.log("__________________________________________________________")

        //insertion du background
        this.back = this.exercice.difficulty;
        this.backImg = this.refGame.global.resources.getImage(this.back.name).image.src;


        let insererBack = new ImgBack(this.back.locationx, this.back.locationy, this.back.x, this.back.y, this.backImg, 'background');
        this.elements.push(insererBack);

        //insertion des images dans le tableau
        var count = 1;
        for (let key in this.exercice.elementsInTab.sprites) {
            var img = this.exercice.elementsInTab.sprites[count];
            if (typeof this.refGame.global.resources.getImage(img.nomFichier).image.src !== "undefined") {
                let imageTab = new ImgBack(img.x, img.y, 37, 37, this.refGame.global.resources.getImage(img.nomFichier).image.src, img.nomFichier);
                //let imageTab = new ImgBack(0, 0, img.width, 32, this.refGame.global.resources.getImage(img.nomFichier).image.src, img.nomFichier);
                this.elements.push(imageTab);
            }
            count++;
        }
        count = 1;
        //this.nbFrome = this.exercice.spriteToMove;
        this.nbFrome = 0;
        for (let key in this.exercice.spriteToMove) {
            this.nbFrome++;
        }
        this.cells = [];
        this.listError = [];
        this.nbBouger = 0;
        //insertion des aimants
        for (let key in this.exercice.elementsInTab.magnets) {
            var magnetInTab = this.exercice.elementsInTab.magnets[count];
            let x = magnetInTab.x;
            let y = magnetInTab.y;
            let isPlaced = false;
            let magnet = new Guide(parseInt(x), parseInt(y), 20, magnetInTab.text,);
            this.cells.push(magnet);
            this.elements.push(magnet);
            count++;
        }

        //Insertion des images que l'utilisateur pourra bouger
        let startXgreen;
        let startYgreen;

        if (this.exercice.difficulty.name == DIFFICULTEHAUTE) {
            var startX = 400;
            var startY = 30;
            startXgreen = 400;
            startYgreen = 40;
            this.correctionY = -2;
        } else if (this.exercice.difficulty.name == "grid2weeks") {
            var startX = 50;
            var startY = 430;
            startXgreen = 50;
            startYgreen = 370;
        } else if (this.exercice.difficulty.name == "grid3days") {
            var startX = 50;
            var startY = 350;
            startXgreen = 50;
            startYgreen = 290;
        } else if (this.exercice.difficulty.name == "grid1week") {
            var startX = 50;
            var startY = 350;
            startXgreen = 50;
            startYgreen = 285;
        }
        count = 1
        //affichage du txt explicatif du
        this.textVerte = new PIXI.Text(txtGreen, {
            // fontFamily: 'Arial',
            fontSize: 18,
            fontWeight: "bolder",
            fill: 0x000000,
            align: 'right',
            //wordWrap: true,
            wordWrapWidth: 380
        });

        this.textVerte.x = startXgreen + 50;
        this.textVerte.y = startYgreen - 60;


        let imggreen = new Shape(startXgreen, startYgreen - 50, 37, 37, count, this.refGame.global.resources.getImage("greenFace"), "greenFace", idOK, true);
        if (this.exercice.difficulty.name == DIFFICULTEHAUTE) {
            //var info = new InfoImg(550, startY + 8, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            var info = new InfoImg(550, 10, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClickGreen.bind());//,count,this.onShapeClick.bind(this)
            this.elements.push(info);
        } else {
            this.elements.push(this.textVerte);
            var info = new InfoImg(550, startYgreen - 50, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClickGreen.bind(this));//,count,this.onShapeClick.bind(this)
            this.elements.push(info);
        }


        let nb = 1
        let startX2 = startX
        let startY2 = startY
        this.elements.push(imggreen);

        for (let key in this.exercice.spriteToMove) {
            var txt = this.exercice.spriteToMove[count].texte;
            var img = this.exercice.spriteToMove[count].sprite;
            var i = 1;
            var idOK = 0;
            //Ici on récupère quelle est la bonne réponse, on va utiliser la variable idOk plus tard, l'hors de la validation de l'exercice

            for (let key in this.exercice.spriteToMove[count].popup.answers) {
                if (this.exercice.spriteToMove[count].popup.answers) {
                    if (this.exercice.spriteToMove[count].popup.answers[i].ok) {
                        idOK = i;
                    }
                    i++;
                }
            }
            //insertion des images qui pourront être bougées
            let imageTab;
            let imageTab2;
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                startX2 = startX2 + 60
                imageTab = new Shape(startX2, startY, 37, 37, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2 = new Shape(startX2, startY, 37, 37, count, this.refGame.global.resources.getImage(img));
                imageTab2.setStopMove(true);

            } else {
                this,
                    imageTab = new Shape(startX2, startY2 + 80, 37, 37, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2 = new Shape(startX2, startY2 + 80, 37, 37, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2.setStopMove(true);
            }
            imageTab2.setAplha(0.3)
            this.elements.push(imageTab2);
            this.elements.push(imageTab);
            //insertion du texte
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                this.texte = new PIXI.Text(txt, {
                    // fontFamily: 'Arial',
                    fontSize: 12,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.texte.x = 100;
                this.texte.y = startY;
                //this.elements.push(this.texte);
            }

            //insertion des "i" a gauche du text / des images
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                var info = new InfoImg(startX2, startY + 30, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            } else {
                var info = new InfoImg(startX2 + 50, startY2 + 80, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            }
            this.elements.push(info);
            //startY = startY + 40;
            startY2 = startY2 + 35
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                if (nb % 8 == 0) {
                    startX2 = startX
                    startY = startY + 70
                }
            } else {
                if (nb % 14 == 0) {
                    startX2 = startX2 + 100
                    startY2 = startY
                }

                //startY = startY + 30
            }
            count++;
            nb++;
        }

        //this.elements.push(texte1);
        this.refGame.global.util.showTutorialInputs('btnValider');
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.validateWholeExercice.bind(this));
        //écrire


        this.init();


    }

    onInfoClickGreen() {
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: justify"> ' + txtGreen + ' </div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    /**
     * Cette méthode sera déclenchée quand on clique sur l'un des boutons Info, elle va générer une popup avec le texte et
     * l'icône d'un hautparleur qui va déclencher le tts
     * @param info objet de l'image qui contient un id permettant d'identifier le texte qu'il faut afficher
     */
    onInfoClick(info) {
        let txt;
        if (!this.exercice.spriteToMove[info.getTextId()].text) {
            txt = this.exercice.spriteToMove[info.getTextId()].texte
        } else {
            txt = this.exercice.spriteToMove[info.getTextId()].text
        }
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td style="padding-right: 30px"><div><img src=' + this.refGame.global.resources.getImage(this.exercice.spriteToMove[info.getTextId()].sprite).image.src + ' alt="' + this.exercice.spriteToMove[info.getTextId()].sprite + '"></div></td>' +
                '<td><div id="activites" style="text-align: justify">' + txt + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    onShapeMove(shape) {
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.cells[i].setShape(null);
            }
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.cells[i].getShape() === shape || !this.cells[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.cells[i].setShape(shape);
            }
        }
    }

    /**
     * Methode appellée quand l'utilisateur arrête de bouger une image
     * @param shape
     */
    onShapeEndMove(shape) {
        let ok = false;
        if (!shape.getStopMove()) {
            for (let i = 0; i < this.cells.length; i++) {
                let cell = this.cells[i];
                let minX = cell.x - cell.width / 1.7;
                let maxX = cell.x + cell.width / 1.7;
                let minY = cell.y - cell.width / 1.7;
                let maxY = cell.y + cell.width / 1.7;
                if (cell.x == shape.getX() && cell.y == shape.getY()) {
                    ok = true
                }
                if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                    shape.setX(cell.x);
                    shape.setY(cell.y);
                    if (this.cells[i].getShape() != null) {
                        this.generateJustificationPopup(this.cells[i], shape.getposition());
                        if (this.cells[i].getShape() !== shape) {
                            this.cells[i].getShape().setX(this.cells[i].getShape().getBaseX());
                            this.cells[i].getShape().setY(this.cells[i].getShape().getBaseY());
                        }
                    }
                    this.cells[i].setShape(shape);
                }

            }
            if (!ok) {
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
                this.elements.push(shape)
                this.init()
            }
        }
    }


    /**
     * Cette methode génère la popup de justification quand elle est appellée (quand une image a été déposée dans un aimant)
     * @param cell cellule ou l'image a été déposée
     * @param i
     */
    generateJustificationPopup(cell, i) {
        //creation de l'html qui sera mis à l'interieur de la popup
        var shape = cell.getShape();
        let count = 1;
        let htmlToInsertInPopup = "";
        let k = 1
        let sprit = this.exercice.spriteToMove[i];
        for (let key in this.exercice.spriteToMove[i].popup.answers) {
            htmlToInsertInPopup += '<input type="radio" class="radiobtn"id="input_' + count + '" name="validate_' + i +
                '" value="op' + count + '" data-correct="' + this.exercice.spriteToMove[i].popup.answers[k].ok + '">' +
                ' <label htmlFor="op' + count + '" style="padding-left: 3px">';
            let txt = "";
            txt = this.exercice.spriteToMove[i].popup.answers[k].text;
            htmlToInsertInPopup += txt + '</label>';
            htmlToInsertInPopup += '<br>';
            count++;
            k++
        }
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            html:
                '<h4>' + 'Tu places ' + this.refGame.global.resources.getOtherText(shape.name) + ' le ' + cell.getDate() + '. Pourquoi ?' + '</h4>' +
                '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                '</div>',
            preConfirm: (login) => {
                //check si les btns radio sont sélectionnés
                let ischeck;
                for (let j = 1; j <= 4; j++) {
                    ischeck = document.getElementById("input_" + j).checked;
                    if (ischeck) {
                        break;
                    }
                }
                if (!ischeck) {
                    Swal.showValidationMessage(`Attention tu n'as rien s\u00e9lectionn\u00e9`);
                    //si aucun btn n'est sélectionné -> popup de Warning
                }
            },
            showConfirmButton: true,
        }).then((result) => {
            if (result.dismiss == 'cancel') {
                //Si l'utilisateur clique sur le bouton cancel on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else if (result.dismiss == 'backdrop') {
                //Si l'utilisateur clique dehors la popup on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else {
                //Si l'utilisateur clique sur le bouton valider on test si c'est ok ou pas, et ensuite on bloque l'image
                this.validateImgPositioning(shape.getX(), shape.getY(), shape.getvalidx(), shape.getvalidy(), shape.name);
                //Si l'emplacement est faux ont cree une varible d'erreur qu'on place dans une liste
                this.validateJustification(shape, i, sprit);
                shape.setStopMove(true);
                //ischeck = true;
                //input++;
            }
        });
    }

    onShapeClick(shape) {
    }

    validateWholeExercice() {
        if (this.onEndGame()) {
            var that = this;
            var htmlToPutInTheErrorPopup = [];
            //si tous les éléments ont été placés on teste si sont tous ok ou pas, si ils sont ok on les compte
            var count = 0;
            var countValidation = 0;
            var countImgPositioning = 0;
            var reponseEleve = "";
            for (var key in this.validation) {
                //ici on récupère les résultats de l'élève et on génère le texte qui sera mis dans la BD au mode évaluer
                var ligneCourrante = "";
                if (this.validation[count].substr(0, 2) == "ok") {
                    countValidation++;
                    var elmsOK = this.validation[count].split(" ");
                    ligneCourrante += this.exercice.spriteToMove[elmsOK[1]].sprite + ":" + "Val=OK//";
                } else {
                    var elmsKO = this.validation[count].split("/");
                    let i = 1;
                    let juste = "";
                    //récuperer la bonne réponse
                    while (this.exercice.spriteToMove[elmsKO[0]].popup.answers[i].ok != true) {
                        i++
                    }
                    juste = this.exercice.spriteToMove[elmsKO[0]].popup.answers[i].text;

                    ligneCourrante += this.exercice.spriteToMove[elmsKO[0]].sprite + ":";
                    if (elmsKO[1] == 0) {
                        ligneCourrante += "Val=KO-//";
                        htmlToPutInTheErrorPopup.push("<p>" + this.refGame.global.resources.getOtherText('selected') + ": <span style='color: red'>" + this.refGame.global.resources.getOtherText('noResponse') + "</span></p>");
                    } else {
                        ligneCourrante += "Val=KO-" + elmsKO[1] + "//";
                        htmlToPutInTheErrorPopup.push("<p>" + this.refGame.global.resources.getOtherText('selected') + ": <span style='color: red'>" + this.exercice.spriteToMove[elmsKO[0]].popup.answers[elmsKO[1]].text + "</span></p>");
                    }
                    htmlToPutInTheErrorPopup.push("<p>Juste: <span style='color: green'>" + juste + "</span></p>");
                }
                if (this.imgPositioning[count] == "ok") {
                    countImgPositioning++;
                    ligneCourrante += "Pos=OK;";
                } else {
                    ligneCourrante += "Pos=KO;";
                }
                reponseEleve += ligneCourrante;
                count++;
            }
            count = 0;
            // Si c'est le mode eévaluer on teste les erreurs, on donne une note et on ajoute le resumé de l'exercice
            // On donne un point à l'élève s'il a fait moins de la moitié des erreurs faisables
            // 0 s'il a moins de la moitié et 2 s'il a parfaitement juste
            if (this.evaluate) {
                let points = -1;
                if (countValidation == 0){
                    points = 0;
                }
                if (countValidation == countImgPositioning) {
                    points = 2;
                } else if (countValidation >= (countImgPositioning / 2)) {
                    points = 1;
                } else {
                    points = 0;
                }
                this.refGame.global.statistics.addStats(points);
                let resultSend = points + "," + reponseEleve + "," + this.id;
                this.refGame.global.statistics.updateStats(points);
                // this.refGame.global.statistics.updateStats(resultSend);
                if (points === 2) {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('allGood'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'success',
                        showConfirmButton: true
                    });
                } else {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('errorsMade'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'error',
                        showConfirmButton: true
                    });
                }
                this.clear();
                this.elements = [];
                this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('startBtn'), 0x007bff, 0xFFFFFF, true);
                this.startButton.setOnClick(this.newGame.bind(this));
                this.elements.push(this.startButton);
                this.show(this.evaluate);
            } else {

                // si on a 5 pour la validation et pour le positionning on affiche une popup qui indique que tout est juste
                // sinon on affiche les erreurs
                //if (countValidation == 5 && countImgPositioning == 5) {

                if (this.listError.length == 0) {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('allGood'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'success',
                        showConfirmButton: true
                    }).then(() => {
                        that.reStart();
                        // this.show(false, null);
                    });
                } else {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('errorsMade'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'error',
                        showConfirmButton: true,
                    }).then((result) => {
                        that.generatePopupErrorPlacement();
                        //  this.refGame.global.util.showTutorialInputs('btnResetTr');
                        // this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.resetExercice.bind(this));

                    });
                }
                /*
*/
            }
        } else {
            swal.fire({
                title: "Tu n'as pas fini !",
                text: "Tu pourras valider ton exercice lorsque tu auras plac\u00e9 toutes les images ;)",
                icon: "warning",
            });
        }

    }


    validateImgPositioning(imgX, imgY, correctX, corrextY, name) {
        let c
        if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
            c = 0
        } else {
            c = 80
        }

        let solution;
        let image;
        if (imgX == correctX && imgY == corrextY) {
            this.imgPositioning.push("ok");
            this.checkError = true;
        } else {
            this.imgPositioning.push(imgX + "/" + imgY);
            this.checkError = false;
        }
        this.nbBouger++;
        if (!this.checkError) {
            let i = 1;
            let xJson;
            let yJson;
            for (let key in this.exercice.elementsInTab.magnets) {
                xJson = this.exercice.elementsInTab.magnets[i].x;
                yJson = this.exercice.elementsInTab.magnets[i].y;
                if (xJson != correctX && yJson + c != corrextY) {
                    solution = this.exercice.elementsInTab.magnets[i].text;
                    image = this.getImageForPopUpWithName(name);

                }
                i++;
            }
            this.addErrorToList(false, image, solution, null);
        }
        //this.onEndGame();
    }

    getImageForPopUpWithName(name) {
        return this.refGame.global.resources.getImage(name);
    }


    validateJustification(shape, imgPos, sprit) {
        let solution = "";
        //obtention de la solution
        let k = 1;
        for (var key in sprit.popup.answers) {
            if (sprit.popup.answers[k].ok) {
                solution = sprit.popup.answers[k].text;
            }
            k++;
        }
        let img = this.getImageForPopUpWithCount(imgPos);
        var ok = document.getElementById("input_" + shape.getidValidRes()).checked;
        if (ok) {
            this.validation.push("ok" + " " + imgPos);
        } else {
            let count = 1;
            let selectedInput = 0;
            for (var key in this.exercice.spriteToMove[imgPos].popup.answers) {
                if (document.getElementById("input_" + count).checked) {
                    selectedInput = count;
                }
                count++;
            }
            //ImgPos est la position de l'image dans la liste selected input est la justification sélectionnée
            this.validation.push(imgPos + "/" + selectedInput);
            //ajout de la mauvaise réponse dans la liste d'erreur
            this.addErrorToList(true, img, solution, null);
        }

    }


    getImageForPopUpWithCount(count) {
        let img = this.exercice.spriteToMove[count].sprite;
        return this.getImageForPopUpWithName(img);
    }

    onEndGame() {
        let res = false;
        //si le nombre de sprite to move est égale au nombre d'image bougé
        if (this.nbBouger == this.nbFrome) {
            res = true;
        }
        return res;
    }


    generatePopupErrorPlacement() {
        if (this.listError.length > 0) {
            var that = this;
            let htmlToInsertInPopup = '';
            // i = le nombre d'erreur total
            let i = 0;
            //nbWrong = le nombre d'erreur de placement
            let nbWrong = 0;
            for (let key in this.listError) {
                if (!this.listError[i].isJustification) {
                    htmlToInsertInPopup += "<p size='3'><img src=" + this.listError[i].img.image.src + " alt='oups....'>" + this.listError[i].rightAnswser + "<br>";
                    //+ une erreur de placement
                    nbWrong++;
                }
                //+ une erreur
                i++;
            }
            //check si il y a des erreur de placement
            if (nbWrong > 0) {
                let txtPopUp = "";
                //pour le "s" de erreur
                if (nbWrong == 1) {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreur de placement. Tu aurais d\u00fb le mettre :";
                } else {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreurs de placement. Tu aurais d\u00fb le mettre : ";
                }
                Swal.fire({
                    width: 500,
                    confirmButtonColor: BLEU,
                    showCancelButton: false,
                    confirmButtonText: this.txtBtnOK,
                    showConfirmButton: true,
                    html: '<h4>' + txtPopUp + '</h4>' +
                        '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                        '</div>'
                }).then((result) => {
                    if (that.validatorPopup) {
                        that.generatePopupErrorJustification();
                    } else {
                        that.reStart()
                    }

                })
            }
        }
    }

    reStart() {

        if (this.nbWeek == GRID4WEEKS) {
            this.validerX = 500;
            this.validerY = 200;

            this.btnretourX = 500;
            this.btnretourY = 250;

        } else {
            this.validerX = 400;
            this.validerY = 550;

            this.btnretourX = 150;
            this.btnretourY = 550;

        }

        this.btnVali = new Button(this.validerX, this.validerY, "valider l'exercice", BLUE_COLOR, WITHE_COLOR, true);
        this.btnVali.setHeight(35);
        this.btnRetour = new Button(this.btnretourX, this.btnretourY, "retour", GREY_COLOR, WITHE_COLOR, true);
        this.btnRetour.setHeight(35);


        this.btnRetour.setOnClick(this.validateEventPlace.bind(this));

        this.btnVali.setOnClick(this.saveExercice().bind(this));

        this.elements.push(this.btnVali);

        this.elements.push(this.btnRetour);


        this.refGame.global.util.hideTutorialInputs('btnValider');
        this.refGame.global.util.setTutorialText("tu peux maintenant valider ton exercice, ou revenir en arrière pour faire des modification");

        this.init();

    }


    addErrorToList(justification, img, solution, rep) {
        if (justification) {
            this.validatorPopup = justification
        }
        var error = {
            isJustification: justification,
            falseAnswser: rep,
            img: img,
            rightAnswser: solution
        };
        //ajout à la liste 
        this.listError.push(error);

    }

    generatePopupErrorJustification() {
        if (this.listError.length > 0) {
            let htmlToInsertInPopup = '';
            let i = 0;
            let nbWrong = 0;
            let that = this;
            for (let key in this.listError) {
                if (this.listError[i].isJustification) {
                    htmlToInsertInPopup += "<p size='3'><img src=" + this.listError[i].img.image.src + " alt='oups....'>" + this.listError[i].rightAnswser + "<br>";
                    nbWrong++;
                }
                i++;
            }
            if (nbWrong > 0) {
                // pour le "s" de erreur
                let txtPopUp = "";
                if (this.listError.length == 1) {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreur de justification. Tu aurais d\u00fb justifier comme ceci:";
                } else {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreurs de justification. Tu aurais d\u00fb justifier comme ceci:";
                }
                Swal.fire({
                    width: 500,
                    confirmButtonColor: BLEU,
                    confirmButtonText: this.txtBtnOK,
                    showConfirmButton: true,
                    html: '<h4>' + txtPopUp + '</h4>' +
                        '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                        '</div>'
                }).then((result) => {
                    this.reStart();
                });
            }

        }

    }


}