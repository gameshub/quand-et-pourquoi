//const txtGreen = "la case en vert repr\u00e9sente le moment pr\u00e9sent";
class Explore extends Interface {


    constructor(refGame) {

        super(refGame, "explore");
        this.tutotxtNo = 1;
        this.cells = [];
    }



    /**
     * Cette méthode nous permet créer et afficher tout ce qu'on voit sur le canevas
     */
    show() {

        this.countbtn = 0;
        this.cells = [];
        this.imgPositioning = [];
        this.elements = [];
        this.clear();
        this.txtBtnOK = "Ok";
        this.tutotxtNo = 1;
        this.validation = [];
        this.listError = [];
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.refGame.global.util.setTutorialText("<p>" + this.refGame.global.resources.getTutorialText('tutoWelcome') + "</p> <p style='font-size:20px;'>" + this.refGame.global.resources.getTutorialText('tuto4') + "</p>");
        this.refGame.global.util.showTutorialInputs('btnReset', 'next');
        this.refGame.global.util.hideTutorialInputs('btnResetTr', 'btnValider', 'btnAudioGame');
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('next'), ['click'], this.handleNext.bind(this));

        // Rapport de taille pour augmenter la taille globale des éléments
        const ratio = 1.1862;

        this.exercice = this.refGame.global.resources.getScenario();
        this.exercice = JSON.parse(this.exercice);
        (this.exercice.explorer)
        //insertion des aimants
        var count = 1
        for (let key in this.exercice.explorer.elementsInTab.magnets) {
            var magnetInTab = this.exercice.explorer.elementsInTab.magnets[count];

        //    let tempXMagnet = (parseInt(magnetInTab.x) * 1.1862) - 54;
        //    let tempYMagnet = (parseInt(magnetInTab.y) * 1.1862) - 3;

            let magnet = new Guide(magnetInTab.x - 54, magnetInTab.y - 3, 20, magnetInTab.text);
            this.cells.push(magnet);
            this.elements.push(magnet);
            count++;
            //console.log(cell);
            console.log(count);
            if (this.exercice.explorer.elementsInTab.magnets.length == count) {
                break;
            }
        }
        // insertion du background
        let back = this.exercice.explorer.difficulty;
        let backImag = this.refGame.global.resources.getImage(back.name).image.src;
        let insererBack = new ImgBack(300, 175, 415, 350, backImag, 'background');
        this.elements.push(insererBack);
        //insertion des images dans le tableau
        var count = 1;
        for (let key in this.exercice.explorer.elementsInTab.sprites) {
            var img = this.exercice.explorer.elementsInTab.sprites[count];
            console.log(this.exercice.explorer.elementsInTab.sprites[count]);
        //    let tempX = (parseInt(img.x) * 1.1862) - 54;
        //    let tempY = (parseInt(img.y) * 1.1862) - 3;

            if (typeof this.refGame.global.resources.getImage(img.nomFichier).image.src !== "undefined") {
                let imageTab = new ImgBack(img.x - 54, img.y - 3, 40, 40, this.refGame.global.resources.getImage(img.nomFichier).image.src, img.nomFichier);
                this.elements.push(imageTab);
            }
            count++;
        }
        count = 1;


        //insertion des aimants
        for (let key in this.exercice.explorer.elementsInTab.magnets) {
            var magnetInTab = this.exercice.explorer.elementsInTab.magnets[count];

            let tempXMagnet = (parseInt(magnetInTab.x) * 1.1862) - 54;
            let tempYMagnet = (parseInt(magnetInTab.y) * 1.1862) - 3;
            
            let magnet = new Guide(parseInt(tempXMagnet), parseInt(tempYMagnet), 20);
            this.cells.push(magnet);
            this.elements.push(magnet);
            count++;
        }
        count = 1

        this.graphics2 = new PIXI.Graphics();
        this.graphics2.beginFill(VERT);

        this.validate1 = this.graphics2.drawRect(100, 348 + 70, 210, 20);
        this.validate2 = this.graphics2.drawRect(100, 388 + 70, 50, 20);
        this.validate3 = this.graphics2.drawRect(100, 428 + 70, 33, 20);
        this.validate4 = this.graphics2.drawRect(240, 428 + 70, 60, 20);
        this.validate5 = this.graphics2.drawRect(465, 428 + 70, 60, 20);
        this.validate6 = this.graphics2.drawRect(315, 468 + 70, 90, 20);
        this.validate7 = this.graphics2.drawRect(100, 468 + 70, 45, 20);

        this.elements.push(this.validate1, this.validate2, this.validate3, this.validate4, this.validate5, this.validate6, this.validate7);
        this.validate1.visible = false;
        this.validate2.visible = false;
        this.validate3.visible = false;
        this.validate4.visible = false;
        this.validate5.visible = false;
        this.validate6.visible = false;
        this.validate7.visible = false;

        let startX = 100;
        let startY = 420;
        let imggreen = new Shape(50, startY - 40, 32, 32, count, this.refGame.global.resources.getImage("greenFace"), "greenFace", 1, true);
        this.elements.push(imggreen);
        let texteGreenTab = new PIXI.Text(txtGreen, {
            // fontFamily: 'Arial',
            fontSize: 18,
            fontWeight: "bolder",
            fill: 0x000000,
            align: 'right',
            //wordWrap: true,
            wordWrapWidth: 380
        });
        var infoGreen = new InfoImg(550, startY - 40, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClickGreen.bind());
        texteGreenTab.x = startX;
        texteGreenTab.y = startY - 50;
        this.elements.push(texteGreenTab);
        this.elements.push(infoGreen);





        for (let key in this.exercice.explorer.spriteToMove) {
            var txt = this.exercice.explorer.spriteToMove[count].texte;
            var img = this.exercice.explorer.spriteToMove[count].sprite;
            this.texte = new PIXI.Text(txt, {
                // fontFamily: 'Arial',
                fontSize: 12,
                fill: 0x000000,
                align: 'left',
                wordWrap: true,
                wordWrapWidth: 580
            });
            var info = new InfoImg(550, startY + 8, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));
            this.elements.push(info);
            this.texte.x = startX;
            this.texte.y = startY;
            startY = startY + 40;
            this.elements.push(this.texte);
            count++;
        }



        //création des images "déplaçables"
        this.bicycle = new ImgBack(50, Math.round(350 * ratio), 38, 38, this.refGame.global.resources.getImage("bicycle").image.src, "bicycle");
        this.restaurant = new ImgBack(50, Math.round(390 * ratio), 38, 38, this.refGame.global.resources.getImage("restaurant").image.src, "restaurant");
        this.birthday = new ImgBack(50, Math.round(430 * ratio), 38, 38, this.refGame.global.resources.getImage("birthday").image.src, "birthday");
        this.movie = new ImgBack(50, Math.round(470 * ratio), 38, 38, this.refGame.global.resources.getImage("movie").image.src, "movie");
        this.elements.push(this.bicycle, this.restaurant, this.birthday, this.movie);

        //images "déplaçables" qui ont été placées automatiquement
        this.bicycle2 = new ImgBack(Math.round(321 * ratio), Math.round(90 * ratio), 38, 38, this.refGame.global.resources.getImage("bicycle").image.src, "bicycle");
        this.restaurant2 = new ImgBack(Math.round(364 * ratio), Math.round(245 * ratio), 38, 38, this.refGame.global.resources.getImage("restaurant").image.src, "restaurant");
        this.birthday2 = new ImgBack(Math.round(407 * ratio), Math.round(245 * ratio), 38, 38, this.refGame.global.resources.getImage("birthday").image.src, "birthday");
        this.movie2 = new ImgBack(Math.round(321 * ratio), Math.round(124 * ratio), 38, 38, this.refGame.global.resources.getImage("movie").image.src, "movie");
        this.elements.push(this.bicycle2, this.restaurant2, this.birthday2, this.movie2);

        //cacher les images plaçes au bon endroit car elles seront affichés dans la deuxième étape
        this.bicycle2.setVisible(false);
        this.restaurant2.setVisible(false);
        this.birthday2.setVisible(false);
        this.movie2.setVisible(false);

        //images grisées
        this.bicycle2Gray = new ImgBack(50, Math.round(350 * ratio), 38, 38, this.refGame.global.resources.getImage("bicycleGray").image.src, "bicycle");
        this.restaurant2Gray = new ImgBack(50, Math.round(390 * ratio), 38, 38, this.refGame.global.resources.getImage("restaurantGray").image.src, "restaurant");
        this.birthday2Gray = new ImgBack(50, Math.round(430 * ratio), 38, 38, this.refGame.global.resources.getImage("birthdayGrey").image.src, "birthday");
        this.movie2Gray = new ImgBack(50, Math.round(470 * ratio), 38, 38, this.refGame.global.resources.getImage("movieGray").image.src, "movie");
        this.elements.push(this.bicycle2Gray, this.restaurant2Gray, this.birthday2Gray, this.movie2Gray);

        //cacher les images grisées car elles seront affichés dans la deuxième étape
        this.bicycle2Gray.setVisible(false);
        this.restaurant2Gray.setVisible(false);
        this.birthday2Gray.setVisible(false);
        this.movie2Gray.setVisible(false);

        //Déclaration de pixi graphics
        this.graphics = new PIXI.Graphics();
        this.graphics.lineStyle(3, ROUGE);

        //Création d'un carré rouge
        this.rect = this.graphics.drawRect(30, 560, 40, 40);
        this.elements.push(this.rect);
        this.rect.visible = false;

        //image que l'utilisateur pourra bouger
        this.lapPool = new Shape(50, 590, 32, 32, 1, this.refGame.global.resources.getImage("lapPool"), 'lapPool', 0, 0, 0, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
        this.lapPool.setStopMove(true);
        this.elements.push(this.lapPool);

        this.init();
        //fin de show
    }





    onInfoClickGreen() {
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: justify"> ' + txtGreen + ' </div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic) {

    }

    /**
     * Cette méthode sera déclenchée quand on clique sur un des boutons Info, elle va générer une popup avec le texte et
     * une icône d'un hautparleur qui va déclencher le tts
     * @param info objet de l'image qui contient un id permettant d'identifier le texte qu'il faut afficher
     */
    onInfoClick(info) {
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td style="padding-right: 30px"><div><img src=' + this.refGame.global.resources.getImage(this.exercice.explorer.spriteToMove[info.getTextId()].sprite).image.src + ' alt="' + this.exercice.explorer.spriteToMove[info.getTextId()].sprite + '"></div></td>' +
                '<td><div id="activites" style="text-align: justify">' + this.exercice.explorer.spriteToMove[info.getTextId()].texte + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });

        console.log(this.exercice.explorer.spriteToMove[info.getTextId()]);

    }

    /**
     * Méthode qui est appelée quand une image est déplacée
     * @param shape
     */
    onShapeMove(shape) {
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.cells[i].setShape(null);
            }
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.cells[i].getShape() === shape || !this.cells[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.cells[i].setShape(shape);
            }
        }
    }


    /**
     * Méthode appelée quand on arrête de bouger une image
     * @param shape
     */
    onShapeEndMove(shape) {
        let ok = false;
        console.log(shape)
        if (!shape.getStopMove()) {
            for (let i = 0; i < this.cells.length; i++) {
                let cell = this.cells[i];
                let minX = cell.x - cell.width / 1.7;
                let maxX = cell.x + cell.width / 1.7;
                let minY = cell.y - cell.width / 1.7;
                let maxY = cell.y + cell.width / 1.7;
                if (cell.x == shape.getX() && cell.y == shape.getY()) {
                    ok = true
                }
                if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                    shape.setX(cell.x);
                    shape.setY(cell.y);
                    if (this.cells[i].getShape() != null) {
                        //Lancement de la méthode
                        if (this.cells[i].date !== undefined) {
                            let position = this.getpositionByName(shape);
                            this.datecell = this.cells[i].date;
                            this.position = position
                            this.cellInTheTab = this.cells[i]
                            this.refGame.global.util.showTutorialInputs('next')
                            this.handleNext()
                            this.generateJustificationPopup(this.cellInTheTab, this.position);
                        }
                        if (this.cells[i].getShape() !== shape) {
                            this.cells[i].getShape().setX(this.cells[i].getShape().getBaseX());
                            this.cells[i].getShape().setY(this.cells[i].getShape().getBaseY());
                        }
                    }
                    this.cells[i].setShape(shape);
                }
            }
            if (!ok) {
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
                this.elements.push(shape)
                this.init()
            }
        }
    }
    /**
     * Cette fonction retourne la position de la forme bougée.
     * @param shape la forme dont on cherche le nom
     * @returns  la position du nom de la forme dans le json. -1 si rien ne correspond 
     */
    getpositionByName(shape) {
        let position = -1;
        let count = 1;
        console.log(shape.name);
        for (let key in this.exercice.explorer.spriteToMove) {
            let name = this.exercice.explorer.spriteToMove[count].sprite;
            let areEqual = shape.name.toUpperCase() === name.toUpperCase();
            if (areEqual) {
                position = count;
                break;
            }
            count++;
        }
        return position;
    }

    /**
     * Cette méthode est en charge de générer la popup quand c'est nécessaire. La popup contiendra 4
     */
    popUp() {
        let count = 1;
        let htmlToInsertInPopup = "";
        for (let key in this.exercice.explorer.spriteToMove[5].popup.answers) {
            htmlToInsertInPopup += '<input type="radio" id="op' + count + '" name="op' + count + '" value="op' + count + '"'
            if (this.exercice.explorer.spriteToMove[5].popup.answers[count].ok == true) {
                htmlToInsertInPopup += 'checked="checked">'
            } else {
                htmlToInsertInPopup += 'disabled>'
            }
            '<label htmlFor="op' + count + '" style="padding-left: 3px">'//checked="checked"
            let txt = "";
            txt = this.exercice.explorer.spriteToMove[5].popup.answers[count].texte;
            htmlToInsertInPopup += txt + '</label>';
            htmlToInsertInPopup += '<br>';
            count++;
        }
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<h4>' + this.refGame.global.resources.getTutorialText('tutoPopupTitle') + '</h4>' +
                '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                '</div>'
        });

    }

    /**
     * Cette methode génére la popup de justification quand elle est appéllée (quand une image a été dépose dans un aimant)
     * @param cell cellule ou l'image a été déposée
     * @param i
     */
    generateJustificationPopup(cell, i) {
        //let i = 5
        //creation de l'html qui sera mis à l'interieur de la popup
        var shape = cell.getShape();
        let count = 1;
        let htmlToInsertInPopup = "";
        console.log(i);
        console.log(this.exercice.explorer.spriteToMove)
        for (let key in this.exercice.explorer.spriteToMove[i].popup.answers) {
            htmlToInsertInPopup += '<input type="radio" class="radiobtn"id="input_' + count + '" name="validate_' + i +
                '" value="op' + count + '" data-correct="' + this.exercice.explorer.spriteToMove[i].popup.answers[count].ok + '">' + ' <label htmlFor="op' + count + '" style="padding-left: 3px">';
            let txt = "";//AU SECOURS
            txt = this.exercice.explorer.spriteToMove[i].popup.answers[count].texte;
            htmlToInsertInPopup += txt + '</label>';
            htmlToInsertInPopup += '<br>';
            count++;
            //nécessaire pour que la popUp ne fasse pas n'importe quoi           
            console.log("taille : " + this.exercice.explorer.spriteToMove[i].popup.answers.length)
            if (count == this.exercice.explorer.spriteToMove[i].popup.answers.length) {
                this.countbtn = count - 1;
                break;
            }
        }
        this.countbtn - 1;
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            showConfirmButton: true,
            html:
                '<h4>' + 'Tu places la piscine le ' + this.datecell + '. Pourquoi ?' + '</h4>' +
                '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                '</div>',
            preConfirm: (login) => {
                let ischeck;
                for (let j = 1; j <= 4; j++) {
                    ischeck = document.getElementById("input_" + j).checked;
                    if (ischeck) {
                        break;
                    }
                }
                if (!ischeck) {
                    Swal.showValidationMessage(`Attention tu n'as rien s\u00e9lectionn\u00e9`);
                }
            }
        }).then((result) => {
            if (result.dismiss == 'cancel') {
                //Si l'utilisateur clique sur le bouton cancel on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else if (result.dismiss == 'backdrop') {
                //Si l'utilisateur clique dehors la popup on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else {
                //Si l'utilisateur clique sur le bouton valider on test si c'est ok ou pas, et ensuite on bloque l'image
                this.validateImgPositioning(shape.getX(), shape.getY(), shape.getvalidx(), shape.getvalidy());
                this.validateJustification(shape.getidValidRes(), i);
                this.generatePopupErrorJustification();
                //reesaayser
                this.refGame.global.util.hideTutorialInputs('btnReset', 'next');
                this.refGame.global.util.showTutorialInputs('btnValider');
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.EndGame.bind(this));
                shape.setStopMove(true);
            }
        });
    }

    /**
    * Cette méthode nous permet de tester que le positionement de l'image sur le calendrier est correct puis le stocker
    * @param imgX coordonées en x de l'image
    * @param imgY coordonées en y de l'image
    * @param correcontext coordonées correctes en x
    * @param corrextY coordonées correctes en y
    */
    validateImgPositioning(imgX, imgY, correcontext, corrextY) {
        if (imgX == correcontext && imgY == corrextY) {
            this.imgPositioning.push("ok");
        } else {

            this.imgPositioning.push(imgX + "/" + imgY);
        }
    }


    /**
 * Cette méthode nous permet de tester que la justification soie correcte
 * @param correctInput id de l'input qui faut vérifier si à été coche ou pas
 */
    validateJustification(correctInput, imgPos) {
        let ok = false;
        let count = 1;
        let selectedInput = 0;
        let solution;
        for (var key in this.exercice.explorer.spriteToMove[imgPos].popup.answers) {
            if (this.exercice.explorer.spriteToMove[imgPos].popup.answers) {
                if (this.exercice.explorer.spriteToMove[imgPos].popup.answers[count].ok) {
                    solution = this.exercice.explorer.spriteToMove[imgPos].popup.answers[count].texte;
                    if (document.getElementById("input_" + count).checked) {
                        ok = true;
                        break;
                    } else {
                        ok = false;
                    }
                }
                if (document.getElementById("input_" + count).checked) {
                    selectedInput = count;
                }
                count++;
            }
        }


        //ImgPos est la position de l'image dans la liste selected input est la justification sélectionnée
        if (!ok) {
            this.addErrorToList(false, this.refGame.global.resources.getImage("lapPool"), solution);
        }
        this.validation.push(imgPos + "/" + selectedInput);


    }

    /**
 * cette méthode va ajouter une variable d'erreur a la liste.
 * @param   justification Boolean qui nous indique si l'erreur est une justification ou un placement
 * @param   img l'image de l'activité
 * @param   solution String qui nous donne la solution
 */
    addErrorToList(justification, img, solution) {
        var error = {
            isJustification: justification,
            img: img,
            rightAnswser: solution
        };
        //ajout à la liste 
        this.listError.push(error);
    }


    generatePopupErrorJustification() {
        if (this.listError.length > 0) {
            let htmlToInsertInPopup = '';
            let i = 0;
            let that = this;
            htmlToInsertInPopup += "<p size='3'><img src=" + this.listError[i].img.image.src + " alt='oups....'>" + this.listError[i].rightAnswser + "<br>";
            let txtPopUp = "Tu as commis 1 erreur de justification. Tu aurais d\u00fb justifier comme ceci:";
            Swal.fire({
                width: 500,
                confirmButtonColor: BLEU,
                confirmButtonText: this.txtBtnOK,
                showConfirmButton: true,
                html: '<h4>' + txtPopUp + '</h4>' +
                    '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                    '</div>'
            }).then((result) => {
            });


        } else {
            Swal.fire({
                icon: 'success',
                title: 'Bravo',
                confirmButtonText: "ok",
                showConfirmButton: true,
                text: 'Tu as fait aucune erreur !',
            })

        }

    }


    /**
     * Cette méthode gère les clics qu'on fait sur le menu de gauche du canevas, selon le nombre de fois que le bouton a
     * été clique on affiche certains éléments ou pas
     */
    handleNext() {
        if (this.tutotxtNo === 1) {
            this.refGame.global.util.hideTutorialInputs('next');
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('tuto2'));

            //activer le deplacement pour l'icône "poolLap"
            this.lapPool.setStopMove(false);

            //cacher les icônes qui ont été déjà placées
            this.bicycle.setVisible(false);
            this.restaurant.setVisible(false);
            this.birthday.setVisible(false);
            this.movie.setVisible(false);

            //recréer les icônes mais cette fois dans le bon endroit du tableau
            this.bicycle2.setVisible(true);
            this.restaurant2.setVisible(true);
            this.birthday2.setVisible(true);
            this.movie2.setVisible(true);

            //afficher les images grises
            this.bicycle2Gray.setVisible(true);
            this.restaurant2Gray.setVisible(true);
            this.birthday2Gray.setVisible(true);
            this.movie2Gray.setVisible(true);

            //afficher le carré rouge
            this.rect.visible = true;

            //créer les rectangles verts pour mettre le texte en surbrillance
            this.validate1.visible = true;
            this.validate2.visible = true;
            this.validate3.visible = true;
            this.validate4.visible = true;
            this.validate5.visible = true;
            this.validate6.visible = true;
            this.validate7.visible = true;

            this.tutotxtNo = 2;
        } else if (this.tutotxtNo === 2) {
            //this.popUp(this.refGame.global.resources.getOtherText('titleTest'), "ceci est un test", this.cells[0].getShape());
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('tuto3'));
            //this.refGame.global.util.hideTutorialInputs('next');

            this.rect.visible = false;
            this.tutotxtNo = 3;
        }
    }
    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic) {

    }
    onShapeClick(shape) {
    }
    onDragEnd() {
    }
    EndGame() {
        this.elements = []
        this.init();
        this.show();
    }
}