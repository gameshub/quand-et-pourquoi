const txtGreen = "la case en vert repr\u00e9sente le moment pr\u00e9sent";
class Train extends Interface {

    constructor(refGame) {
        super(refGame, "train");
        this.evaluate = false;
        this.exercice = null;
        //this.show(this.evaluate, this.exercice);
    }

    /**
     * Méthode qui sera appelée pour afficher les éléments dans le canevas
     * @param evaluate boolean pour savoir si on est dans le mode entrainer ou évaluer
     * @param exercice int qui indique l'id de l'exercice à afficher 0x001200
     */
    setRefPlay(refPlay) {
        this.refPlay = refPlay
    }
    show(evaluate, exercice) {
        this.clear();
        this.correctionY = -6;
        this.txtBtnOK = "Ok"
        this.elements = [];
        this.checkError = false;
        this.validatorPopup = false
        if (exercice) {
            this.exercice = exercice;
            this.newGame(-1)
        } else {
            this.refreshLang(this.refGame.global.resources.getLanguage());
            this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('startBtn'), 0x007bff, 0xFFFFFF, true);
            this.startButton.setOnClick(this.newGame.bind(this));
            for (let i = 0; i < this.elements.length; i++) {
                if (typeof this.elements[i].origin !== "undefined") {
                    this.elements[i].reset();
                }
            }
            this.clear();
            this.elements.push(this.startButton);
            this.evaluate = evaluate;
            this.refGame.global.util.hideTutorialInputs('btnResetTr', 'btnAudioGame', 'next', 'btnReset');
            this.init();
        }

    }
    /**
     * Cette méthode va afficher un nouvel exercice
     * @param isReset
     */
    newGame(isReset) {
        this.clear();
        this.imgPositioning = [];
        this.validation = [];
        if (isReset != -1) {
            //MODE TRAIN
            if (forced) {
                console.log("forced : " + forced);
                this.exercice = this.refGame.global.resources.getExerciceById(forced);
            }
            else {
                console.log("Not forced");
                this.exercice = this.refGame.global.resources.getExercice();
            }
            if (this.evaluate) {
                // On initialise la partie évaluation
                this.refGame.global.statistics.addStats(2);
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('evaluateMode'));
            } else {
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('trainMode'));
            }
            this.elements.unshift(this.startButton);
            this.startButton.setVisible(false);
            if (typeof this.exercice == "undefined") {
                this.exercice = this.refGame.global.resources.getExercice();
                this.exercice = JSON.parse(this.exercice.exercice);
            }

            this.refGame.global.util.hideTutorialInputs('btnResetTr');
            //json Sélectionner un exercice sauf celui de base
            //this.exercice = this.refGame.global.resources.getExercice();
            //this.exercice = JSON.parse(this.exercice);
            //this.exercice = this.refGame.global.resources.getExercice();
            //console.log(this.exercice)
            this.exercice = JSON.parse(this.exercice.exercice);
            this.id = this.exercice.id;


        }
        //console.log("__________________________________________________________")
        //console.log(this.exercice)
        //insertion du background
        this.back = this.exercice.difficulty;
        this.backImg = this.refGame.global.resources.getImage(this.back.name).image.src;


        let insererBack = new ImgBack(this.back.locationx, this.back.locationy, this.back.x, this.back.y, this.backImg, 'background');
        this.elements.push(insererBack);

        //insertion des images dans le tableau
        var count = 1;
        for (let key in this.exercice.elementsInTab.sprites) {
            var img = this.exercice.elementsInTab.sprites[count];
            if (typeof this.refGame.global.resources.getImage(img.nomFichier).image.src !== "undefined") {
                let imageTab = new ImgBack(img.x, img.y, img.width, 32, this.refGame.global.resources.getImage(img.nomFichier).image.src, img.nomFichier);
                //let imageTab = new ImgBack(0, 0, img.width, 32, this.refGame.global.resources.getImage(img.nomFichier).image.src, img.nomFichier);
                this.elements.push(imageTab);
            }
            count++;
        }
        count = 1;
        //this.nbFrome = this.exercice.spriteToMove;
        this.nbFrome = 0;
        for (let key in this.exercice.spriteToMove) {
            this.nbFrome++;
        }
        this.cells = [];
        this.listError = [];
        this.nbBouger = 0;
        //insertion des aimants
        for (let key in this.exercice.elementsInTab.magnets) {
            var magnetInTab = this.exercice.elementsInTab.magnets[count];
            let x = magnetInTab.x;
            let y = magnetInTab.y;
            let isPlaced = false;
            let magnet = new Guide(parseInt(x), parseInt(y), 20, magnetInTab.text,);
            this.cells.push(magnet);
            this.elements.push(magnet);
            count++;
        }

        //Insertion des images que l'utilisateur pourra bouger
        let startXgreen;
        let startYgreen;

        if (this.exercice.difficulty.name == DIFFICULTEHAUTE) {
            var startX = 400;
            var startY = 30;
            startXgreen = 400;
            startYgreen = 40;
            this.correctionY = -2;
        } else if (this.exercice.difficulty.name == "grid2weeks") {
            var startX = 50;
            var startY = 430;
            startXgreen = 50;
            startYgreen = 370;
        } else if (this.exercice.difficulty.name == "grid3days") {
            var startX = 50;
            var startY = 350;
            startXgreen = 50;
            startYgreen = 290;
        } else if (this.exercice.difficulty.name == "grid1week") {
            var startX = 50;
            var startY = 350;
            startXgreen = 50;
            startYgreen = 285;
        }

        count = 1
        //affichage du txt explicatif du
        this.textVerte = new PIXI.Text(txtGreen, {
            // fontFamily: 'Arial',
            fontSize: 18,
            fontWeight: "bolder",
            fill: 0x000000,
            align: 'right',
            //wordWrap: true,
            wordWrapWidth: 380
        });

        this.textVerte.x = startXgreen + 50;
        this.textVerte.y = startYgreen - 10;

        let sizeIcon;
        if (this.exercice.difficulty.name == DIFFICULTEHAUTE) {
            sizeIcon = 32;
        } else if (this.exercice.difficulty.name == "grid2weeks") {
            sizeIcon = 37;
        } else if (this.exercice.difficulty.name == "grid3days") {
            sizeIcon = 48;
        } else if (this.exercice.difficulty.name == "grid1week") {
            sizeIcon = 46;
        }

        let imggreen = new Shape(startXgreen, startYgreen, 32, 32, count, this.refGame.global.resources.getImage("greenFace"), "greenFace", idOK, true);
        if (this.exercice.difficulty.name == DIFFICULTEHAUTE) {
            //var info = new InfoImg(550, startY + 8, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            var info = new InfoImg(450, 40, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClickGreen.bind());//,count,this.onShapeClick.bind(this)
            this.elements.push(info);
        } else {
            this.elements.push(this.textVerte);
            var info = new InfoImg(550, startYgreen, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClickGreen.bind());//,count,this.onShapeClick.bind(this)
            this.elements.push(info);
        }


        let nb = 1
        let startX2 = startX
        let startY2 = startY
        this.elements.push(imggreen);

        for (let key in this.exercice.spriteToMove) {
            var txt = this.exercice.spriteToMove[count].texte;
            var img = this.exercice.spriteToMove[count].sprite;
            var i = 1;
            var idOK = 0;
            //Ici on récupère quelle est la bonne réponse, on va utiliser la variable idOk plus tard, l'hors de la validation de l'exercice
            for (let key in this.exercice.spriteToMove[count].popup.answers) {
                if (this.exercice.spriteToMove[count].popup.answers) {
                    if (this.exercice.spriteToMove[count].popup.answers[i].ok) {
                        idOK = i;
                    }
                    i++;
                }
            }
            //insertion des images qui pourront être bougées
            let imageTab;
            let imageTab2;
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                startX2 = startX2 + 60
                imageTab = new Shape(startX2, startY, sizeIcon, sizeIcon, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2 = new Shape(startX2, startY, sizeIcon, sizeIcon, count, this.refGame.global.resources.getImage(img));
                imageTab2.setStopMove(true);

            } else {
                this,
                    imageTab = new Shape(startX2, startY2 + 80, sizeIcon, sizeIcon, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2 = new Shape(startX2, startY2 + 80, sizeIcon, sizeIcon, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2.setStopMove(true);
            }
            imageTab2.setAplha(0.3)
            this.elements.push(imageTab2);
            this.elements.push(imageTab);
            //insertion du texte
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                this.texte = new PIXI.Text(txt, {
                    // fontFamily: 'Arial',
                    fontSize: 12,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.texte.x = 100;
                this.texte.y = startY;
                //this.elements.push(this.texte);
            }

            //insertion des "i" a gauche du text / des images
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                var info = new InfoImg(startX2, startY + 30, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            } else {
                var info = new InfoImg(startX2 + 50, startY2 + 80, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            }
            this.elements.push(info);
            //startY = startY + 40; 
            startY2 = startY2 + 35
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                if (nb % 8 == 0) {
                    startX2 = startX
                    startY = startY + 70
                }
            } else {
                if (nb % 14 == 0) {
                    startX2 = startX2 + 100
                    startY2 = startY
                }

                //startY = startY + 30
            }
            count++;
            nb++;
        }

        //this.elements.push(texte1);
        this.refGame.global.util.showTutorialInputs('btnValider');
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.validateWholeExercice.bind(this));
        //écrire 


        this.init();


    }

    /**
     * Cette méthode sera déclenchée quand on clique sur un des boutons Info, elle va générer une popup avec le texte et
     * une icône d'un hautparleur qui va déclencher le tts
     * @param info objet de l'image qui contient un id permettant d'identifier le texte qu'il faut afficher
     */
    onInfoClickGreen(info) {
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: justify"> ' + txtGreen + ' </div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    /**
     * Cette méthode sera déclenchée quand on clique sur un des boutons Info, elle va générer une popup avec le texte et
     * une icône d'un hautparleur qui va déclencher le tts
     * @param info objet de l'image qui contient un id permettant d'identifier le texte qu'il faut afficher
     */
    onInfoClick(info) {
        let txt;
        if (!this.exercice.spriteToMove[info.getTextId()].text) {
            txt = this.exercice.spriteToMove[info.getTextId()].texte
        } else {
            txt = this.exercice.spriteToMove[info.getTextId()].text
        }

        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td style="padding-right: 30px"><div><img src=' + this.refGame.global.resources.getImage(this.exercice.spriteToMove[info.getTextId()].sprite).image.src + ' alt="' + this.exercice.spriteToMove[info.getTextId()].sprite + '"></div></td>' +
                '<td><div id="activites" style="text-align: justify">' + txt + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    /**
     * Méthode appelée quand l'utilisateur bouge une des images
     * @param shape
     */
    onShapeMove(shape) {
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.cells[i].setShape(null);
            }
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.cells[i].getShape() === shape || !this.cells[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.cells[i].setShape(shape);
            }
        }
    }

    /**
     * Methode appéllée quand l'utilisateur arrete de bouger une image
     * @param shape
     */
    onShapeEndMove(shape) {
        let ok = false;
        if (!shape.getStopMove()) {
            for (let i = 0; i < this.cells.length; i++) {
                let cell = this.cells[i];
                let minX = cell.x - cell.width / 1.7;
                let maxX = cell.x + cell.width / 1.7;
                let minY = cell.y - cell.width / 1.7;
                let maxY = cell.y + cell.width / 1.7;
                if (cell.x == shape.getX() && cell.y == shape.getY()) {
                    ok = true
                }
                if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                    shape.setX(cell.x);
                    shape.setY(cell.y);
                    if (this.cells[i].getShape() != null) {
                        this.generateJustificationPopup(this.cells[i], shape.getposition());
                        if (this.cells[i].getShape() !== shape) {
                            this.cells[i].getShape().setX(this.cells[i].getShape().getBaseX());
                            this.cells[i].getShape().setY(this.cells[i].getShape().getBaseY());
                        }
                    }
                    this.cells[i].setShape(shape);
                }

            }
            if (!ok) {
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
                this.elements.push(shape)
                this.init()
            }
        }
    }



    /**
     * Cette methode génére la popup de justification quand elle est appéllée (quand une image a été dépose dans un aimant)
     * @param cell cellule ou l'image a été déposée
     * @param i
     */
    generateJustificationPopup(cell, i) {
        //creation de l'html qui sera mis à l'interieur de la popup
        var shape = cell.getShape();
        let count = 1;
        let htmlToInsertInPopup = "";
        let k = 1
        let sprit = this.exercice.spriteToMove[i];
        for (let key in this.exercice.spriteToMove[i].popup.answers) {
            htmlToInsertInPopup += '<input type="radio" class="radiobtn"id="input_' + count + '" name="validate_' + i +
                '" value="op' + count + '" data-correct="' + this.exercice.spriteToMove[i].popup.answers[k].ok + '">' +
                ' <label htmlFor="op' + count + '" style="padding-left: 3px">';
            let txt = "";
            txt = this.exercice.spriteToMove[i].popup.answers[k].text;
            htmlToInsertInPopup += txt + '</label>';
            htmlToInsertInPopup += '<br>';
            count++;
            k++
        }
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            html:
                '<h4>' + 'Tu places ' + this.refGame.global.resources.getOtherText(shape.name) + ' le ' + cell.getDate() + '. Pourquoi ?' + '</h4>' +
                '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                '</div>',
            preConfirm: (login) => {
                //check si les btns radio sont sélectionnés
                let ischeck;
                for (let j = 1; j <= 4; j++) {
                    ischeck = document.getElementById("input_" + j).checked;
                    if (ischeck) {
                        break;
                    }
                }
                if (!ischeck) {
                    Swal.showValidationMessage(`Attention tu n'as rien s\u00e9lectionn\u00e9`);
                    //si aucun btn n'est sélectionné -> popup de Warning
                }
            },
            showConfirmButton: true,
        }).then((result) => {
            if (result.dismiss == 'cancel') {
                //Si l'utilisateur clique sur le bouton cancel on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else if (result.dismiss == 'backdrop') {
                //Si l'utilisateur clique dehors la popup on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else {
                //Si l'utilisateur clique sur le bouton valider on test si c'est ok ou pas, et ensuite on bloque l'image
                this.validateImgPositioning(shape.getX(), shape.getY(), shape.getvalidx(), shape.getvalidy(), shape.name);
                //Si l'emplacement est faux ont cree une varible d'erreur qu'on place dans une liste
                this.validateJustification(shape, i, sprit);
                shape.setStopMove(true);
                //ischeck = true;
                //input++;
            }
        });
    }

    /**
     * Méthode qui affiche le bouton de validation.
     */
    onEndGame() {
        let res = false;
        //si le nombre de sprite to move est égale au nombre d'image bougé
        if (this.nbBouger == this.nbFrome) {
            res = true;
        }
        return res;
    }
    /**
     * Méthode qui permet de générer une popup affichant les solutions des erreur de justification.
     * 
     */
    generatePopupErrorJustification() {
        if (this.listError.length > 0) {
            let htmlToInsertInPopup = '';
            let i = 0;
            let nbWrong = 0;
            let that = this;
            for (let key in this.listError) {
                if (this.listError[i].isJustification) {
                    htmlToInsertInPopup += "<p size='3'><img src=" + this.listError[i].img.image.src + " alt='oups....'>" + this.listError[i].rightAnswser + "<br>";
                    nbWrong++;
                }
                i++;
            }
            if (nbWrong > 0) {
                // pour le "s" de erreur
                let txtPopUp = "";
                if (this.listError.length == 1) {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreur de justification. Tu aurais d\u00fb justifier comme ceci:";
                } else {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreurs de justification. Tu aurais d\u00fb justifier comme ceci:";
                }
                Swal.fire({
                    width: 500,
                    confirmButtonColor: BLEU,
                    confirmButtonText: this.txtBtnOK,
                    showConfirmButton: true,
                    html: '<h4>' + txtPopUp + '</h4>' +
                        '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                        '</div>'
                }).then((result) => {
                    this.reStart();
                });
            }

        }

    }
    reStart() {
        if (this.refPlay != null) {
            this.refPlay.show();
        } else {
            this.clear();
            this.elements = [];
            this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('startBtn'), 0x007bff, 0xFFFFFF, true);
            this.startButton.setOnClick(this.newGame.bind(this));
            this.elements.push(this.startButton);
            this.show(this.evaluate);
        }
    }
    /**
     * Méthode qui permet de générer une popup affichant les solutions des erreur de placement.
     * 
     */
    generatePopupErrorPlacement() {
        if (this.listError.length > 0) {
            var that = this;
            let htmlToInsertInPopup = '';
            // i = le nombre d'erreur total
            let i = 0;
            //nbWrong = le nombre d'erreur de placement 
            let nbWrong = 0;
            for (let key in this.listError) {
                if (!this.listError[i].isJustification) {
                    htmlToInsertInPopup += "<p size='3'><img src=" + this.listError[i].img.image.src + " alt='oups....'>" + this.listError[i].rightAnswser + "<br>";
                    //+ une erreur de placement
                    nbWrong++;
                }
                //+ une erreur
                i++;
            }
            //check si il y a des erreur de placement
            if (nbWrong > 0) {
                let txtPopUp = "";
                //pour le "s" de erreur
                if (nbWrong == 1) {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreur de placement. Tu aurais d\u00fb le mettre :";
                } else {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreurs de placement. Tu aurais d\u00fb le mettre : ";
                }
                Swal.fire({
                    width: 500,
                    confirmButtonColor: BLEU,
                    showCancelButton: false,
                    confirmButtonText: this.txtBtnOK,
                    showConfirmButton: true,
                    html: '<h4>' + txtPopUp + '</h4>' +
                        '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                        '</div>'
                }).then((result) => {
                    if (that.validatorPopup) {
                        that.generatePopupErrorJustification();
                    } else {
                        that.reStart()
                    }

                })
            }
        }
    }

    /**
     * Cette méthode nous permet de tester que le positionement de l'image sur le calendrier est correct puis le stocker
     * @param imgX coordonées en x de l'image
     * @param imgY coordonées en y de l'image
     * @param correctX coordonées correctes en x
     * @param corrextY coordonées correctes en y
     */
    validateImgPositioning(imgX, imgY, correctX, corrextY, name) {
        let c
        if (this.exercice.difficulty.name != DIFFICULTEHAUTE) { c = 0 } else { c = 80 }

        let solution;
        let image;
        if (imgX == correctX && imgY == corrextY) {
            this.imgPositioning.push("ok");
            this.checkError = true;
        } else {
            this.imgPositioning.push(imgX + "/" + imgY);
            this.checkError = false;
        }
        this.nbBouger++;
        if (!this.checkError) {
            let i = 1;
            let xJson;
            let yJson;
            for (let key in this.exercice.elementsInTab.magnets) {
                xJson = this.exercice.elementsInTab.magnets[i].x;
                yJson = this.exercice.elementsInTab.magnets[i].y;
                if (xJson != correctX && yJson + c != corrextY) {
                    solution = this.exercice.elementsInTab.magnets[i].text;
                    image = this.getImageForPopUpWithName(name);

                }
                i++;
            }
            this.addErrorToList(false, image, solution, null);
        }
        //this.onEndGame();
    }

    /**
     * Cette méthode nous permet de tester que la justification soie correcte
     * @param correctInput id de l'input qui faut vérifier si à été coche ou pas
     */
    //                this.validateJustification(shape, i, sprit);

    validateJustification(shape, imgPos, sprit) {
        let solution = "";
        //obtention de la solution
        let k = 1;
        for (var key in sprit.popup.answers) {
            if (sprit.popup.answers[k].ok) {
                solution = sprit.popup.answers[k].text;
            }
            k++;
        }
        let img = this.getImageForPopUpWithCount(imgPos);
        var ok = document.getElementById("input_" + shape.getidValidRes()).checked;
        if (ok) {
            this.validation.push("ok" + " " + imgPos);
        } else {
            let count = 1;
            let selectedInput = 0;
            for (var key in this.exercice.spriteToMove[imgPos].popup.answers) {
                if (document.getElementById("input_" + count).checked) {
                    selectedInput = count;
                }
                count++;
            }
            //ImgPos est la position de l'image dans la liste selected input est la justification sélectionnée
            this.validation.push(imgPos + "/" + selectedInput);
            //ajout de la mauvaise réponse dans la liste d'erreur
            this.addErrorToList(true, img, solution, null);
        }

    }
    /**
     * Cette méthode va me permettre valider l'exercice au complet quand on appuie sur le bouton valider à gauche, si on
     * est dans le mode évaluer on va calculer la note et on enverra les résultats de l'exercice à la BD
     */
    validateWholeExercice() {
        if (this.onEndGame()) {
            var that = this;
            var htmlToPutInTheErrorPopup = [];
            //si tous les éléments ont été placés on teste si sont tous ok ou pas, si ils sont ok on les compte
            var count = 0;
            var countValidation = 0;
            var countImgPositioning = 0;
            var reponseEleve = "";
            for (var key in this.validation) {
                //ici on récupère les résultats de l'élève et on génère le texte qui sera mis dans la BD au mode évaluer
                var ligneCourrante = "";
                if (this.validation[count].substr(0, 2) == "ok") {
                    countValidation++;
                    var elmsOK = this.validation[count].split(" ");
                    ligneCourrante += this.exercice.spriteToMove[elmsOK[1]].sprite + ":" + "Val=OK//";
                } else {
                    var elmsKO = this.validation[count].split("/");
                    let i = 1;
                    let juste = "";
                    //récuperer la bonne réponse
                    while (this.exercice.spriteToMove[elmsKO[0]].popup.answers[i].ok != true) {
                        i++
                    }
                    juste = this.exercice.spriteToMove[elmsKO[0]].popup.answers[i].text;

                    ligneCourrante += this.exercice.spriteToMove[elmsKO[0]].sprite + ":";
                    if (elmsKO[1] == 0) {
                        ligneCourrante += "Val=KO-//";
                        htmlToPutInTheErrorPopup.push("<p>" + this.refGame.global.resources.getOtherText('selected') + ": <span style='color: red'>" + this.refGame.global.resources.getOtherText('noResponse') + "</span></p>");
                    } else {
                        ligneCourrante += "Val=KO-" + elmsKO[1] + "//";
                        htmlToPutInTheErrorPopup.push("<p>" + this.refGame.global.resources.getOtherText('selected') + ": <span style='color: red'>" + this.exercice.spriteToMove[elmsKO[0]].popup.answers[elmsKO[1]].text + "</span></p>");
                    }
                    htmlToPutInTheErrorPopup.push("<p>Juste: <span style='color: green'>" + juste + "</span></p>");
                }
                if (this.imgPositioning[count] == "ok") {
                    countImgPositioning++;
                    ligneCourrante += "Pos=OK;";
                } else {
                    ligneCourrante += "Pos=KO;";
                }
                reponseEleve += ligneCourrante;
                count++;
            }
            //si c'est le mode évaluer on teste les erreurs, on donne une note et on ajoute le resumé de l'exercice
            if (this.evaluate) {
                let pointsExe = -1;
                if (countImgPositioning == count && countValidation == count){
                    pointsExe = 2;
                } else if ((countImgPositioning + countValidation) >= count) { // On teste directement sur la variable count car c'est déjà le 50% des points faisables en tout
                    pointsExe = 1;
                } else {
                    pointsExe = 0;
                }
                this.refGame.global.statistics.addStats(pointsExe);
                let resultSend = pointsExe + "," + reponseEleve + "," + this.id;
                this.refGame.global.statistics.updateStats(pointsExe);
                // this.refGame.global.statistics.updateStats(resultSend);
                if (pointsExe === 2) {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('allGood'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'success',
                        showConfirmButton: true
                    });
                } else {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('errorsMade'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'error',
                        showConfirmButton: true
                    });
                }
                this.clear();
                this.elements = [];
                this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('startBtn'), 0x007bff, 0xFFFFFF, true);
                this.startButton.setOnClick(this.newGame.bind(this));
                this.elements.push(this.startButton);
                this.show(this.evaluate);
            } else {

                // si on a 5 pour la validation et pour le positionning on affiche une popup qui indique que tout est juste
                // sinon on affiche les erreurs
                //if (countValidation == 5 && countImgPositioning == 5) {
                if (this.listError.length == 0) {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('allGood'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'success',
                        showConfirmButton: true
                    }).then((result) => {
                        that.generatePopupErrorPlacement();
                        this.clear();
                        this.elements = [];
                        this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('startBtn'), 0x007bff, 0xFFFFFF, true);
                        this.startButton.setOnClick(this.newGame.bind(this));
                        this.elements.push(this.startButton);
                        this.reStart();
                        // this.show(false, null);
                    });
                } else {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('errorsMade'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'error',
                        showConfirmButton: true,
                    }).then((result) => {
                        that.generatePopupErrorPlacement();
                        //  this.refGame.global.util.showTutorialInputs('btnResetTr');
                        // this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.resetExercice.bind(this));

                    });
                }
                /*
*/
            }
        } else {
            swal.fire({
                title: "Tu n'as pas fini !",
                text: "Tu pourras valider ton exercice lorsque tu auras plac\u00e9 toutes les images ;)",
                icon: "warning",
            });
        }

    }

    /**
     * cette méthode va ajouter une variable d'erreur a la liste.
     * @param   justification Boolean qui nous indique si l'erreur est une justification ou un placement
     * @param   img l'image de l'activité
     * @param   solution String qui nous donne la solution
     */
    addErrorToList(justification, img, solution, rep) {
        if (justification) {
            this.validatorPopup = justification
        }
        var error = {
            isJustification: justification,
            falseAnswser: rep,
            img: img,
            rightAnswser: solution
        };
        //ajout à la liste 
        this.listError.push(error);

    }

    /**
     * Méthode qui se charge de récupérer de l'image grace au numéro du "priteToMove"
     * @param count le numéro du "spriteToMove" ou il faut aller chercher le nom
     * @returns une image sous format "base64"
     */
    getImageForPopUpWithCount(count) {
        let img = this.exercice.spriteToMove[count].sprite;
        return this.getImageForPopUpWithName(img);
    }
    /**
     *  Méthode pour obtenir l'image avec le nom du "sprite"
     * @param name le nom du sprite
     * @returns une image sous format "base64"
     */
    getImageForPopUpWithName(name) {
        return this.refGame.global.resources.getImage(name);
    }

    /**
     * Cette méthode sere appelée quand l'utilisateur aura appuie sur le bouton "btnResetTr" elle va récharger le même
     * exercice.
     */
    resetExercice() {
        this.clear();
        this.elements = [];
        this.newGame(this.id);
        // this.listError = [];
    }
    //    refreshLang(lang) {
    //    }
    refreshFont(isOpenDyslexic) {
    }
    onShapeClick(shape) {
    }
    onDragEnd() {
    }
}