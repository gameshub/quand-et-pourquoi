class Play extends Interface {

    constructor(refGame) {

        super(refGame, "play");


    }

    show() {
        this.exeSelected = -1;
        this.clear();
        this.exercices = {};
        this.elements = {
            title: new PIXI.Text('', { fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left' }),
            scrollPane: new ScrollPane(0, 60, 600, 480),
            btn: new Button(300, 570, 'Jouer', 0x0088FF, 0x000000, true)
        };
        this.elements.btn.setOnClick(function () {

            if (this.exeSelected != -1) {
                console.log(this.exercice[this.exeSelected])
                this.refGame.trainMode.init(false, 'playMode');
                console.log(this.exercice[this.exeSelected].exercice)
                this.refGame.trainMode.setRefPlay(this);
                this.refGame.trainMode.show(this.exercice[this.exeSelected].exercice);
            }
        }.bind(this));
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 30;
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    init() {
        console.log("init")
        this.refGame.global.util.hideTutorialInputs('btnReset');
        super.init();
    }

    startExercices(lang) {
        //choose exe
        this.exercice = this.refGame.global.resources.getExercicesEleves();
        let tg = new ToggleGroup('single')
        if (this.exercice) {
            this.exercice = JSON.parse(this.exercice);
            console.log(Object.keys(this.exercice))
            console.log(this.exercice)
            for (let exID of Object.keys(this.exercice)) {
                let ex = this.exercice[exID];
                console.log("_------------------------------------_")
                console.log(ex)
                if (ex.exercice) {
                    let label = (ex.name);
                    let btn = new ToggleButton(0, 0, label, false, 580);
                    btn.enable();
                    tg.addButton(btn);
                    btn.setOnClick(function () {
                        this.exeSelected = exID;
                    }.bind(this));
                    this.elements.scrollPane.addElements(btn);
                }

            }
            this.elements.scrollPane.init();
        }

    }



    ///////////////////////
    refreshLang(lang) {
        this.startExercices(lang)
    }

}