/**
 * @classdesc Asset checkbox
 * @author Huwiler Paul
 * @version 1.0
 */
class ComboBox extends Asset {
    constructor(x, y, list, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {list} la liste des données */
        this.list = list;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /**@type {double} la taille */
        this.height = 0;
        this.autofit = false;
        this.width = width;
    }
/**
 * @returns la taille
 */
    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


}