class Shape extends Asset {
    constructor(x, y, width, height, position, image, name, validx, validy, idValidRes, toMove, onDBClick, onMove = null, endMove = null) {
        super();
        this.x = x;
        this.y = y;
        this.baseX = x;
        this.baseY = y;
        this.width = width;
        this.height = height;
        this.positionInTab = position;
        this.image = image;
        this.name = name;
        this.onDBClick = onDBClick;
        this.toMove = toMove;
        this.onMove = onMove;
        this.endMove = endMove;
        this.stopMove = false;
        this.alpha = 1;
        this.container = new PIXI.Container();
        this.isFirstClick = true;
        this.data = undefined;
        this.shape = undefined;
        if (image) {
            this.display();
        }
        this.validx = validx;
        this.validy = validy;
        this.idValidRes = idValidRes;
        this.onClick = function () {
            this.isClicked = true;
        };
        this.init();
    }
    init() {
        if (this.shape) {
            //this.setText(this.text);
            this.shape.interactive = true;
            this.shape.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }


    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        //Créer un "Sprite" avec une image en base64.
        //Le type d'image est "IMG" donc pour récupérer l'image, il faut faire
        //this.image.image.src
        this.shape = PIXI.Sprite.fromImage(this.image.image.src);
        this.shape.alpha = this.alpha;
        //Définir sa position et l'ancre sur l'image, ici au centre de l'image
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.width;
        this.shape.height = this.height;

        //Ajouter les listeners
        this.shape.interactive = true;
        this.shape.on('pointerdown', this.onClick.bind(this));
        this.shape.on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));
        //Ajouter notre forme sur l'élément graphique
        this.container.addChild(this.shape);
    }

    onClick() {
        if (!this.shape.stopMove) {
            if (this.isFirstClick) {
                this.isFirstClick = false;
                //Annulation du double clique après 500ms
                setTimeout(function () {
                    this.isFirstClick = true;
                }.bind(this), 500);
            } else {
                this.isFirstClick = false;
                if (this.onDBClick)
                    this.onDBClick(this);
            }
        }
    }

    onDragStart(event) {
        this.data = event.data;
    }

    onDragEnd() {
        if (!this.shape.stopMove) {
            this.data = null;
            //console.log("x: " + this.getX() + " y: " + this.getY());
            if (this.endMove) {
                this.endMove(this);
            }
        }
    }

    onDragMove() {
        if (!this.shape.stopMove) {
            if (this.data) {
                //Regarde si la forme peut bouger
                if (this.toMove == true) {
                    if (!this.stopMove) {
                        //Position actuelle
                        let newPosition = this.data.getLocalPosition(this.shape.parent);

                        //Empêcher la forme de sortir du canvas et de devenir invisible
                        newPosition.x = newPosition.x < 0 ? 0 : newPosition.x;
                        newPosition.y = newPosition.y < 0 ? 0 : newPosition.y;
                        newPosition.x = newPosition.x > 600 ? 600 : newPosition.x;
                        newPosition.y = newPosition.y > 600 ? 600 : newPosition.y;

                        //Mise à jour la position de la forme
                        this.y = newPosition.y;
                        this.x = newPosition.x;
                        this.display();

                        //Appel au callback s'il y en a un
                        if (this.onMove) {
                            this.onMove(this);
                        }
                    }
                }
            }
        }
    }

    setName(name) {
        this.name = name;
    }
    getName() {
        return this.name;
    }
    setStopMove(stopMove){
        this.shape.stopMove = stopMove
    }
    setOnClick(onClick) {
        this.onClick = onClick;
        this.display();
    }
    getvalidx() {
        return this.validx;
    }

    setvalidx(value) {
        this.validx = value;
    }

    getvalidy() {
        return this.validy;
    }

    setvalidy(value) {
        this.validy = value;
    }

    getidValidRes() {
        return this.idValidRes;
    }
    setAplha(alpha) {
        this.alpha = alpha;
        this.display();
    }

    setidValidRes(value) {
        this.idValidRes = value;
    }

    getposition() {
        return this.positionInTab;
    }

    setposition(value) {
        this.positionInTab = value;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }
    setImage(image) {
        this.image = image;
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    setStopMove(stopMove) {
        this.stopMove = stopMove;
    }

    setShape(sprite) {
        this.shape = sprite;
    }

    getStopMove() {
        return this.stopMove;
    }

    getBaseX() {
        return this.baseX;
    }

    getBaseY() {
        return this.baseY;
    }

    setWidth(w) {
        this.width = w;
        this.display();
    }

    setHeight(h) {
        this.height = h;
        this.display();
    }

    setBaseX(baseX) {
        this.baseX = baseX;
        this.setX(baseX);
    }

    setBaseY(baseY) {
        this.baseY = baseY;
        this.setY(baseY);
    }
}