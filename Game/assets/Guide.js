
class Guide extends Asset {
    constructor(x, y, width, date, shape = null, lastEvent = null) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.date = date;
        this.shape = shape;
        this.isFirstClick = true;
        this.clicked = false;
        this.container = new PIXI.Container();
        this.lastEvent = lastEvent;
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        /*
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;
        */
        this.container.interactive = true;
        this.container.buttonMode = true;
        this.container.removeChildren();
        this.container.x = this.x;
        this.container.y = this.y;
        this.container.width = this.width;
        this.container.height = this.width;
        //Ajouter notre forme sur l'élément graphique
        this.graphics = new PIXI.Graphics();
        this.graphics.beginFill(0xffffff);
        this.graphics.lineStyle(2, 0x000000, 1);
        this.graphics.endFill();
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', this.onClick.bind(this));
        this.container.addChild(this.graphics);
        //this.container.on('pointerdown', this.onClick.bind(this));
    }

    onClick() {
   
        this.clicked = true;
        
        if (this.lastEvent) {
            this.clicked = true;
            this.lastEvent.bind(this);
        }
        
        this.clicked = true;
        if (this.isFirstClick) {
            this.isFirstClick = false;
            setTimeout(function () {    
                this.clicked = true;    
                this.isFirstClick = true;
            }.bind(this), 150)
        } else {
            this.isFirstClick = true;
            if (this.lastEvent) {
                this.clicked = true;
                this.lastEvent(this);
            }
        }

    }

    setLastEvent(lastEvent) {
        this.lastEvent = lastEvent;
    }
    getClicked() {
        return this.Clicked;
    }

    getPixiChildren() {
        return [this.container];
    }

    getDate() {
        return this.date;
    }

    setDate(date) {
        this.date = date;
    }
    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }

    getWidth() {
        return this.width
    }

    setWidth(width) {
        this.width = width;
    }

    getValue() {
        return this.value;
    }

    setValue(value) {
        this.value = value;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    getShape() {
        return this.shape;
    }

    setShape(shape) {
        this.shape = shape;
    }
}