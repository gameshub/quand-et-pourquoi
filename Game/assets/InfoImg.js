class InfoImg extends Asset {

    constructor(x, y, width, height, imgBackground, name,textId,onDBClick ) {
        super();
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = height;
        this.imgBackground = imgBackground;
        this.name = name;
        this.textID = textId;
        this.isFirstClick = true;
        this.onDBClick = onDBClick;
        this.container = new PIXI.Container();
        this.imgStatic = undefined;
        this.init();

    }

    init() {
        this.container.removeChildren();
        this.imgStatic = PIXI.Sprite.fromImage(this.imgBackground);
        this.imgStatic.anchor.x = 0.5;
        this.imgStatic.anchor.y = 0.5;
        this.imgStatic.x = this.x;
        this.imgStatic.y = this.y;
        this.imgStatic.width = this.width;
        this.imgStatic.height = this.height;

        this.imgStatic.interactive = true;
        this.imgStatic.on('pointerdown', this.onClick.bind(this));

        this.container.addChild(this.imgStatic);
    }

    onClick() {
           if (this.onDBClick) {
               this.onDBClick(this);
           }
    }

   getTextId() {
       return this.textID;
   }

   setTextId(value) {
       this.textID = value;
   }

    getPixiChildren() {
        return [this.container];
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
    }

    setX(x) {
        this.x = x;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}