/**
 * @interface
 * @classdesc Interface Asset, défini un asset, soit un élément du canvas comme un bouton ou une grille
 * @author Vincent Audergon
 * @version 1.0
 */
class Asset {

    /**
     * Retourne la liste des éléments PIXI d'un asset
     * @return {Object[]} les éléments PIXI
     */
    getPixiChildren(){}

    /**
     * Retourne la position y du composant
     * @return {number} posY
     */
    getY(){}
    /**
     * Retourne la position x du composant
     * @return {number} posX
     */
    getX(){}
    setY(y){}
    setX(x){}
    getWidth(){}
    getHeight(){}
    setVisible(visible){}

}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class Button extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        this.alpha = 1;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: fgColor, align: 'center' })
        this.isClicked = false
        this.height = 0;
        this.autofit = autofit;
        this.width = width;
        this.isSetted = false;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            this.isClicked = true;
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update() {
        let buttonWidth = this.getWidth();
        let buttonHeight = this.lbl.height * 1.5;
        if (this.graphics) {
            if (!this.isSetted) {
                this.height = buttonHeight;
                this.graphics.clear();
                this.graphics.beginFill(this.bgColor,  this.alpha);
                this.graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
                this.graphics.endFill();
                this.lbl.anchor.set(0.5);
                this.lbl.x = this.x;
                this.lbl.y = this.y;
            } else {
                //TODO:
                this.graphics.clear();
                this.graphics.beginFill(this.bgColor);
                this.graphics.drawRect(this.x, this.y, this.width, this.height);
                this.graphics.endFill();
                this.lbl.anchor.set(0.5);
                this.lbl.x = this.x;
                this.lbl.y = this.y;
            }
        }

    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setAlpha(alpha){
        
        this.alpha = alpha;
        this.update();
    }
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }

    getIsClicked(){
        return this.isClicked;
    }

    setWidth(w) {
        this.width = w;
        this.update();
    }
    setHeight(h) {
        //this.isSetted = true
        this.height = h;
        this.update();
    }

    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }

    setbgcolor(color) {
        this.bgColor = color;
        this.update();
    }

    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }

}
/**
 * @classdesc Asset Calendrier
 * @author Dorian Barras
 * @version 1.0
 */
class Calendar extends Asset {

    /**
     * Constructeur de l'asset Calendar
     */
    constructor(nbJours, nbSemaines, dateDebut) {
        super();

        // Variables modifiables pour façonner le calendrier à sa guise

        this.baseX = 150;
        this.baseY = 0;
        this.tailleCellule = 50;
        this.nbSemaines = nbSemaines;
        this.dateDebut = dateDebut;
        this.tempDate = this.dateDebut;
        this.nbJours = nbJours;
        this.baseJours = ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"];
        this.baseMois = ["janvier", "f\u00e9vrier", "mars", "avril", "mai", "juin", "juillet", "ao\u00fbt", "sept", "octobre", "novembre", "d\u00e9c"];
        this.moments = ["Matin", "Apr\u00e8s-\nmidi", "Soir"];
        this.jours = [];
        this.calendarElments = [];

        this.init();
    }

    init() {

        // Boucles imbriquées permettant de formatter les jours dans un tableau de tableau de jours : "Me 5"

        for (let j = 0; j < this.nbSemaines; j++) {
            this.jours[j] = [];
            for (let i = 0; i < this.nbJours; i++) {
                let jourDeLaSemaine = this.tempDate.getDay();
                let jourFormate = `${this.baseJours[jourDeLaSemaine]} ${this.tempDate.getDate()}\n${this.baseMois[this.tempDate.getMonth()]}`;
                this.jours[j][i] = jourFormate;
                this.tempDate.setDate(this.tempDate.getDate() + 1);
            }

        }

        this.update();


    }

    update() {


         // Variables permettant de décaler chaque rectangle (case) du bon nombre de pixels lors des itérations des boucles
 
         let addx = 0;
         let addy = 0;
         let addyTexte = 0;    // Cette variable permet de déplacer les textes si le calendrier comporte plusieurs semaines
         let ecartSemaine = this.nbSemaines * 10 / (this.nbSemaines - 1); // Cette variable permet de faire en sorte que l'écart entre les semaines soit petit proportionnellement au nombre de semaines totales
 
 
         // Variables utilisées pour insérer des rectangles et des textes dans le FrameWork lors des itérations des boucles
 
         var rect = new PIXI.Graphics();
         rect.lineStyle(1, 0x000000, 1);
 
 
         let tabTextCal = [];
         // var textcal = new PIXI.Text();
         const style = new PIXI.TextStyle({
             fontFamily: 'Arial',
             fontSize: (this.tailleCellule / 4.1666666666)
         });
 
 
 
         // Boucles embriquées pour pouvoir réaliser le tableau
 
         for (let h = 0; h < this.jours.length; h++) {
             for (let i = 0; i < this.moments.length + 1; i++) {
                 for (let j = 0; j < this.jours[h].length + 1; j++) {
                     rect.drawRect(this.baseX + 1 + addx, this.baseY + 1 + addy, this.tailleCellule, this.tailleCellule);
                     
                     addx += this.tailleCellule;
                     if (j != 0 && i == 0) {
                         var textcal = new PIXI.Text();
                         textcal = new PIXI.Text(this.jours[h][j - 1], style);
                         textcal.x = this.baseX + (j * this.tailleCellule) + 5;
                         textcal.y = this.baseY + (this.tailleCellule / 3) + addyTexte - 5;
                         tabTextCal.push(textcal);
                     }
                     if (j == 0 && i >= 1) {
                         var textcal = new PIXI.Text();
                         textcal = new PIXI.Text(this.moments[i - 1], style);
                         textcal.x = this.baseX + 5;
                         textcal.y = this.baseY + i * this.tailleCellule + (this.tailleCellule / 3) + addyTexte;
                         tabTextCal.push(textcal);
                     }
                 }
                 addx = 0;
                 addy += this.tailleCellule;
             }
             addy += ecartSemaine;
             addyTexte += ((this.moments.length + 1 ) * this.tailleCellule + ecartSemaine);
         }


         // Formattage de la valeur de retour

         this.calendarElements = [];
         this.calendarElements[0] = rect;
         this.calendarElements[1] = tabTextCal;
    }

    getCalendarElements(){
        return this.calendarElements;
    }
}
class CheckBox extends Asset {

    /**
     * Créé le composant graphic
     */
    constructor(x = 0, y = 0, text = '', sizeFactor=1) {
        super();
        this.y = y;
        this.x = x;
        this.text = text;
        this.sizeFactor = sizeFactor;

        this.selected = false;

        this.elements = {
            square: new PIXI.Graphics(),
            line1: new PIXI.Graphics(),
            line2: new PIXI.Graphics(),
            text: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'left', breakWords:true, wordWrap:true})
        };

        this.elements.square.interactive = true;
        this.elements.square.buttonMode = true;
        this.elements.square.on('pointerdown', function () {
            this.select();
        }.bind(this));

        this.draw();
    }


    /**
     * Modifie la position de la checkbox.
     * @param x {number} Position X
     * @param y {number} Position Y
     */
    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.draw();
    }

    setText(value) {
        this.text = value;
        this.draw();
    }

    /**
     * Affiche ou cache le composant.
     * @param visible {boolean} Est visible ?
     */
    setVisible(visible) {
        if (!visible)
            for (let element of this.getPixiChildren())
                element.visible = false;
        else {
            this.elements.text.visible = true;
            this.elements.square.visible = true;
            this.select(false);
        }
    }

    draw() {
        let line1StartX = this.x + 3*this.sizeFactor;
        let line1StartY = this.y + 20*this.sizeFactor;
        let line1EndX = this.x + 13*this.sizeFactor;
        let line1EndY = this.y + 30*this.sizeFactor;
        let line2StartX = line1EndX-5*this.sizeFactor/4;
        let line2StartY = line1EndY+5*this.sizeFactor/4;
        let line2EndX = this.x + 35*this.sizeFactor;
        let line2EndY = this.y + 8*this.sizeFactor;
        
        this.elements.square.clear();
        this.elements.square.lineStyle(1, 0x000000, 1);
        this.elements.square.beginFill(0xe0e0e0, 0.25);
        this.elements.square.drawRoundedRect(this.x, this.y, 38*this.sizeFactor, 38*this.sizeFactor, 5);
        this.elements.square.endFill();
        this.elements.line1.clear();
        this.elements.line1.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line1StartX, line1StartY).lineTo(line1EndX, line1EndY);
        this.elements.line2.clear();
        this.elements.line2.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line2StartX, line2StartY).lineTo(line2EndX, line2EndY);
        this.elements.text.text = this.text;
        this.elements.text.style.wordWrapWidth =600-Math.floor( this.x + 5 + 38*this.sizeFactor +25);
        this.elements.text.anchor.set(0.0);
        this.elements.text.x = this.x + 38*this.sizeFactor+5;
        this.elements.text.y = this.y + (38*this.sizeFactor-this.elements.text.height)/2;

    }

    getPixiChildren() {
        let objects = [];
        for (let element of Object.keys(this.elements)) {
            objects.push(this.elements[element]);
        }
        return objects;
    }

    select(toggle = true) {
        if (toggle)
            this.selected = !this.selected;
        this.elements.line1.visible = this.selected;
        this.elements.line2.visible = this.selected;
    }

    isChecked() {
        return this.selected;
    }


    updateFont(font){
        this.elements.text.style.fontFamily = font;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


    setY(y) {
        this.y = y;
        this.setPosition(this.x, this.y);
    }

    setX(x) {
        this.x = x;
        this.setPosition(this.x, this.y);
    }
}
/**
 * @classdesc Asset checkbox
 * @author Huwiler Paul
 * @version 1.0
 */
class ComboBox extends Asset {
    constructor(x, y, list, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {list} la liste des données */
        this.list = list;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /**@type {double} la taille */
        this.height = 0;
        this.autofit = false;
        this.width = width;
    }
/**
 * @returns la taille
 */
    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


}
/**
 * @classdesc Asset grille de dessin, retourne une séries de points lorsque l'utilisateur a dessiné un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class DrawingGrid extends Asset {

    /**
     * Constructeur de la grille de dessin
     * @param {int} col Le nombre de colonnes qui composent la grille
     * @param {int} lines Le nombre de lignes qui composent la grille
     * @param {double} width La largeur / hauteur entre chaque noeuds de la grille
     * @param {Pixi.Stage} stage Le stage Pixi
     * @param {function} onShapeCreated La fonction de callback appelée lorsqu'une forme a été dessinée
     */
    constructor(col, lines, width, stage, onShapeCreated) {
        super();
        /** @type {int} le nombre de colonnes */
        this.col = col;
        /** @type {int} le nombre de lignes */
        this.lines = lines;
        /** @type {double} la largeur / hauteur d'une cellule */
        this.width = width;
        /** @type {PIXI.Stage} le stage PIXI sur lequel dessiner les éléments de la grille */
        this.stage = stage;
        /** @type {Node[]} la liste des noeuds qui composent la grille */
        this.nodes = [];
        /** @type {Point[]} la liste des points de la forme en cours de dessin */
        this.points = [];
        /** @type {PIXI.Graphics} la liste des lignes dessinées */
        this.strLines = [];
        /** @type {Point} l'origine de la forme en cours de dessin */
        this.origin = new Point(0, 0);
        /** @type {Node} le dernier noeud rencontré lors du dessin */
        this.lastnode = undefined;
        /** @type {PIXI.Graphics} le trait qui suit le curseur lors d'un dessin */
        this.line = undefined;
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = onShapeCreated;
        /** @type {boolean} si un dessin est en cours */
        this.drawing = false;
        this.init();
    }

    /**
     * Initialise la grille de dessin
     */
    init() {
        this.stage.interactive = true;
        this.stage.on('pointermove', this.onPointerMove.bind(this));
        this.stage.on('pointerup', this.onReleased.bind(this));
        for (let c = 0; c < this.col; c++) {
            for (let l = 0; l < this.lines; l++) {
                this.nodes.push(new Node(c * this.width, l * this.width, this.width / 4.5, this));
            }
        }
    }

    /**
     * Réinitialise la grille de dessin
     */
    reset() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
    }

    /**
     * Créer le prochain point du polygone en cours de dessin
     * @param {Node} node le {@link Node} qui défini le nouveau point
     * @return {Point} le nouveau {@link Point}
     */
    createNextPoint(node) {
        let p = new Point(node.x / this.width - this.origin.x, node.y / this.width - this.origin.y);
        p.name = String.fromCharCode(65 + this.points.length);
        return p;
    }

    /**
     * Vérifie qu'un point ne soit pas deja existant dans la grille
     * @param {Point} p le point à controller
     * @return {boolean} si le point existe déjà
     */
    containsPoint(p) {
        for (let point of this.points) {
            if (point.x === p.x && point.y === p.y) return true;
        }
        return false;
    }

    /**
     * Créer le trait affiché sur la grille lorsque l'utilisateur déssine
     * @param {Node} node le noeud de départ
     */
    initLine(node) {
        this.line = new PIXI.Graphics();
        this.stage.addChild(this.line);
        this.line.lineStyle(6, 0xFF0000, 1);
        this.line.moveTo(node.center().x, node.center().y);
    }

    /**
     * Dessine une ligne le dernier noeud rencontré et le noeud donné en argument
     * @param {Node} node le noeud jusqu'au quel faire le trait
     */
    drawStrLine(node) {
        this.stage.removeChild(this.line);
        this.initLine(node);
        let straightLine = new PIXI.Graphics();
        this.strLines.push(straightLine);
        this.stage.addChild(straightLine);
        straightLine.lineStyle(6, 0xFF, 1);
        straightLine.moveTo(this.lastnode.center().x, this.lastnode.center().y);
        straightLine.lineTo(node.center().x, node.center().y);
        this.lastnode = node;
        if (this.onLineDrawn) this.onLineDrawn();
    }

    /**
     * Retourne la liste des éléments PIXI de la grille de dessin
     * @return {Object[]} les éléments PIXI qui composent la grille
     */
    getPixiChildren() {
        let children = [];
        for (let n of this.nodes) {
            if (n.graphics) children.push(n.graphics);
            if(n.point) children.push(n.point);
        }
        return children;
    }

    /**
     * Méthode de callback appelée lorsqu'un click est effectué sur un noeud de la grille.
     * Défini le noeud comme étant le noeud de départ et crée le trait de dessin
     * @param {Event} e l'événement JavaScript
     * @param {Node} node le noeud sur lequel on a clické
     */
    onNodeClicked(e, node) {
        this.lastnode = node;
        this.initLine(node);
        this.drawing = true;
        this.points.push();
        this.origin = this.createNextPoint(node);
        let p = new Point(0, 0);
        p.name = this.origin.name;
        this.points.push(p);
    }

    /**
     * Méthode de callback appelée lorsque le click est relâché
     * @param {Event} e L'événement JavaScript
     */
    onReleased(e) {
        this.drawing = false;
        this.stage.removeChild(this.line);
        this.reset();
    }

    /**
     * Méthode de callback appelée lorsque le curseur bouge.
     * Déssine le trait à la position du curseur
     * @param {Event} e L'événement JavaScript
     */
    onPointerMove(e) {
        if (this.drawing) {
            let position = e.data.getLocalPosition(this.stage);
            this.line.lineTo(position.x, position.y);
            for (let n of this.nodes) {
                if (n.graphics.containsPoint(e.data.getLocalPosition(n.graphics.parent))) this.onNodeEncountered(e, n);
            }
        }
    }

    /**
     * Méthode de callback appelée lorsque que le curseur touche un noeud de la grille.
     * Vérifie si la forme est terminée ou si il faut dessiner un trait droit entre ce noeud et le noeud de départ.
     * @param {Event} e L'événement JavaScript
     * @param {Node} node Le noeud de la grille touché
     */
    onNodeEncountered(e, node) {
        if (this.drawing && this.lastnode !== node) { // Si le noeud rencontré n'est pas le dernier noeud
            let point = this.createNextPoint(node);
            this.drawStrLine(node);
            let len = this.points.length;
            // console.log("Diagonale : ");
            // if (len >= 2) {
            //     let val1 = this.points[len - 2].sub(this.points[len - 1]);
            //     let val2 =  this.points[len - 1].sub(point);
            //     console.log("############################################################");
            //     console.log("Point len - 2 : ");
            //     console.log(this.points[len - 2]);
            //     console.log("Point len - 1 : ");
            //     console.log(this.points[len - 1]);
            //     console.log("Point :");
            //     console.log(point);
            //     console.log("len-2 - len-1 :");
            //     console.log(val1);
            //     console.log("len-1 - len :");
            //     console.log(val2);
            //
            //     if ((this.points[len - 1].x === this.points[len - 2].x && this.points[len - 1].x === point.x) || //Al. horizontal
            //         (this.points[len - 1].y === this.points[len - 2].y && this.points[len - 1].y === point.y) || //Al. vertical
            //         (val1.equals(val2))) { //Al. diagonale
            //         //Si trois points sont allignés on ne crée pas un nouveau mais on déplace le dernier
            //         if (point.equals(this.points[0]) && len >= 3) {
            //             //Si c'est le dernier point, on efface le dernier
            //             this.points.pop();
            //             this.pointsCalcul.push(point);
            //         } else if (!this.containsPoint(point)) {
            //             //Si c'est pas le dernier et qu'il n'exite pas encore on déplace le dernier
            //             point.name = this.points[len - 1].name;
            //             this.points[len - 1] = point;
            //             this.pointsCalcul.push(point);
            //         }
            //     } else if (!this.containsPoint(point)) {
            //         this.points.push(point);
            //         this.pointsCalcul.push(point);
            //     }
            // }
            if (!this.containsPoint(point)) {
                this.points.push(point);
            }
            if (this.points.length >= 3 && point.equals(this.points[0])) { //Sinon, si il correspond à l'origine
                //Fin de la forme

                this.drawStrLine(node);
                this.onShapeCreated(new Shape(0, 0, this.points, this.width, 'unknown', {}, this.points[0]));
                this.reset();
            }
        }
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une ligne est dessinée
     * @param {function} onLineDrawn La fonction de callback
     */
    setOnLineDrawn(onLineDrawn) {
        this.onLineDrawn = onLineDrawn;
    }

    /**
     * Défini la fonction de callback lorsqu'un polygone est créé sur le canvas
     * @param {function} onShapeCreated La fonction de callback
     */
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}

class Guide extends Asset {
    constructor(x, y, width, date, shape = null, lastEvent = null) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.date = date;
        this.shape = shape;
        this.isFirstClick = true;
        this.clicked = false;
        this.container = new PIXI.Container();
        this.lastEvent = lastEvent;
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        /*
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;
        */
        this.container.interactive = true;
        this.container.buttonMode = true;
        this.container.removeChildren();
        this.container.x = this.x;
        this.container.y = this.y;
        this.container.width = this.width;
        this.container.height = this.width;
        //Ajouter notre forme sur l'élément graphique
        this.graphics = new PIXI.Graphics();
        this.graphics.beginFill(0xffffff);
        this.graphics.lineStyle(2, 0x000000, 1);
        this.graphics.endFill();
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', this.onClick.bind(this));
        this.container.addChild(this.graphics);
        //this.container.on('pointerdown', this.onClick.bind(this));
    }

    onClick() {
   
        this.clicked = true;
        
        if (this.lastEvent) {
            this.clicked = true;
            this.lastEvent.bind(this);
        }
        
        this.clicked = true;
        if (this.isFirstClick) {
            this.isFirstClick = false;
            setTimeout(function () {    
                this.clicked = true;    
                this.isFirstClick = true;
            }.bind(this), 150)
        } else {
            this.isFirstClick = true;
            if (this.lastEvent) {
                this.clicked = true;
                this.lastEvent(this);
            }
        }

    }

    setLastEvent(lastEvent) {
        this.lastEvent = lastEvent;
    }
    getClicked() {
        return this.Clicked;
    }

    getPixiChildren() {
        return [this.container];
    }

    getDate() {
        return this.date;
    }

    setDate(date) {
        this.date = date;
    }
    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }

    getWidth() {
        return this.width
    }

    setWidth(width) {
        this.width = width;
    }

    getValue() {
        return this.value;
    }

    setValue(value) {
        this.value = value;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    getShape() {
        return this.shape;
    }

    setShape(shape) {
        this.shape = shape;
    }
}
class ImgBack extends Asset {

    constructor(x, y, width, height, imgBackground, name, onDBClick) {
        super();
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = height;
        this.imgBackground = imgBackground;
        this.name = name;
        this.onDBClick = onDBClick;
        this.isFirstClick = true;
        this.container = new PIXI.Container();
        this.imgStatic = undefined;
        this.init();
    }

    init() {
        this.container.removeChildren();
        this.imgStatic = PIXI.Sprite.fromImage(this.imgBackground);
        this.imgStatic.anchor.x = 0.5;
        this.imgStatic.anchor.y = 0.5;
        this.imgStatic.x = this.x;
        this.imgStatic.y = this.y;
        this.imgStatic.width = this.width;
        this.imgStatic.height = this.height;
        this.imgStatic.interactive = true;
        this.imgStatic.on('pointerdown', this.onClick.bind(this));
        //ici


        this.container.addChild(this.imgStatic);
    }


    onClick() {
        if (this.isFirstClick) {
            this.isFirstClick = false;
            setTimeout(function () {
                this.isFirstClick = true;
            }.bind(this), 250)
        } else {
            this.isFirstClick = true;
            if (this.onDBClick) {
                this.onDBClick(this);
            }
        }
    }



    setOnClick(onClick){
        this.onDBClick = onClick;
    }

    getPixiChildren() {
        return [this.container];
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
    }

    setX(x) {
        this.x = x;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}
class InfoImg extends Asset {

    constructor(x, y, width, height, imgBackground, name,textId,onDBClick ) {
        super();
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = height;
        this.imgBackground = imgBackground;
        this.name = name;
        this.textID = textId;
        this.isFirstClick = true;
        this.onDBClick = onDBClick;
        this.container = new PIXI.Container();
        this.imgStatic = undefined;
        this.init();

    }

    init() {
        this.container.removeChildren();
        this.imgStatic = PIXI.Sprite.fromImage(this.imgBackground);
        this.imgStatic.anchor.x = 0.5;
        this.imgStatic.anchor.y = 0.5;
        this.imgStatic.x = this.x;
        this.imgStatic.y = this.y;
        this.imgStatic.width = this.width;
        this.imgStatic.height = this.height;

        this.imgStatic.interactive = true;
        this.imgStatic.on('pointerdown', this.onClick.bind(this));

        this.container.addChild(this.imgStatic);
    }

    onClick() {
           if (this.onDBClick) {
               this.onDBClick(this);
           }
    }

   getTextId() {
       return this.textID;
   }

   setTextId(value) {
       this.textID = value;
   }

    getPixiChildren() {
        return [this.container];
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
    }

    setX(x) {
        this.x = x;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}
class ScrollPane extends Asset {


    constructor(x, y, width, height) {
        super();
        this.x = x | 0;
        this.y = y | 0;
        this.width = width | 0;
        this.height = height | 0;
        this.index = 1;

        this.maxIndex = 0;

        this.touchScrollLimiter = 0;
        this.lastTouch = 0;
        this.data = {};
        this.dragging = false;

        this.elements = [];

        this.graphics = {
            elementContainer: new PIXI.Container(),
            scrollBar: new PIXI.Graphics(),
            scrollButton: new PIXI.Graphics()
        };

        this.graphics.scrollButton
            .on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));


        let canvas = document.getElementById('canvas');
        canvas.addEventListener('wheel', function (event) {
            this.index += (event.deltaY > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length));
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }.bind(this));
        canvas.addEventListener('touchmove', function (event) {
            if (this.lastTouch === 0)
                this.lastTouch = event.touches[0].clientY;
            this.touchScrollLimiter += (this.lastTouch - event.touches[0].clientY);
            this.lastTouch = event.touches[0].clientY;
            if ((this.touchScrollLimiter > (this.maxIndex / this.elements.length))
                || (this.touchScrollLimiter < -(this.maxIndex / this.elements.length))) {
                console.log(this.touchScrollLimiter);
                this.index += this.touchScrollLimiter > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length);
                this.touchScrollLimiter = 0;
                if (this.index > this.height - 31)
                    this.index = (this.height - 31);
                else if (this.index <= 1)
                    this.index = 1;
                this.scroll();
            }
        }.bind(this));
        canvas.addEventListener('touchend', function (event) {
            this.lastTouch = 0;
        }.bind(this));
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.init();
    }

    addElements(...elements) {
        for (let element of elements)
            this.elements.push(element);
    }

    init() {

        console.log(this.elements);

        this.graphics.scrollBar.clear();
        this.graphics.scrollBar.beginFill(0xDDDDDD);
        this.graphics.scrollBar.drawRect(this.x + this.width - 18, this.y, 18, this.height);
        this.graphics.scrollBar.endFill();

        this.graphics.scrollButton.clear();
        this.graphics.scrollButton.beginFill(0x999999);
        this.graphics.scrollButton.drawRect(this.x + this.width - 17, this.y + 1, 16, 30);
        this.graphics.scrollButton.endFill();

        this.graphics.scrollButton.interactive = true;
        this.graphics.scrollButton.buttonMode = true;

        let y = this.y;
        for (let element of this.elements) {
            if (element instanceof Asset) {
                element.setY(y);
                element.setX((this.width-20) / 2);
                y += element.getHeight();
                element.setVisible(this.checkIsInContainer(element.getY(), element.getHeight() / 2));
                for (let pc of element.getPixiChildren())
                    this.graphics.elementContainer.addChild(pc);
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                element.y = y;
                y += (typeof element.height !== 'undefined' ? element.height : 25);
                element.visible = this.checkIsInContainer(element.y);
                this.graphics.elementContainer.addChild(element);
                element.x = this.width / 2;
            }
            y += 5;
        }
        this.maxIndex = y;

        this.graphics.scrollButton.visible = (this.height < this.maxIndex);
        this.graphics.scrollBar.visible = (this.height < this.maxIndex);

        this.scroll();
    }

    clear() {
        this.elements = [];
    }

    scroll() {
        if (this.elements.length === 0)
            return;

        this.graphics.scrollButton.position.y = this.index;

        let firstDisplayedIndex = Math.floor((this.elements.length - 1) * (this.index / this.height));

        let posY = 1 + this.y + this.elements[firstDisplayedIndex].getHeight()/2;

        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            if (element instanceof Asset) {
                if (i < firstDisplayedIndex)
                    element.setVisible(false);
                else {
                    element.setY(posY);
                    element.setVisible(this.checkIsInContainer(element.getY()));
                    posY += element.getHeight() + 5;
                }
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                if (i < firstDisplayedIndex)
                    element.visible = false;
                else {
                    element.y = posY;
                    element.visible = this.checkIsInContainer(element.y);
                    posY += element.height + 5;
                }
            }
            if(element instanceof ToggleButton)
                element.updateView();
        }
    }


    checkIsInContainer(posY, compensation = 0) {
        return (posY < (this.y + this.height) && (posY > (this.y + compensation)));
    }

    getPixiChildren() {
        let elements = [];
        for (let e in this.graphics)
            if (this.graphics[e] instanceof Asset)
                for (let element of this.graphics[e].getPixiChildren())
                    elements.push(element);
            else
                elements.push(this.graphics[e]);
        return elements;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {

    }

    onDragStart(event) {
        this.data = event.data;
        this.graphics.scrollButton.alpha = 0.5;
        this.dragging = true;
    }

    onDragEnd() {
        this.graphics.scrollButton.alpha = 1;
        this.dragging = false;
        this.data = null;
    }

    onDragMove() {
        if (this.dragging) {
            let newPosition = this.data.getLocalPosition(this.graphics.scrollButton.parent);
            this.index = newPosition.y - this.y;
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }
    }
}
class Select extends Asset{


    constructor(height, width, x, y) {
        super();

        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;

        this.button = new Button(x,y,'SELECT',0xFFFFFF,0x000000, false,width);
        this.container = new PIXI.Container();

        this.currentIndex = 0;
        this.elements = [];

        this.button.setOnClick(function () {
            this.container.visible = !this.container.visible;
        }.bind(this));

        this.container.visible = true;
    }

    addElement(element){
        element.setSelect(this);
        this.elements.push(element);
        this.build();
    }


    onClick(data){
        console.log(data);
    }

    build(){
        this.container.removeChildren(0);
        this.container.width = this.width;
        this.container.height = this.height;
        this.container.x = this.x-this.width/2;
        this.container.y = this.y+30;


        let currentY = 0;
        for(let element of this.elements){
            element.build(this.width/2, currentY, this.width);
            currentY+=element.getHeight();
            for(let child of element.getPixiChildren()){
                this.container.addChild(child);
            }
        }


    }


    resize(height, width){
        this.height = height;
        this.width = width;
        this.build();
    }

    setPosition(x,y){
        this.x = x;
        this.y = y;
        this.build();
    }

    getPixiChildren() {
        let elements = [];
        for (let e of this.button.getPixiChildren()){
            elements.push(e);
        }
        elements.push(this.container);
        return elements;
    }



    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        this.button.setVisible(visible);
        this.container.visible = visible;
    }
}
class SelectItem extends Asset{

    constructor(text, data=null) {
        super();
        this.selectRef = null;
        this.button = null;
        this.text = text;
        this.data = data;
        this.build(0,0,100);
    }


    build(x,y,width){
        this.button = new Button(x,y,this.text,0xFFFFFF,0x000000, false,width);
        this.button.setOnClick(function () {
            this.selectRef.onClick(this.data);
        }.bind(this));
    }


    getPixiChildren() {
        return this.button ? this.button.getPixiChildren():[];
    }

    setText(text){
        this.text = text;
    }

    setSelect(ref){
        this.selectRef = ref;
    }

    getData(){
        return data;
    }

    setData(data){
        this.data = data;
    }

    getHeight(){
        return this.button != null ? this.button.getHeight() : 0;
    }


    getY() {
        this.button.getY();
    }

    getX() {
        this.button.getX();
    }

    setVisible(visible) {
        this.button.setVisible(visible);
    }
}
class Shape extends Asset {
    constructor(x, y, width, height, position, image, name, validx, validy, idValidRes, toMove, onDBClick, onMove = null, endMove = null) {
        super();
        this.x = x;
        this.y = y;
        this.baseX = x;
        this.baseY = y;
        this.width = width;
        this.height = height;
        this.positionInTab = position;
        this.image = image;
        this.name = name;
        this.onDBClick = onDBClick;
        this.toMove = toMove;
        this.onMove = onMove;
        this.endMove = endMove;
        this.stopMove = false;
        this.alpha = 1;
        this.container = new PIXI.Container();
        this.isFirstClick = true;
        this.data = undefined;
        this.shape = undefined;
        if (image) {
            this.display();
        }
        this.validx = validx;
        this.validy = validy;
        this.idValidRes = idValidRes;
        this.onClick = function () {
            this.isClicked = true;
        };
        this.init();
    }
    init() {
        if (this.shape) {
            //this.setText(this.text);
            this.shape.interactive = true;
            this.shape.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }


    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        //Créer un "Sprite" avec une image en base64.
        //Le type d'image est "IMG" donc pour récupérer l'image, il faut faire
        //this.image.image.src
        this.shape = PIXI.Sprite.fromImage(this.image.image.src);
        this.shape.alpha = this.alpha;
        //Définir sa position et l'ancre sur l'image, ici au centre de l'image
        this.shape.anchor.x = 0.5;
        this.shape.anchor.y = 0.5;
        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.width = this.width;
        this.shape.height = this.height;

        //Ajouter les listeners
        this.shape.interactive = true;
        this.shape.on('pointerdown', this.onClick.bind(this));
        this.shape.on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));
        //Ajouter notre forme sur l'élément graphique
        this.container.addChild(this.shape);
    }

    onClick() {
        if (!this.shape.stopMove) {
            if (this.isFirstClick) {
                this.isFirstClick = false;
                //Annulation du double clique après 500ms
                setTimeout(function () {
                    this.isFirstClick = true;
                }.bind(this), 500);
            } else {
                this.isFirstClick = false;
                if (this.onDBClick)
                    this.onDBClick(this);
            }
        }
    }

    onDragStart(event) {
        this.data = event.data;
    }

    onDragEnd() {
        if (!this.shape.stopMove) {
            this.data = null;
            //console.log("x: " + this.getX() + " y: " + this.getY());
            if (this.endMove) {
                this.endMove(this);
            }
        }
    }

    onDragMove() {
        if (!this.shape.stopMove) {
            if (this.data) {
                //Regarde si la forme peut bouger
                if (this.toMove == true) {
                    if (!this.stopMove) {
                        //Position actuelle
                        let newPosition = this.data.getLocalPosition(this.shape.parent);

                        //Empêcher la forme de sortir du canvas et de devenir invisible
                        newPosition.x = newPosition.x < 0 ? 0 : newPosition.x;
                        newPosition.y = newPosition.y < 0 ? 0 : newPosition.y;
                        newPosition.x = newPosition.x > 600 ? 600 : newPosition.x;
                        newPosition.y = newPosition.y > 600 ? 600 : newPosition.y;

                        //Mise à jour la position de la forme
                        this.y = newPosition.y;
                        this.x = newPosition.x;
                        this.display();

                        //Appel au callback s'il y en a un
                        if (this.onMove) {
                            this.onMove(this);
                        }
                    }
                }
            }
        }
    }

    setName(name) {
        this.name = name;
    }
    getName() {
        return this.name;
    }
    setStopMove(stopMove){
        this.shape.stopMove = stopMove
    }
    setOnClick(onClick) {
        this.onClick = onClick;
        this.display();
    }
    getvalidx() {
        return this.validx;
    }

    setvalidx(value) {
        this.validx = value;
    }

    getvalidy() {
        return this.validy;
    }

    setvalidy(value) {
        this.validy = value;
    }

    getidValidRes() {
        return this.idValidRes;
    }
    setAplha(alpha) {
        this.alpha = alpha;
        this.display();
    }

    setidValidRes(value) {
        this.idValidRes = value;
    }

    getposition() {
        return this.positionInTab;
    }

    setposition(value) {
        this.positionInTab = value;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
        //Mise à jour de l'affichage
        this.display();
    }

    setX(x) {
        this.x = x;
        //Mise à jour de l'affichage
        this.display();
    }
    setImage(image) {
        this.image = image;
        this.display();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    setStopMove(stopMove) {
        this.stopMove = stopMove;
    }

    setShape(sprite) {
        this.shape = sprite;
    }

    getStopMove() {
        return this.stopMove;
    }

    getBaseX() {
        return this.baseX;
    }

    getBaseY() {
        return this.baseY;
    }

    setWidth(w) {
        this.width = w;
        this.display();
    }

    setHeight(h) {
        this.height = h;
        this.display();
    }

    setBaseX(baseX) {
        this.baseX = baseX;
        this.setX(baseX);
    }

    setBaseY(baseY) {
        this.baseY = baseY;
        this.setY(baseY);
    }
}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class ToggleButton extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {boolean} fitToText Taille du bouton en fonction du texte
     * @param {number} width Taille forcée du bouton
     */
    constructor(x, y, label, fitToText=false, width = 150) {
        super();

        this.fitToText = fitToText;
        this.refToggleGroup = null;
        this.width = width;
        this.height = 0;
        this._data = null;
        this.currentState = 'inactive';
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;

        this.lastOnClick = function(){
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };

        this.states = {
            active: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0xFFFFFF, align: 'center' })
            },
            inactive: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'center' })
            },
            disabled: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x666666, align: 'center' })
            }
        };

        this.onClick = this.lastOnClick;
        this.init();
        this.updateView();

    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        for (let state of Object.keys(this.states)) {
            this.states[state].graphics.interactive = true;
            this.states[state].graphics.buttonMode = true;
            this.states[state].graphics.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        for (let state of Object.keys(this.states)){
            this.states[state].lbl.text = text;
        }
        this.update();
    }


    update(){
        for (let state of Object.keys(this.states)){
            let buttonWidth = this.fitToText ? (20 + this.states[state].lbl.width): this.width;
            let buttonHeight = this.states[state].lbl.height * 1.5;
            this.height = buttonHeight;
            this.states[state].graphics.clear();
            if(state === 'active')
                this.states[state].graphics.beginFill(0x00AA00);
            else if(state === 'inactive')
                this.states[state].graphics.beginFill(0xDDDDDD);
            else
                this.states[state].graphics.beginFill(0xCCCCCC);
            this.states[state].graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
            this.states[state].graphics.endFill();
            this.states[state].lbl.anchor.set(0.5);
            this.states[state].lbl.x = this.x;
            this.states[state].lbl.y = this.y;
        }
    }

    toggle(){
        this.currentState = (this.currentState === 'active') ? 'inactive' : 'active';
        this.updateView();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = function () {
            onClick(this);
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };
        this.lastOnClick = this.onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.states.inactive.graphics,
            this.states.inactive.lbl,
            this.states.active.graphics,
            this.states.active.lbl,
            this.states.disabled.graphics,
            this.states.disabled.lbl];
    }

    updateView(){
        for (let state of Object.keys(this.states)){
            if(state === this.currentState){
                this.states[state].lbl.visible = true;
                this.states[state].graphics.visible = true;
            } else{
                this.states[state].lbl.visible = false;
                this.states[state].graphics.visible = false;
            }
        }
    }

    isActive(){
        return this.currentState === 'active';
    }

    isEnabled(){
        return this.currentState !== 'disabled';
    }

    set data(data){
        this._data = data;
    }

    get data(){
        return this._data;
    }


    show(){
        this.updateView();
    }

    hide(){
        for (let element of this.getPixiChildren()){
            element.visible = false;
        }
    }

    disable(){
        this.currentState = 'disabled';
        this.onClick = function () {};
        this.updateView();
    }

    enable(){
        this.currentState = 'inactive';
        this.onClick = this.lastOnClick;
        this.updateView();
    }

    setRefToggleGroup(ref){
        this.refToggleGroup = ref;
    }

    updateFont(font){
        for(let state of Object.keys(this.states)){
            this.states[state].lbl.style.fontFamily = font;
        }
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        visible = true;
        console.log(this.currentState);
        for (let element of this.getPixiChildren()){
            element.visible = visible;
        }
    }


    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}
class ToggleGroup extends Asset {

    /**
     *
     * @param mode
     * @param buttons
     */
    constructor(mode = 'single', buttons = []) {
        super();
        this.mode = mode;
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    addButton(button) {
        if (button != null) {
            button.setRefToggleGroup(this);
            this.buttons.push(button);
        }
    }

    setButtons(buttons = []) {
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    getButtons() {
        return this.buttons;
    }

    getActives() {
        let actives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'active')
                actives.push(btn);
        return actives;
    }

    getInactives() {
        let inactives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'inactive')
                inactives.push(btn);
        return inactives;
    }

    getPixiChildren() {
        let elements = [];
        for (let toggleButton of this.buttons)
            for (let element of toggleButton.getPixiChildren())
                elements.push(element);
        return elements;
    }

    updateList(button) {
        if (this.mode === 'single' && button.isActive()) {
            for (let btn of this.buttons) {
                if (btn !== button && btn.isActive()) {
                    btn.toggle();
                }
            }
        }
    }

    reset(){
        for (let btn of this.buttons){
            if(btn.isActive())
                btn.toggle();
        }
    }

    show() {
        for (let btn of this.buttons)
            btn.show();
    }

    hide() {
        for (let btn of this.buttons)
            btn.hide();
    }

    updateFont(font){
        for (let btn of this.buttons) {
            btn.updateFont(font);
        }
    }

    clearButtons(){
        this.buttons = [];
    }


    getY() {
        return 0;
    }

    getX() {
        return 0;
    }

    setVisible(visible) {
        for (let btn of this.buttons)
            btn.setVisible(visible);
    }
}

/**
 * @classdesc Définition d'une interface (ihm)
 * @author Vincent Audergon
 * @version 1.0
 */
const BLEU = "#3085d6";
const VERT = "0x00ff00";
const ROUGE = "0xff0000";
const DIFFICULTEHAUTE ="grid4weeks";

class Interface {

    /**
     * Constructeur d'une ihm
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} scene le nom de la scène utilisée par l'ihm
     */
    constructor(refGame, scene) {
        this.refGame = refGame;
        /** @type {PIXI.Container} la scène PIXI sur laquelle dessiner l'ihm */
        this.scene = refGame.scenes[scene];
        /** @type {string[]} les textes de l'interface dans toutes les langues */
        this.texts = [];
        /** @type {Object[]} les éléments qui composent l'ihm */
        this.elements = [];
    }

    /**
     * Défini la scène de l'ihm
     * @param {PIXI.Container} scene la scène PIXI
     */
    setScene(scene) {
        this.scene = scene;
    }

    /**
     * Définis les éléments PIXI contenus dans la scène
     * @param {Objects} elements la liste des éléments PIXI
     */
    setElements(elements) {
        this.elements = elements;
    }

    /**
     * Défini la liste des textes de l'ihm dans la langue courante
     * @param {string[]} texts la liste des textes
     */
    setTexts(texts) {
        this.texts = texts;
    }

    /**
     * Retourne un texte de l'ihm dans la langue courante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key) {
        return this.texts[key];
    }

    /**
     * Fonction de callback appelée lors d'un changement de langue
     * @param {string} lang
     */
    refreshLang(lang) { }

    /**
     * Retire tous les éléments de l'ihm de la scène PIXI
     */
    clear() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.removeChild(c);
                }
            } else {
                this.scene.removeChild(el);
            }
        }
    }

    /**
     * Ajoute tous les éléments de l'ihm dans la scène PIXI
     */
    init() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.addChild(c);
                }
            } else {
                this.scene.addChild(el);
            }
        }
        this.refGame.showScene(this.scene);
    }

    /**
     * Affiche l'ihm
     */
    show() { }

}
const WITHE_COLOR = 0xffffff;
const GREEN_COLOR = 0x67eb34;
const BLUE_COLOR = 0x007bff;
const GREY_COLOR = 0x808080;
const GRID3DAYS = "grid3days";
const GRID1WEEK = "grid1week";
const GRID2WEEKS = "grid2weeks";
const GRID4WEEKS = "grid4weeks";



// référence au json
const nameBtnThree = "btnThreeDays";
const nameBtnOneWeek = "btnOneWeek";
const nameBtnTwoWeeks = "btnTwoWeeks";
const nameBtnOneMonth = "btnOneMonth";
const nameBtnValider = "btnValider"
const btnHarmosLess = "btnHarmosLess"
const btnHarmosMore = "btnHarmosMore";
const BTNHeight = 35;
const isDev = true;

//const nameBtnOneWeek ="";
//const nameBtnOneWeek
class Create extends Interface {
    constructor(refGame) {
        super(refGame, "create");
    }

    /**
     * cette méthode initialise les variables globales
     */
    show() {
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        //========================================================================================================
        //********************************************************************************************************
        //********************************************************************************************************
        //********************************************************************************************************
        //========================================================================================================
        for (let i = 0; i < this.elements.length; i++) {
            if (typeof this.elements[i].origin !== "undefined") {
                this.elements[i].reset();
            }
        }
        /*=========================================*/
        //var Importante//
        //ces variables vont nous servir aau formattagqe JSON
        this.spriteToMoveToJson;
        this.elementsInTabToJson
        this.difficultyToJson;

        this.nbWeek;
        this.levelHarmos;
        this.imgCalendar;
        this.imggreen;
        //liste des évènements qu'on met dans le tableau (Sans justification)

        //liste des justification, des phrase d'indice ainsi que de la forme correspondante

        this.imgEventXAndY = [];
        this.listJustification = []
        this.mainSentence = []
        this.mapJustification = new Map();
        /*=========================================*/
        //liste de toutes activité du jeu
        let ybtnFirstPage = 50
        this.rendom = true;
        this.listEvent = [];
        this.img = new Shape();
        this.index = -1;
        this.indexImg = -1;
        this.indexHarmos = 0;
        this.elements = [];
        this.cells = [];
        this.imggreen = null;
        this.correctionX = 0;
        this.correctionY = 0
        this.eventNb = 0;
        this.eventNbJustification = 0;
        this.exercice = this.refGame.global.resources.getScenario();
        this.exercice = JSON.parse(this.exercice);
        this.listHarmos = [];
        this.harmosText = " Harmos";
        for (let i = 4; i <= 6; i++) {
            let txt = i + " Harmos";
            this.listHarmos.push(new String(txt));
        }
        this.cbxRendomEvent = new CheckBox(400, 530, "aléatoire", 1);
        this.cbxRendomEvent.select(false);
        let txtCreatMode = new PIXI.Text(this.refGame.global.resources.getOtherText('creatMode'), {
            // fontFamily: 'Arial',
            fontSize: 24,
            fill: 0x000000,
            align: 'left',
            wordWrap: true,
            wordWrapWidth: 580,
            fontWeight: "bolder"
        });
        txtCreatMode.x = 135;
        txtCreatMode.y = 150;
        this.elements.push(txtCreatMode)
        this.refGame.global.util.hideTutorialInputs('btnResetTr', 'btnAudioGame', 'next', 'btnReset');
        //la ligne si dessous doit être documenté pour le problème 4
        //this.cmboxHarmos =  new ComboBox(450, 450,this.listHarmos);
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial1'))
        //this.refGame.global.util.hideTutorialInputs(this.refGame.global.resources.getOtherText('trainMode'))
        this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('btncreat'), BLUE_COLOR, WITHE_COLOR, true);
        this.startButton.setOnClick(this.chooseHarmosAndCalendar.bind(this));
        this.elements.push(this.startButton);
        //this.nextHarmos = new Button(400, 50, this.refGame.global.resources.getOtherText(btnHarmosMore), BLUE_COLOR, WITHE_COLOR, true, 35);
        //this.nextHarmos.setOnClick(this.nextHarmosFun.bind(this));
        //this.nextHarmos.setOnClick(this.chooseHarmosAndCalendar.bind());
        //this.previousHarmos = new Button(200, 50, this.refGame.global.resources.getOtherText(btnHarmosLess), BLUE_COLOR, WITHE_COLOR, true, 35);
        //this.previousHarmos.setOnClick(this.previousHarmosFun.bind(this));

        //btn pour le calendrier
        this.treeDays = new Button(150, ybtnFirstPage, this.refGame.global.resources.getOtherText(nameBtnThree), BLUE_COLOR, WITHE_COLOR, true, 50);
        this.treeDays.setOnClick(this.changeImgCalendar.bind(this, 0));

        this.oneWeek = new Button(250, ybtnFirstPage, this.refGame.global.resources.getOtherText(nameBtnOneWeek), BLUE_COLOR, WITHE_COLOR, true, 50);
        this.oneWeek.setOnClick(this.changeImgCalendar.bind(this, 1));

        this.towWeeks = new Button(400, ybtnFirstPage, this.refGame.global.resources.getOtherText(nameBtnTwoWeeks), BLUE_COLOR, WITHE_COLOR, true, 50);
        this.towWeeks.setOnClick(this.changeImgCalendar.bind(this, 2));

        this.oneMonth = new Button(500, ybtnFirstPage, this.refGame.global.resources.getOtherText(nameBtnOneMonth), BLUE_COLOR, WITHE_COLOR, true, 50);
        this.oneMonth.setOnClick(this.changeImgCalendar.bind(this, 3));

        this.btnValidation = new Button(300, 550, this.refGame.global.resources.getOtherText(nameBtnValider), BLUE_COLOR, WITHE_COLOR, true);
        this.btnValidation.setHeight(35);
        this.btnRetour = new Button(250, 550, this.refGame.global.resources.getOtherText(nameBtnValider), BLUE_COLOR, WITHE_COLOR, true);
        this.btnRetour.setHeight(35);
        //this.btnValidation.setWidth(150)
        this.btnValidation.setOnClick(this.validateHarmosAndCalendar.bind(this));
        //btn start
        this.refGame.global.util.showTutorialInputs("startBtn");
        this.texteHarmos = new PIXI.Text(this.listHarmos[0], {
            // fontFamily: 'Arial',
            fontSize: 16,
            fill: 0x000000,
            align: 'left',
            wordWrap: true,
            wordWrapWidth: 580,
            fontWeight: "bolder"
        });
        this.texteHarmos.x = 268;
        this.texteHarmos.y = 45;
        //========================================================================================================
        //********************************************************************************************************
        //********************************************************************************************************
        //========================================================================================================
        //récupération de toutes les images
        this.getAllEventFromDB();
        this.init();

        //this.clear();
    }

    /**
     * cette méthode va afficher les boutons
     */
    chooseHarmosAndCalendar() {
        this.clear();
        this.elements = []
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial1'));

        //this.elements.push(this.formatteTxt(this.listHarmos[0]))
        this.elements.push(this.treeDays);
        this.elements.push(this.oneWeek);
        this.elements.push(this.towWeeks);
        this.elements.push(this.oneMonth);
        //this.elements.push(this.texteHarmos);
        this.elements.push(this.btnValidation);
        this.init();
        this.changeImgCalendar(0);

    }

    nextHarmosFun(e) {
        this.indexHarmos++;
        this.changeTxtHarmos();
    }

    previousHarmosFun(e) {
        this.indexHarmos--;
        this.changeTxtHarmos();
    }

    /**
     * Cette méthode va changer le texte du label pour la séléction du niveau Harmos
     */
    changeTxtHarmos() {
        if (this.indexHarmos < 0) {
            //on recommence à la dernière case
            this.indexHarmos = 0
        } else if (this.indexHarmos == this.listHarmos.length) {
            //on recommence à 0
            this.indexHarmos = this.listHarmos.length - 1;
        }

        this.levelHarmos = this.listHarmos[this.indexHarmos];
        this.texteHarmos.text = this.listHarmos[this.indexHarmos];
        this.elements.push(this.texteHarmos);
        this.init();
    }


    /**
     * Cette méthode va changer l'image du calendrier sélectionné
     * @param {int} imgCalendar est un numéro indiquant le calendrier choisi
     */
    changeImgCalendar(imgCalendar) {
        /*
        calendarImg nous permttra de choisir l'image.
        0 = 3 jours
        1 = 1 semaine
        2 = 2 semaines
        3 = 1 mois (4 semaines)
        */
        this.treeDays.setbgcolor(BLUE_COLOR);
        this.oneWeek.setbgcolor(BLUE_COLOR);
        this.towWeeks.setbgcolor(BLUE_COLOR);
        this.oneMonth.setbgcolor(BLUE_COLOR);
        let imgToChoose = "";
        switch (imgCalendar) {
            case 0:
                imgToChoose = GRID3DAYS;
                this.treeDays.setbgcolor(GREEN_COLOR);
                break;
            case 1:
                imgToChoose = GRID1WEEK;
                this.oneWeek.setbgcolor(GREEN_COLOR);
                this.correctionY = -6;
                break;
            case 2:
                imgToChoose = GRID2WEEKS;
                this.towWeeks.setbgcolor(GREEN_COLOR);
                this.correctionY = -6;
                break;
            case 3:
                imgToChoose = GRID4WEEKS;
                this.oneMonth.setbgcolor(GREEN_COLOR);
                this.correctionY = -2;
                break;
            default:
        }
        let x = 0;
        let y = 0;
        let i = 1;
        this.exercice = this.refGame.global.resources.getScenario();
        this.exercice = JSON.parse(this.exercice);
        for (let key in this.exercice.creat.grid) {
            if (this.exercice.creat.grid[i]) {
                if (this.exercice.creat.grid[i].data.name == imgToChoose) {
                    x = this.exercice.creat.grid[i].data.x;
                    y = this.exercice.creat.grid[i].data.y;
                    break;
                }
            }
            i++;
        }
        //réduire la taille de l'image 

        this.nbWeek = imgToChoose;
        if (imgToChoose === GRID4WEEKS) {
            x = x / 1.5;
            y = y / 1.5;

        } else {
            x = x * 1.188;
            y = y * 1.188;
        }
        let placeXY = 300
        this.img.setImage(this.refGame.global.resources.getImage(imgToChoose));
        this.img.setWidth(x);
        this.img.setHeight(y);
        this.img.setX(placeXY);
        this.img.setY(placeXY);
        this.elements.push(this.img);
        this.init();
    }

    /**
     * cette méthode va remplacer les élements du canvas par l'image du calendrier.
     */
    validateHarmosAndCalendar() {
        if (this.nbWeek) {
            this.clear();
            this.elements = [];
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial2'));
            /* if(isDev){
                console.log(this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial2')));
            } */
            this.btnValidation.setOnClick(this.validateCurrentTime.bind(this));
            this.elements.push(this.btnValidation);

            this.exercice = this.refGame.global.resources.getScenario();
            this.exercice = JSON.parse(this.exercice);
            /* if(isDev){
                console.log(JSON.parse(this.exercice));
            } */

            let count = 1;
            for (let key in this.exercice.creat.grid) {
                if (this.exercice.creat.grid[count].data) {
                    if (this.exercice.creat.grid[count].data.name == this.nbWeek) {
                        this.currentGrid = this.exercice.creat.grid[count];
                        console.log(this.currentGrid);
                        break;
                    }
                }
                count++
            }

            if (this.nbWeek == GRID4WEEKS) {
                this.btnValidationX = 500;
                this.btnValidationY = 300;


                this.btnValidation.setX(this.btnValidationX);
                this.btnValidation.setY(this.btnValidationY);
            } else {
                this.btnValidationX = 300;
                this.btnValidationY = 550;
                this.btnValidation.setX(this.btnValidationX);
                this.btnValidation.setY(this.btnValidationY);
            }

            this.backImg = this.refGame.global.resources.getImage(this.currentGrid.data.name).image.src;
            this.insererBack = new ImgBack(this.currentGrid.data.locationx, this.currentGrid.data.locationy, this.currentGrid.data.x, this.currentGrid.data.y, this.backImg, 'background');
            this.elements.push(this.insererBack);
            this.putButton();
            if (isDev) {
                console.log(this.backImg);
            }
        }
    }

    /**
     * cette méthode va initaliser les boutons sur le quadrillage.
     */
    putButton() {
        let i = 1;
        let size = 24;
        let sizeMonth = 18;
        let autofit = false;
        let label = null;
        let magnets = this.currentGrid.magnets;

        for (let key in magnets) {
            //on retire 10 au x et y pour mieux centrer le bouton
            let caseToPress = new Button(magnets[i].x - this.correctionX, magnets[i].y, label, WITHE_COLOR, WITHE_COLOR, autofit);
            //let caseToPress = new Button(magnets[i].x , magnets[i].y  , label, WITHE_COLOR, WITHE_COLOR, autofit);

            //Si l'image vert a déja été posé on place les btns pour la 2eme étape 
            if (this.imggreen) {
                //si les x et y sont les mêmes 
                //if (magnets[i].x == this.imggreen.getX() && magnets[i].y == this.imggreen.getY() + this.correctionY) {
                if (magnets[i].x - this.correctionX == this.imggreen.getX() && magnets[i].y - this.correctionY == this.imggreen.getY()) {
                    caseToPress = null;
                } else {
                    caseToPress.setOnClick(this.popupEvent.bind(this, caseToPress));
                }
                //Si l'image vert n'a pas été posé on place les btn pour la 1er étape
            } else {
                caseToPress.setOnClick(this.putCurrentTime.bind(this, caseToPress));
            }

            //on set des données générale du bouton (taille)
            if (caseToPress) {
                caseToPress.setAlpha(0);
                caseToPress.setWidth(size + 6);
                caseToPress.setHeight(size);
                this.cells.push(caseToPress);
                this.elements.push(caseToPress);
            }

            i++;
        }
        //on charge les modifications
        this.init();
    }

    /**
     * cette méthode va placer l'image du temps présent (verte) dans la case que le joueur a sélectionnée.
     * @param {shape} e
     */
    putCurrentTime(e) {
        let x = e.x;
        let y = e.y;
        let size = 32;
        if (this.currentGrid.data.name == GRID4WEEKS) {
            size = 32;
        } else if (this.currentGrid.data.name == GRID2WEEKS) {
            size = 37;
        } else if (this.currentGrid.data.name == GRID1WEEK) {
            size = 47;
        } else if (this.currentGrid.data.name == GRID3DAYS) {
            size = 45;
        }
        if (!this.imggreen) {
            //création de l'image
            //this.imggreen = new Shape(x + this.correctionX, y, size, size, 1, this.refGame.global.resources.getImage(this.exercice.creat.currentTime), "greenFace", 1, true);

            this.imggreen = new Shape(x, y, size, size, 1, this.refGame.global.resources.getImage(this.exercice.creat.currentTime), "greenFace", 1, true);
        } else {
            //modification de x et y
            this.imggreen.setX(x);
            this.imggreen.setY(y)
        }
        this.elements.push(this.imggreen);
        this.init();
    }

    /**
     * Cette méthode va nous faire passer à la page suivante. En retirant les élements du tableau.
     */
    validateCurrentTime() {
        if (this.imggreen) {

            this.refGame.global.util.hideTutorialInputs('btnValider');

            if (this.nbWeek == GRID4WEEKS) {
                this.btnValidationX = 500;
                this.btnValidationY = 300;


                this.btnValidation.setX(this.btnValidationX);
                this.btnValidation.setY(this.btnValidationY);
            } else {
                this.btnValidationX = 300;
                this.btnValidationY = 550;
                this.btnValidation.setX(this.btnValidationX);
                this.btnValidation.setY(this.btnValidationY);
            }


            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial3'));
            this.clear();
            this.elements = [];

            this.elements.push(this.insererBack);
            this.elements.push(this.imggreen)
            this.btnValidation.setX(this.btnValidationX)
            this.btnValidation.setY(this.btnValidationY)
            this.btnValidation.setVisible(true);

            this.btnValidation.setOnClick(this.validateEventPlace.bind(this));

            this.elements.push(this.btnValidation);
            this.elements.push(this.cbxRendomEvent);

            let i = 0;
            //on parcourt la liste des activités
            for (let key in this.imgEventXAndY) {
                let tempShape = this.imgEventXAndY[i];

                //on leur ajoute un event


                this.elements.push(tempShape)
                i++;
            }


            this.init();
            this.putButton();
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'Attention',
                text: 'tu dois placer le moment',
                confirmButtonText: "Ok"
                //footer: '<a href="">Why do I have this issue?</a>'
            })
        }

    }


    /**
     * Cette méthode va placer les "formes" dans le tableau et y assigner un event.
     */
    setShape() {
        let i = 1;
        let magnets = this.currentGrid.magnets;
        let tailleSprite;
         if (this.currentGrid.data.name == GRID4WEEKS) {
            tailleSprite = 32;
        } else if (this.currentGrid.data.name == GRID2WEEKS) {
            tailleSprite = 37;
        } else if (this.currentGrid.data.name == GRID1WEEK) {
            tailleSprite = 47;
        } else if (this.currentGrid.data.name == GRID3DAYS) {
            tailleSprite = 45;
        }
        for (let key in magnets) {
            let shapeToPress
            if (magnets[i].x != this.imggreen.getX() && magnets[i].y != this.imggreen.getY) {
                console.log(magnets[i].x);
                console.log(magnets[i].y);
                shapeToPress = new Shape(magnets[i].x, magnets[i].y, tailleSprite, tailleSprite, 1, null, null, 1, true);
                shapeToPress.setOnClick(this.popupEvent.bind(this, shapeToPress));
                shapeToPress.setImage(this.refGame.global.resources.getImage(this.exercice.creat.currentTime))
                this.cells.push(shapeToPress);
                this.elements.push(shapeToPress);
            }
            i++;
        }
        this.init();
    }

    /**
     * cette méthode va récupérer toutes les images des activités dans la db.
     */
    getAllEventFromDB() {
        //récupération des images 
        let count = 0;
        this.exercice = this.refGame.global.resources.getScenario();
        this.exercice = JSON.parse(this.exercice);
        //est-ce vraiment util ?? je ne le sais pas !
        for (let key in this.exercice.creat.events) {
            this.listEvent.push(this.exercice.creat.events[count]);
            count++;
        }
    }


    /**
     * cette méthode va générer la popup avec laquelle l'utilisateur va choisir le type d'activité.
     * Si il y a déja une forme dans la case elle va suprimer l'activitée.
     * Et si la case aléatoire est checkée alors on prend une activité aléatoire.
     * @param {shape} e
     */
    async popupEvent(e) { 

        let sameId = 0;




        let sameEvent = false;
        let i = 0;
        let eX = 0;
        let eY = 0;


        for (let key in this.imgEventXAndY) {
            console.log(key);

            let tempShape = this.imgEventXAndY[i];


            if ((e.getX() == tempShape.x) && (e.getY() == (tempShape.y))) {
                sameEvent = true;
                sameId = i;
                eX = e.getX();
                eY = e.getY();
                break;
            }
            i++;

        }


        if (!sameEvent) {
            let i = 0;


            if (!this.cbxRendomEvent.isChecked()) {
                const { value: logoselected } = await Swal.fire({
                    title: "Sélectionnez un évenement.",
                    html: this.addLogo(),
                    width: 500,
                    focusConfirm: false,
                    preConfirm: () => {
                        if (document.querySelector("input[name='logo']:checked") !== null) {
                            return [
                                document.querySelector("input[name='logo']:checked").id
                            ]
                        } else {
                            return [null]
                        }
                    }
                });



                if (logoselected != "") {
                    let size;
                     if (this.currentGrid.data.name == GRID4WEEKS) {
                        size = 32;
                    } else if (this.currentGrid.data.name == GRID2WEEKS) {
                        size = 37;
                    } else if (this.currentGrid.data.name == GRID1WEEK) {
                        size = 47;
                    } else if (this.currentGrid.data.name == GRID3DAYS) {
                        size = 45;
                    }
                    let count = 0;
                    let htmlToInsertInPopup;
                    let btnx = e.getX();
                    let btny = e.getY();
                    console.log(btnx);
                    console.log(btny);

                    let shape = new Shape();
                    let that = this;
                    let select;
                    let icons = [];
                    let name = logoselected;

                    shape.setImage(that.refGame.global.resources.getImage(name));
                    shape.setWidth(size);
                    shape.setHeight(size);
                    //shape.setX(btnx);
                    //shape.setY(btny);
                    shape.setX(btnx);
                    shape.setY(btny);
                    //shape.setX(btnx);
                    //shape.setY(btny);
                    shape.setName(name);
                    //on ajoute la forme dans le tableau
                    this.imgEventXAndY.push(shape);
                    that.eventNb++;
                    that.elements.push(shape);
                    that.init();
                }


            } else {
                console.log("bonjour monsieur");
                let sizeSiAlea;
                if (this.currentGrid.data.name == GRID4WEEKS) {
                    sizeSiAlea = 32;
                } else if (this.currentGrid.data.name == GRID2WEEKS) {
                    sizeSiAlea = 37;
                } else if (this.currentGrid.data.name == GRID1WEEK) {
                    sizeSiAlea = 47;
                } else if (this.currentGrid.data.name == GRID3DAYS) {
                    sizeSiAlea = 45;
                }

                let count = 0;
                let htmlToInsertInPopup;
                let btnx = e.getX();
                let btny = e.getY();
                console.log(btnx);
                console.log(btny);

                let shape = new Shape();
                let that = this;
                let select;
                let icons = [];
                let name = this.rendomEvent();
                console.log(name);

                shape.setImage(that.refGame.global.resources.getImage(name));
                shape.setWidth(sizeSiAlea);
                shape.setHeight(sizeSiAlea);
                //shape.setX(btnx);
                //shape.setY(btny);
                shape.setX(btnx);
                shape.setY(btny);
                //shape.setX(btnx);
                //shape.setY(btny);
                shape.setName(name);
                //on ajoute la forme dans le tableau
                this.imgEventXAndY.push(shape);
                that.eventNb++;
                that.elements.push(shape);
                that.init();
            }

            this.putButton();
        } else {
            this.removeEvent(sameId, eX, eY, 0);
        }
    }


    addLogo() {
        var nbreImageLigne = 3;
        var div = '<div style="overflow:scroll; overflow-x: hidden;  display: block; width: 100%; height:500px; border:#000000 1px solid;">';
        let nbreImage = 0;

        const organisedItems = []
        for (let i = 0; i < this.listEvent.length; i++) {
            organisedItems.push(
                {
                    'name': this.refGame.global.resources.getOtherText(this.listEvent[i]),
                    'key': this.listEvent[i],
                    'value': this.refGame.global.resources.getImage(this.listEvent[i])
                })
        }
        organisedItems.sort((a, b) => a.name.localeCompare(b.name))

        for (let i = 0; i < organisedItems.length; i++) {
            if (nbreImage % nbreImageLigne === 0) {
                div += '<div style="display: flex;"><div style="display: inline;width:30%;height:80px"><img src="' + organisedItems[i].value.image.currentSrc + ' " /></br><span>' + organisedItems[i].name + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div>';
            } else if (nbreImage % nbreImageLigne === nbreImageLigne - 1) {
                div += '<div style="display: inline;width:30%;height:80px"><img src="' + organisedItems[i].value.image.currentSrc + '"/></br><span>' + organisedItems[i].name + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div></div></br>';
            } else {
                div += '<div style="display: inline;width:30%;height:80px"><img src="' + organisedItems[i].value.image.currentSrc + '"/></br><span>' + organisedItems[i].name + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div>';
            }
            nbreImage++;
        }
        div += '</div></div>';

        return div;
    }

    /**
     * Permet de supprimer une activité
     * @param idEvent id de l'activité dans le tableau des activité
     * @param shapeX position en x de la case cliquée
     * @param shapeY position en Y de la case cliquée
     * @param tableDiff différence en Y entre l'activité et la case du tableau
     */
    removeEvent(idEvent, shapeX, shapeY, tableDiff) {
        this.imgEventXAndY.splice(idEvent, 1);
        this.elements.splice(idEvent, 1);
        let a = 0;
        this.clear();
        this.elements = [];
        this.elements.push(this.insererBack);
        this.elements.push(this.imggreen);
        this.btnValidation.setX(this.validerX);
        this.btnValidation.setY(this.validerY);
        this.btnRetour = new Button(this.btnretourX, this.btnretourY, "retour", GREY_COLOR, WITHE_COLOR, true);
        this.btnRetour.setHeight(BTNHeight);
        for (let key in this.imgEventXAndY) {
            let tempShape = this.imgEventXAndY[a];
            this.elements.push(tempShape);
            a++;
            if (!this.listJustification.length == 0) {
                let i = 0;
                for (let key in this.listJustification) {
                    let justX = this.listJustification[i].shape.getX();
                    let justY = this.listJustification[i].shape.getY();
                    if ((justX == shapeX) && (justY + tableDiff == shapeY)) {
                        this.listJustification.splice(i, 1);
                        this.eventNbJustification--;
                    }
                    i++;
                }
            }
        }
        this.btnValidation.setX(this.btnValidationX)
        this.btnValidation.setY(this.btnValidationY)
        this.btnValidation.setVisible(true);
        this.btnValidation.setOnClick(this.validateEventPlace.bind(this));
        this.elements.push(this.btnValidation);
        this.elements.push(this.cbxRendomEvent);
        this.init();
        this.putButton();
    }

    /**
     *  Cette méthode va  nous faire passer à la page suivante en retirant les élements du tableau.
     */
    validateEventPlace() {
        if (this.eventNb != 0) {
            this.clear();
            this.elements = [];

            if (this.nbWeek == GRID4WEEKS) {
                this.validerX = 500;
                this.validerY = 200;

                this.btnretourX = 500;
                this.btnretourY = 250;

            } else {
                this.validerX = 400;
                this.validerY = 550;

                this.btnretourX = 150;
                this.btnretourY = 550;

            }


            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('creatTutorial4'));
            this.btnValidation.setText("valider");
            if (isDev){
                console.log(this.insererBack);
                console.log(this.imggreen);
            }
            this.elements.push(this.insererBack);
            this.elements.push(this.imggreen);
            this.btnValidation.setX(this.validerX);
            this.btnValidation.setY(this.validerY);


            this.btnRetour = new Button(this.btnretourX, this.btnretourY, "retour", GREY_COLOR, WITHE_COLOR, true);
            this.btnRetour.setHeight(35);


            this.btnRetour.setOnClick(this.validateCurrentTime.bind(this));
            this.btnRetour.setVisible(true);

            this.elements.push(this.btnRetour);


            this.btnValidation.setVisible(true);
            this.btnValidation.setOnClick(this.formatToJson.bind(this));
            let i = 0;
            //on parcours la liste des activités
            for (let key in this.imgEventXAndY) {
                let tempShape = this.imgEventXAndY[i];
                //on leur ajoute un event
                tempShape.setOnClick(this.addJustification.bind(this, tempShape));
                this.elements.push(tempShape)
                i++;
            }

            this.elements.push(this.btnValidation);

            this.init();
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'Attention',
                text: 'tu dois placer au moins une activit\u00e9',
                confirmButtonText: "Ok"
                //footer: '<a href="">Why do I have this issue?</a>'
            })
        }
    }

    /**
     * Cette méthode crée une popup dans laquelle l'utilisateur entrera les justifications.
     * @param {shape} e
     */
    addJustification(e) {
        //if (this.eventNbJustification < 5) {

        let that = this;
        let indexShapeGlobal = 0;

        let i = 0;
        let ok = false;
        let isAlreadyJust = false;
        let tempShape;
        let tempList = []
        let ms;
        //on regarde si la forme a déja été justifier
        for (let key in this.listJustification) {
            //on récupère les coordonnées
            let x = this.listJustification[i].shape.getX();
            let y = this.listJustification[i].shape.getY();
            if (y == e.getY() && x == e.getX()) {
                if (this.listJustification[i].data) {
                    isAlreadyJust = true;
                    indexShapeGlobal = i;
                    break;
                }
            }
            i++;
        }
        let htmlToInsertInPopup = `<div> <h3>` + this.refGame.global.resources.getOtherText("popupJustification1") + `</h3><br>  `;
        if (!isAlreadyJust) {
            htmlToInsertInPopup +=
                `<div id="all">
                <div id = "top-pop-pu">
                <label for="fname">` + this.refGame.global.resources.getOtherText("popupJustification2") + `</label><br>
                <input type="text" id="mainSentence"  style="width: 450px"  name="fname"><br>`;

            htmlToInsertInPopup += `<div style="text-align: left; margin-top: 5px" class="position-relative"><br>` + this.refGame.global.resources.getOtherText("popupJustification3") + `<br>`

            for (let i = 0; i < 4; i++) {
                htmlToInsertInPopup += `<input type="radio" class="radiobtn"id="input_btn_rd` + i + `" name="validate_" value="op1"> <input type="text" id="input_txt_` + i + `" name="fname" value=""><br><br>`
            }

            htmlToInsertInPopup += `</div>`
        } else {
            htmlToInsertInPopup +=
                `<div id="all">
                    <div id = "top-pop-pu">
                        <label for="fname">` + this.refGame.global.resources.getOtherText("popupJustification2") + `</label><br>
                        <input type="text" id="mainSentence" style="width: 450px" name="fname" value="` + this.listJustification[indexShapeGlobal].mainSentence + `"><br>
                `;
            htmlToInsertInPopup += `<div style="text-align: left; margin-top: 5px" class="position-relative"><br>` + this.refGame.global.resources.getOtherText("popupJustification3") + `<br>`


            for (let i = 0; i < 4; i++) {
                if (this.listJustification[indexShapeGlobal].data[i].isTrue) {
                    htmlToInsertInPopup += `<input type="radio" class="radiobtn"id="input_btn_rd` + i + `" name="validate_" value="op1" checked>`
                } else {
                    htmlToInsertInPopup += `<input type="radio" class="radiobtn"id="input_btn_rd` + i + `" name="validate_" value="op1">`
                }
                htmlToInsertInPopup += `
                        <input type="text" id="input_txt_` + i + `" name="fname" value="` + this.listJustification[indexShapeGlobal].data[i].sentence + `"> <br><br>`
            }
            htmlToInsertInPopup += `</div>`
        }
        Swal.fire({
            confirmButtonText: 'Validation',
            showCancelButton: true,
            showConfirmButton: true,
            html:
                htmlToInsertInPopup + `
                    </div>
                </div>`,
            preConfirm: (login) => {
                //on parcours pour savoir si un des 4 btn à été coché
                for (let i = 0; i < 4; i++) {
                    if ($('#input_btn_rd' + i).is(':checked')) {
                        ok = true;

                    }
                }
                if (!ok) {
                    Swal.showValidationMessage(`Attention tu n'as rien s\u00e9lectionn\u00e9 de r\u00e9ponse correcte`);
                }
            }
        }).then((result) => {
            if (result.value) {
                let setence = [];
                let okChamp = true;
                for (let i = 0; i < 4; i++) {
                    let s = {
                        sentence: $('#input_txt_' + i).val(),
                        isTrue: $('#input_btn_rd' + i).is(':checked')
                    }
                    if (!$('#input_txt_' + i).val()) {
                        okChamp = false;
                        console.log("attentionn")
                        break;
                    }


                    setence.push(s);
                }
                let data = {
                    shape: e,
                    mainSentence: $("#mainSentence").val(),
                    data: setence
                }

                //vérification des champs

                //récupération des information
                if (isAlreadyJust) {
                    let j = 0;
                    for (let key in this.listJustification) {
                        if (this.listJustification[j]) {
                            let x = this.listJustification[j].shape.getX();
                            let y = this.listJustification[j].shape.getY();
                            if (y == e.getY() && x == e.getX()) {
                                if (ok) {
                                    this.eventNbJustification++;
                                    this.listJustification.splice(j, 1, data);
                                    break;
                                }
                            }
                            j++;
                        }
                    }
                } else {
                    if (okChamp) {
                        e.setAplha(0.5)
                        this.elements.push(e)
                        this.eventNbJustification++;
                        //console.log("x "+x+" \n y "+ y)
                        this.listJustification.push(data)
                    }
                }
            }
        });

        /*} else {
            Swal.fire({
                icon: 'warning',
                title: 'Attention',
                text: 'tu ne peux pas justifier plus de 5 \u00e9v\u00e9ment',
                confirmButtonText: "Ok"
                //footer: '<a href="">Why do I have this issue?</a>'
            })
        }*/
        this.init();
    }

    /**
     * cette méthode va formater l'exercice et l'envoyer au serveur.
     */
    saveExercice() {
        console.log(this.exercice);
        this.refGame.global.resources.saveExercice(JSON.stringify(this.exercice), function (result) {
            Swal.fire(
                result.message,
                undefined,
                result.type
            );

            this.show();
        }.bind(this));
    }

    /**
     *
     */
    formatToJson() {

        if (this.eventNbJustification == 0) {
            Swal.fire({
                icon: 'warning',
                title: 'Attention',
                text: 'Ton dois justifier au moins une activit\u00e9',
                confirmButtonText: "Ok"
                //footer: '<a href="">Why do I have this issue?</a>'
            })
        } else {
            let eits = this.formatEITSoJson();
            let stm = this.formatSTMToJson();
            let exe = {
                difficulty: {
                    //on récupère tous les infos suivante de la grille courante.
                    locationx: this.currentGrid.data.locationx,
                    locationy: this.currentGrid.data.locationy,
                    name: this.currentGrid.data.name,
                    x: this.currentGrid.data.x,
                    y: this.currentGrid.data.y
                },
                elementsInTab: {
                    // eits = les image par défaut dans le tableau
                    sprites: Object.fromEntries(eits),
                    ///on récupère tous les magnets de la grille courante.
                    magnets: this.currentGrid.magnets
                },
                //stm = les images déplaçables 
                spriteToMove: Object.fromEntries(stm)
            }
            //On converti notre exercice en String
            exe = JSON.stringify(exe);
            this.exercice = exe;
            this.newGame(exe);


        }


    }


    /**
     * cette méthode va formater les élements dans le tableau. elle va se charger des "sprites".
     * @returns un string des élements du tableau.
     */
    formatEITSoJson() {
        //--------------------------------------------
        //                  elementsInTab
        //--------------------------------------------
        let eventNotJust = [];
        //ajout de l'image verte
        eventNotJust.push(this.imggreen)
        //compteur
        let i = 0
        let size = 32;
        //forme temporaire
        let tempShape;
        //Map de images par défaut et des informations
        let sprit = new Map();
        //on récupère toutes les event sans justification
        for (let key in this.imgEventXAndY) {
            let j = 0;
            tempShape = this.imgEventXAndY[i];
            let ok = true;
            for (let key in this.listJustification) {
                //Si les x y sont identique
                if (tempShape.getX() == this.listJustification[j].shape.getX() && tempShape.getY() == this.listJustification[j].shape.getY()) {
                    ok = false;
                    break;
                }
                j++;
            }
            if (ok) {
                eventNotJust.push(tempShape)
            }

            i++;
        }
        //on formatte
        i = 0;

        for (let key in eventNotJust) {

            let data;
            if (eventNotJust[i]) {

                data = {
                    nomFichier: eventNotJust[i].getName().toString(),
                    x: eventNotJust[i].getX().toString(),
                    y: eventNotJust[i].getY().toString(),
                    width: size.toString(),
                    height: size.toString()
                }
                let i2 = i + 1
                //formatter en str
                sprit.set(i2, data);
            }

            i++;
        }
        //console.log(JSON.stringify(Object.fromEntries(sprit)))
        return sprit;
    }

    /**
     * Cette méthode va se charger de formater les "spritetomove"
     * @returns un string de tous les sprites to move
     */
    formatSTMToJson() {

        //--------------------------------------------
        //                  spriteToMove
        //--------------------------------------------
        //index
        let i = 0;
        //forme temporaire
        let tempShape;
        //liste a retourné
        let spriteToMove = new Map();
        //this.listJustification est la liste des formes placées et justifiées
        for (let key in this.listJustification) {
            //phrase d'indice
            let txt = this.listJustification[i].mainSentence;
            //la forme
            tempShape = this.listJustification[i].shape;
            //le nom de la forme
            let sprite2 = tempShape.getName();
            //nouvel index
            let j = 0;
            //liste des réponses 
            let answersData = new Map();
            //data = liste des réponses
            for (let key in this.listJustification[i].data) {
                //on controle bien si les X & Y sont semblables
                if (tempShape.getX() == this.listJustification[i].shape.getX() && tempShape.getY() == this.listJustification[i].shape.getY()) {
                    //on ajout un js object
                    // index commence à 1
                    answersData.set(j + 1, {
                        text: this.listJustification[i].data[j].sentence,
                        ok: this.listJustification[i].data[j].isTrue
                    });
                }
                j++;
            }
            //création de l'objet sprite to move
            let data = {
                sprite: sprite2,
                text: txt,
                popup: {
                    answers: Object.fromEntries(answersData)
                },
                verif: {
                    x: this.listJustification[i].shape.x.toString(),
                    y: this.listJustification[i].shape.y.toString()
                }
            };
            i++;
            spriteToMove.set(i, data);
        }
        return spriteToMove;
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic) {

    }


    rendomEvent() {
        let max = this.listEvent.length;

        let rendom = Math.floor(Math.random() * max);
        return this.listEvent[rendom];


    }


    newGame(exe) {
        this.btnValidation.setVisible(false);
        this.exercice = exe
        this.clear();
        this.imgPositioning = [];
        this.validation = [];

        //MODE TRAIN

        this.refGame.global.util.setTutorialText("Test maintenant ton niveau");

        this.elements.unshift(this.startButton);
        this.startButton.setVisible(false);


        this.refGame.global.util.hideTutorialInputs('btnResetTr');
        //json Sélectionner un exercice sauf celui de base

        //console.log(this.exercice)
        this.exercice = JSON.parse(this.exercice);


        //console.log("__________________________________________________________")

        //insertion du background
        this.back = this.exercice.difficulty;
        this.backImg = this.refGame.global.resources.getImage(this.back.name).image.src;


        let insererBack = new ImgBack(this.back.locationx, this.back.locationy, this.back.x, this.back.y, this.backImg, 'background');
        this.elements.push(insererBack);

        //insertion des images dans le tableau
        var count = 1;
        for (let key in this.exercice.elementsInTab.sprites) {
            var img = this.exercice.elementsInTab.sprites[count];
            if (typeof this.refGame.global.resources.getImage(img.nomFichier).image.src !== "undefined") {
                let imageTab = new ImgBack(img.x, img.y, 37, 37, this.refGame.global.resources.getImage(img.nomFichier).image.src, img.nomFichier);
                //let imageTab = new ImgBack(0, 0, img.width, 32, this.refGame.global.resources.getImage(img.nomFichier).image.src, img.nomFichier);
                this.elements.push(imageTab);
            }
            count++;
        }
        count = 1;
        //this.nbFrome = this.exercice.spriteToMove;
        this.nbFrome = 0;
        for (let key in this.exercice.spriteToMove) {
            this.nbFrome++;
        }
        this.cells = [];
        this.listError = [];
        this.nbBouger = 0;
        //insertion des aimants
        for (let key in this.exercice.elementsInTab.magnets) {
            var magnetInTab = this.exercice.elementsInTab.magnets[count];
            let x = magnetInTab.x;
            let y = magnetInTab.y;
            let isPlaced = false;
            let magnet = new Guide(parseInt(x), parseInt(y), 20, magnetInTab.text,);
            this.cells.push(magnet);
            this.elements.push(magnet);
            count++;
        }

        //Insertion des images que l'utilisateur pourra bouger
        let startXgreen;
        let startYgreen;

        if (this.exercice.difficulty.name == DIFFICULTEHAUTE) {
            var startX = 400;
            var startY = 30;
            startXgreen = 400;
            startYgreen = 40;
            this.correctionY = -2;
        } else if (this.exercice.difficulty.name == "grid2weeks") {
            var startX = 50;
            var startY = 430;
            startXgreen = 50;
            startYgreen = 370;
        } else if (this.exercice.difficulty.name == "grid3days") {
            var startX = 50;
            var startY = 350;
            startXgreen = 50;
            startYgreen = 290;
        } else if (this.exercice.difficulty.name == "grid1week") {
            var startX = 50;
            var startY = 350;
            startXgreen = 50;
            startYgreen = 285;
        }
        count = 1
        //affichage du txt explicatif du
        this.textVerte = new PIXI.Text(txtGreen, {
            // fontFamily: 'Arial',
            fontSize: 18,
            fontWeight: "bolder",
            fill: 0x000000,
            align: 'right',
            //wordWrap: true,
            wordWrapWidth: 380
        });

        this.textVerte.x = startXgreen + 50;
        this.textVerte.y = startYgreen - 60;


        let imggreen = new Shape(startXgreen, startYgreen - 50, 37, 37, count, this.refGame.global.resources.getImage("greenFace"), "greenFace", idOK, true);
        if (this.exercice.difficulty.name == DIFFICULTEHAUTE) {
            //var info = new InfoImg(550, startY + 8, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            var info = new InfoImg(550, 10, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClickGreen.bind());//,count,this.onShapeClick.bind(this)
            this.elements.push(info);
        } else {
            this.elements.push(this.textVerte);
            var info = new InfoImg(550, startYgreen - 50, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClickGreen.bind(this));//,count,this.onShapeClick.bind(this)
            this.elements.push(info);
        }


        let nb = 1
        let startX2 = startX
        let startY2 = startY
        this.elements.push(imggreen);

        for (let key in this.exercice.spriteToMove) {
            var txt = this.exercice.spriteToMove[count].texte;
            var img = this.exercice.spriteToMove[count].sprite;
            var i = 1;
            var idOK = 0;
            //Ici on récupère quelle est la bonne réponse, on va utiliser la variable idOk plus tard, l'hors de la validation de l'exercice

            for (let key in this.exercice.spriteToMove[count].popup.answers) {
                if (this.exercice.spriteToMove[count].popup.answers) {
                    if (this.exercice.spriteToMove[count].popup.answers[i].ok) {
                        idOK = i;
                    }
                    i++;
                }
            }
            //insertion des images qui pourront être bougées
            let imageTab;
            let imageTab2;
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                startX2 = startX2 + 60
                imageTab = new Shape(startX2, startY, 37, 37, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2 = new Shape(startX2, startY, 37, 37, count, this.refGame.global.resources.getImage(img));
                imageTab2.setStopMove(true);

            } else {
                this,
                    imageTab = new Shape(startX2, startY2 + 80, 37, 37, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2 = new Shape(startX2, startY2 + 80, 37, 37, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2.setStopMove(true);
            }
            imageTab2.setAplha(0.3)
            this.elements.push(imageTab2);
            this.elements.push(imageTab);
            //insertion du texte
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                this.texte = new PIXI.Text(txt, {
                    // fontFamily: 'Arial',
                    fontSize: 12,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.texte.x = 100;
                this.texte.y = startY;
                //this.elements.push(this.texte);
            }

            //insertion des "i" a gauche du text / des images
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                var info = new InfoImg(startX2, startY + 30, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            } else {
                var info = new InfoImg(startX2 + 50, startY2 + 80, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            }
            this.elements.push(info);
            //startY = startY + 40;
            startY2 = startY2 + 35
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                if (nb % 8 == 0) {
                    startX2 = startX
                    startY = startY + 70
                }
            } else {
                if (nb % 14 == 0) {
                    startX2 = startX2 + 100
                    startY2 = startY
                }

                //startY = startY + 30
            }
            count++;
            nb++;
        }

        //this.elements.push(texte1);
        this.refGame.global.util.showTutorialInputs('btnValider');
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.validateWholeExercice.bind(this));
        //écrire


        this.init();


    }

    onInfoClickGreen() {
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: justify"> ' + txtGreen + ' </div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    /**
     * Cette méthode sera déclenchée quand on clique sur l'un des boutons Info, elle va générer une popup avec le texte et
     * l'icône d'un hautparleur qui va déclencher le tts
     * @param info objet de l'image qui contient un id permettant d'identifier le texte qu'il faut afficher
     */
    onInfoClick(info) {
        let txt;
        if (!this.exercice.spriteToMove[info.getTextId()].text) {
            txt = this.exercice.spriteToMove[info.getTextId()].texte
        } else {
            txt = this.exercice.spriteToMove[info.getTextId()].text
        }
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td style="padding-right: 30px"><div><img src=' + this.refGame.global.resources.getImage(this.exercice.spriteToMove[info.getTextId()].sprite).image.src + ' alt="' + this.exercice.spriteToMove[info.getTextId()].sprite + '"></div></td>' +
                '<td><div id="activites" style="text-align: justify">' + txt + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    onShapeMove(shape) {
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.cells[i].setShape(null);
            }
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.cells[i].getShape() === shape || !this.cells[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.cells[i].setShape(shape);
            }
        }
    }

    /**
     * Methode appellée quand l'utilisateur arrête de bouger une image
     * @param shape
     */
    onShapeEndMove(shape) {
        let ok = false;
        if (!shape.getStopMove()) {
            for (let i = 0; i < this.cells.length; i++) {
                let cell = this.cells[i];
                let minX = cell.x - cell.width / 1.7;
                let maxX = cell.x + cell.width / 1.7;
                let minY = cell.y - cell.width / 1.7;
                let maxY = cell.y + cell.width / 1.7;
                if (cell.x == shape.getX() && cell.y == shape.getY()) {
                    ok = true
                }
                if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                    shape.setX(cell.x);
                    shape.setY(cell.y);
                    if (this.cells[i].getShape() != null) {
                        this.generateJustificationPopup(this.cells[i], shape.getposition());
                        if (this.cells[i].getShape() !== shape) {
                            this.cells[i].getShape().setX(this.cells[i].getShape().getBaseX());
                            this.cells[i].getShape().setY(this.cells[i].getShape().getBaseY());
                        }
                    }
                    this.cells[i].setShape(shape);
                }

            }
            if (!ok) {
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
                this.elements.push(shape)
                this.init()
            }
        }
    }


    /**
     * Cette methode génère la popup de justification quand elle est appellée (quand une image a été déposée dans un aimant)
     * @param cell cellule ou l'image a été déposée
     * @param i
     */
    generateJustificationPopup(cell, i) {
        //creation de l'html qui sera mis à l'interieur de la popup
        var shape = cell.getShape();
        let count = 1;
        let htmlToInsertInPopup = "";
        let k = 1
        let sprit = this.exercice.spriteToMove[i];
        for (let key in this.exercice.spriteToMove[i].popup.answers) {
            htmlToInsertInPopup += '<input type="radio" class="radiobtn"id="input_' + count + '" name="validate_' + i +
                '" value="op' + count + '" data-correct="' + this.exercice.spriteToMove[i].popup.answers[k].ok + '">' +
                ' <label htmlFor="op' + count + '" style="padding-left: 3px">';
            let txt = "";
            txt = this.exercice.spriteToMove[i].popup.answers[k].text;
            htmlToInsertInPopup += txt + '</label>';
            htmlToInsertInPopup += '<br>';
            count++;
            k++
        }
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            html:
                '<h4>' + 'Tu places ' + this.refGame.global.resources.getOtherText(shape.name) + ' le ' + cell.getDate() + '. Pourquoi ?' + '</h4>' +
                '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                '</div>',
            preConfirm: (login) => {
                //check si les btns radio sont sélectionnés
                let ischeck;
                for (let j = 1; j <= 4; j++) {
                    ischeck = document.getElementById("input_" + j).checked;
                    if (ischeck) {
                        break;
                    }
                }
                if (!ischeck) {
                    Swal.showValidationMessage(`Attention tu n'as rien s\u00e9lectionn\u00e9`);
                    //si aucun btn n'est sélectionné -> popup de Warning
                }
            },
            showConfirmButton: true,
        }).then((result) => {
            if (result.dismiss == 'cancel') {
                //Si l'utilisateur clique sur le bouton cancel on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else if (result.dismiss == 'backdrop') {
                //Si l'utilisateur clique dehors la popup on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else {
                //Si l'utilisateur clique sur le bouton valider on test si c'est ok ou pas, et ensuite on bloque l'image
                this.validateImgPositioning(shape.getX(), shape.getY(), shape.getvalidx(), shape.getvalidy(), shape.name);
                //Si l'emplacement est faux ont cree une varible d'erreur qu'on place dans une liste
                this.validateJustification(shape, i, sprit);
                shape.setStopMove(true);
                //ischeck = true;
                //input++;
            }
        });
    }

    onShapeClick(shape) {
    }

    validateWholeExercice() {
        if (this.onEndGame()) {
            var that = this;
            var htmlToPutInTheErrorPopup = [];
            //si tous les éléments ont été placés on teste si sont tous ok ou pas, si ils sont ok on les compte
            var count = 0;
            var countValidation = 0;
            var countImgPositioning = 0;
            var reponseEleve = "";
            for (var key in this.validation) {
                //ici on récupère les résultats de l'élève et on génère le texte qui sera mis dans la BD au mode évaluer
                var ligneCourrante = "";
                if (this.validation[count].substr(0, 2) == "ok") {
                    countValidation++;
                    var elmsOK = this.validation[count].split(" ");
                    ligneCourrante += this.exercice.spriteToMove[elmsOK[1]].sprite + ":" + "Val=OK//";
                } else {
                    var elmsKO = this.validation[count].split("/");
                    let i = 1;
                    let juste = "";
                    //récuperer la bonne réponse
                    while (this.exercice.spriteToMove[elmsKO[0]].popup.answers[i].ok != true) {
                        i++
                    }
                    juste = this.exercice.spriteToMove[elmsKO[0]].popup.answers[i].text;

                    ligneCourrante += this.exercice.spriteToMove[elmsKO[0]].sprite + ":";
                    if (elmsKO[1] == 0) {
                        ligneCourrante += "Val=KO-//";
                        htmlToPutInTheErrorPopup.push("<p>" + this.refGame.global.resources.getOtherText('selected') + ": <span style='color: red'>" + this.refGame.global.resources.getOtherText('noResponse') + "</span></p>");
                    } else {
                        ligneCourrante += "Val=KO-" + elmsKO[1] + "//";
                        htmlToPutInTheErrorPopup.push("<p>" + this.refGame.global.resources.getOtherText('selected') + ": <span style='color: red'>" + this.exercice.spriteToMove[elmsKO[0]].popup.answers[elmsKO[1]].text + "</span></p>");
                    }
                    htmlToPutInTheErrorPopup.push("<p>Juste: <span style='color: green'>" + juste + "</span></p>");
                }
                if (this.imgPositioning[count] == "ok") {
                    countImgPositioning++;
                    ligneCourrante += "Pos=OK;";
                } else {
                    ligneCourrante += "Pos=KO;";
                }
                reponseEleve += ligneCourrante;
                count++;
            }
            count = 0;
            // Si c'est le mode eévaluer on teste les erreurs, on donne une note et on ajoute le resumé de l'exercice
            // On donne un point à l'élève s'il a fait moins de la moitié des erreurs faisables
            // 0 s'il a moins de la moitié et 2 s'il a parfaitement juste
            if (this.evaluate) {
                let points = -1;
                if (countValidation == 0){
                    points = 0;
                }
                if (countValidation == countImgPositioning) {
                    points = 2;
                } else if (countValidation >= (countImgPositioning / 2)) {
                    points = 1;
                } else {
                    points = 0;
                }
                this.refGame.global.statistics.addStats(points);
                let resultSend = points + "," + reponseEleve + "," + this.id;
                this.refGame.global.statistics.updateStats(points);
                // this.refGame.global.statistics.updateStats(resultSend);
                if (points === 2) {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('allGood'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'success',
                        showConfirmButton: true
                    });
                } else {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('errorsMade'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'error',
                        showConfirmButton: true
                    });
                }
                this.clear();
                this.elements = [];
                this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('startBtn'), 0x007bff, 0xFFFFFF, true);
                this.startButton.setOnClick(this.newGame.bind(this));
                this.elements.push(this.startButton);
                this.show(this.evaluate);
            } else {

                // si on a 5 pour la validation et pour le positionning on affiche une popup qui indique que tout est juste
                // sinon on affiche les erreurs
                //if (countValidation == 5 && countImgPositioning == 5) {

                if (this.listError.length == 0) {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('allGood'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'success',
                        showConfirmButton: true
                    }).then(() => {
                        that.reStart();
                        // this.show(false, null);
                    });
                } else {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('errorsMade'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'error',
                        showConfirmButton: true,
                    }).then((result) => {
                        that.generatePopupErrorPlacement();
                        //  this.refGame.global.util.showTutorialInputs('btnResetTr');
                        // this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.resetExercice.bind(this));

                    });
                }
                /*
*/
            }
        } else {
            swal.fire({
                title: "Tu n'as pas fini !",
                text: "Tu pourras valider ton exercice lorsque tu auras plac\u00e9 toutes les images ;)",
                icon: "warning",
            });
        }

    }


    validateImgPositioning(imgX, imgY, correctX, corrextY, name) {
        let c
        if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
            c = 0
        } else {
            c = 80
        }

        let solution;
        let image;
        if (imgX == correctX && imgY == corrextY) {
            this.imgPositioning.push("ok");
            this.checkError = true;
        } else {
            this.imgPositioning.push(imgX + "/" + imgY);
            this.checkError = false;
        }
        this.nbBouger++;
        if (!this.checkError) {
            let i = 1;
            let xJson;
            let yJson;
            for (let key in this.exercice.elementsInTab.magnets) {
                xJson = this.exercice.elementsInTab.magnets[i].x;
                yJson = this.exercice.elementsInTab.magnets[i].y;
                if (xJson != correctX && yJson + c != corrextY) {
                    solution = this.exercice.elementsInTab.magnets[i].text;
                    image = this.getImageForPopUpWithName(name);

                }
                i++;
            }
            this.addErrorToList(false, image, solution, null);
        }
        //this.onEndGame();
    }

    getImageForPopUpWithName(name) {
        return this.refGame.global.resources.getImage(name);
    }


    validateJustification(shape, imgPos, sprit) {
        let solution = "";
        //obtention de la solution
        let k = 1;
        for (var key in sprit.popup.answers) {
            if (sprit.popup.answers[k].ok) {
                solution = sprit.popup.answers[k].text;
            }
            k++;
        }
        let img = this.getImageForPopUpWithCount(imgPos);
        var ok = document.getElementById("input_" + shape.getidValidRes()).checked;
        if (ok) {
            this.validation.push("ok" + " " + imgPos);
        } else {
            let count = 1;
            let selectedInput = 0;
            for (var key in this.exercice.spriteToMove[imgPos].popup.answers) {
                if (document.getElementById("input_" + count).checked) {
                    selectedInput = count;
                }
                count++;
            }
            //ImgPos est la position de l'image dans la liste selected input est la justification sélectionnée
            this.validation.push(imgPos + "/" + selectedInput);
            //ajout de la mauvaise réponse dans la liste d'erreur
            this.addErrorToList(true, img, solution, null);
        }

    }


    getImageForPopUpWithCount(count) {
        let img = this.exercice.spriteToMove[count].sprite;
        return this.getImageForPopUpWithName(img);
    }

    onEndGame() {
        let res = false;
        //si le nombre de sprite to move est égale au nombre d'image bougé
        if (this.nbBouger == this.nbFrome) {
            res = true;
        }
        return res;
    }


    generatePopupErrorPlacement() {
        if (this.listError.length > 0) {
            var that = this;
            let htmlToInsertInPopup = '';
            // i = le nombre d'erreur total
            let i = 0;
            //nbWrong = le nombre d'erreur de placement
            let nbWrong = 0;
            for (let key in this.listError) {
                if (!this.listError[i].isJustification) {
                    htmlToInsertInPopup += "<p size='3'><img src=" + this.listError[i].img.image.src + " alt='oups....'>" + this.listError[i].rightAnswser + "<br>";
                    //+ une erreur de placement
                    nbWrong++;
                }
                //+ une erreur
                i++;
            }
            //check si il y a des erreur de placement
            if (nbWrong > 0) {
                let txtPopUp = "";
                //pour le "s" de erreur
                if (nbWrong == 1) {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreur de placement. Tu aurais d\u00fb le mettre :";
                } else {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreurs de placement. Tu aurais d\u00fb le mettre : ";
                }
                Swal.fire({
                    width: 500,
                    confirmButtonColor: BLEU,
                    showCancelButton: false,
                    confirmButtonText: this.txtBtnOK,
                    showConfirmButton: true,
                    html: '<h4>' + txtPopUp + '</h4>' +
                        '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                        '</div>'
                }).then((result) => {
                    if (that.validatorPopup) {
                        that.generatePopupErrorJustification();
                    } else {
                        that.reStart()
                    }

                })
            }
        }
    }

    reStart() {

        if (this.nbWeek == GRID4WEEKS) {
            this.validerX = 500;
            this.validerY = 200;

            this.btnretourX = 500;
            this.btnretourY = 250;

        } else {
            this.validerX = 400;
            this.validerY = 550;

            this.btnretourX = 150;
            this.btnretourY = 550;

        }

        this.btnVali = new Button(this.validerX, this.validerY, "valider l'exercice", BLUE_COLOR, WITHE_COLOR, true);
        this.btnVali.setHeight(35);
        this.btnRetour = new Button(this.btnretourX, this.btnretourY, "retour", GREY_COLOR, WITHE_COLOR, true);
        this.btnRetour.setHeight(35);


        this.btnRetour.setOnClick(this.validateEventPlace.bind(this));

        this.btnVali.setOnClick(this.saveExercice().bind(this));

        this.elements.push(this.btnVali);

        this.elements.push(this.btnRetour);


        this.refGame.global.util.hideTutorialInputs('btnValider');
        this.refGame.global.util.setTutorialText("tu peux maintenant valider ton exercice, ou revenir en arrière pour faire des modification");

        this.init();

    }


    addErrorToList(justification, img, solution, rep) {
        if (justification) {
            this.validatorPopup = justification
        }
        var error = {
            isJustification: justification,
            falseAnswser: rep,
            img: img,
            rightAnswser: solution
        };
        //ajout à la liste 
        this.listError.push(error);

    }

    generatePopupErrorJustification() {
        if (this.listError.length > 0) {
            let htmlToInsertInPopup = '';
            let i = 0;
            let nbWrong = 0;
            let that = this;
            for (let key in this.listError) {
                if (this.listError[i].isJustification) {
                    htmlToInsertInPopup += "<p size='3'><img src=" + this.listError[i].img.image.src + " alt='oups....'>" + this.listError[i].rightAnswser + "<br>";
                    nbWrong++;
                }
                i++;
            }
            if (nbWrong > 0) {
                // pour le "s" de erreur
                let txtPopUp = "";
                if (this.listError.length == 1) {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreur de justification. Tu aurais d\u00fb justifier comme ceci:";
                } else {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreurs de justification. Tu aurais d\u00fb justifier comme ceci:";
                }
                Swal.fire({
                    width: 500,
                    confirmButtonColor: BLEU,
                    confirmButtonText: this.txtBtnOK,
                    showConfirmButton: true,
                    html: '<h4>' + txtPopUp + '</h4>' +
                        '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                        '</div>'
                }).then((result) => {
                    this.reStart();
                });
            }

        }

    }


}
//const txtGreen = "la case en vert repr\u00e9sente le moment pr\u00e9sent";
class Explore extends Interface {


    constructor(refGame) {

        super(refGame, "explore");
        this.tutotxtNo = 1;
        this.cells = [];
    }



    /**
     * Cette méthode nous permet créer et afficher tout ce qu'on voit sur le canevas
     */
    show() {

        /*
        let dateDebut = new Date("2023-01-30");
        let calendrier = new Calendar(7, 1, dateDebut);
        this.elements.push(calendrier.getCalendarElements()[0]);
        for (let i = 0; i < calendrier.getCalendarElements()[1].length; i++) {
            this.elements.push(calendrier.getCalendarElements()[1][i]);
        }
        */

        this.countbtn = 0;
        this.cells = [];
        this.imgPositioning = [];
        this.elements = [];
        this.clear();
        this.txtBtnOK = "Ok";
        this.tutotxtNo = 1;
        this.validation = [];
        this.listError = [];
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.refGame.global.util.setTutorialText("<p>" + this.refGame.global.resources.getTutorialText('tutoWelcome') + "</p> <p style='font-size:20px;'>" + this.refGame.global.resources.getTutorialText('tuto4') + "</p>");
        this.refGame.global.util.showTutorialInputs('btnReset', 'next');
        this.refGame.global.util.hideTutorialInputs('btnResetTr', 'btnValider', 'btnAudioGame');
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('next'), ['click'], this.handleNext.bind(this));

        // Rapport de taille pour augmenter la taille globale des éléments
        const ratio = 1.1862;

        this.exercice = this.refGame.global.resources.getScenario();
        this.exercice = JSON.parse(this.exercice);
        (this.exercice.explorer)
        //insertion des aimants
        var count = 1
        for (let key in this.exercice.explorer.elementsInTab.magnets) {
            var magnetInTab = this.exercice.explorer.elementsInTab.magnets[count];

        //    let tempXMagnet = (parseInt(magnetInTab.x) * 1.1862) - 54;
        //    let tempYMagnet = (parseInt(magnetInTab.y) * 1.1862) - 3;

            let magnet = new Guide(magnetInTab.x - 54, magnetInTab.y - 3, 20, magnetInTab.text);
            this.cells.push(magnet);
            this.elements.push(magnet);
            count++;
            //console.log(cell);
            console.log(count);
            if (this.exercice.explorer.elementsInTab.magnets.length == count) {
                break;
            }
        }
        // insertion du background
        let back = this.exercice.explorer.difficulty;
        let backImag = this.refGame.global.resources.getImage(back.name).image.src;
        let insererBack = new ImgBack(300, 175, 415, 350, backImag, 'background');
        this.elements.push(insererBack);
        //insertion des images dans le tableau
        var count = 1;
        for (let key in this.exercice.explorer.elementsInTab.sprites) {
            var img = this.exercice.explorer.elementsInTab.sprites[count];
            console.log(this.exercice.explorer.elementsInTab.sprites[count]);
        //    let tempX = (parseInt(img.x) * 1.1862) - 54;
        //    let tempY = (parseInt(img.y) * 1.1862) - 3;

            if (typeof this.refGame.global.resources.getImage(img.nomFichier).image.src !== "undefined") {
                let imageTab = new ImgBack(img.x - 54, img.y - 3, 40, 40, this.refGame.global.resources.getImage(img.nomFichier).image.src, img.nomFichier);
                this.elements.push(imageTab);
            }
            count++;
        }
        count = 1;


        //insertion des aimants
        for (let key in this.exercice.explorer.elementsInTab.magnets) {
            var magnetInTab = this.exercice.explorer.elementsInTab.magnets[count];

            let tempXMagnet = (parseInt(magnetInTab.x) * 1.1862) - 54;
            let tempYMagnet = (parseInt(magnetInTab.y) * 1.1862) - 3;
            
            let magnet = new Guide(parseInt(tempXMagnet), parseInt(tempYMagnet), 20);
            this.cells.push(magnet);
            this.elements.push(magnet);
            count++;
        }
        count = 1

        this.graphics2 = new PIXI.Graphics();
        this.graphics2.beginFill(VERT);

        this.validate1 = this.graphics2.drawRect(100, 348 + 70, 210, 20);
        this.validate2 = this.graphics2.drawRect(100, 388 + 70, 50, 20);
        this.validate3 = this.graphics2.drawRect(100, 428 + 70, 33, 20);
        this.validate4 = this.graphics2.drawRect(240, 428 + 70, 60, 20);
        this.validate5 = this.graphics2.drawRect(465, 428 + 70, 60, 20);
        this.validate6 = this.graphics2.drawRect(315, 468 + 70, 90, 20);
        this.validate7 = this.graphics2.drawRect(100, 468 + 70, 45, 20);

        this.elements.push(this.validate1, this.validate2, this.validate3, this.validate4, this.validate5, this.validate6, this.validate7);
        this.validate1.visible = false;
        this.validate2.visible = false;
        this.validate3.visible = false;
        this.validate4.visible = false;
        this.validate5.visible = false;
        this.validate6.visible = false;
        this.validate7.visible = false;

        let startX = 100;
        let startY = 420;
        let imggreen = new Shape(50, startY - 40, 32, 32, count, this.refGame.global.resources.getImage("greenFace"), "greenFace", 1, true);
        this.elements.push(imggreen);
        let texteGreenTab = new PIXI.Text(txtGreen, {
            // fontFamily: 'Arial',
            fontSize: 18,
            fontWeight: "bolder",
            fill: 0x000000,
            align: 'right',
            //wordWrap: true,
            wordWrapWidth: 380
        });
        var infoGreen = new InfoImg(550, startY - 40, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClickGreen.bind());
        texteGreenTab.x = startX;
        texteGreenTab.y = startY - 50;
        this.elements.push(texteGreenTab);
        this.elements.push(infoGreen);





        for (let key in this.exercice.explorer.spriteToMove) {
            var txt = this.exercice.explorer.spriteToMove[count].texte;
            var img = this.exercice.explorer.spriteToMove[count].sprite;
            this.texte = new PIXI.Text(txt, {
                // fontFamily: 'Arial',
                fontSize: 12,
                fill: 0x000000,
                align: 'left',
                wordWrap: true,
                wordWrapWidth: 580
            });
            var info = new InfoImg(550, startY + 8, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));
            this.elements.push(info);
            this.texte.x = startX;
            this.texte.y = startY;
            startY = startY + 40;
            this.elements.push(this.texte);
            count++;
        }



        //création des images "déplaçables"
        this.bicycle = new ImgBack(50, Math.round(350 * ratio), 38, 38, this.refGame.global.resources.getImage("bicycle").image.src, "bicycle");
        this.restaurant = new ImgBack(50, Math.round(390 * ratio), 38, 38, this.refGame.global.resources.getImage("restaurant").image.src, "restaurant");
        this.birthday = new ImgBack(50, Math.round(430 * ratio), 38, 38, this.refGame.global.resources.getImage("birthday").image.src, "birthday");
        this.movie = new ImgBack(50, Math.round(470 * ratio), 38, 38, this.refGame.global.resources.getImage("movie").image.src, "movie");
        this.elements.push(this.bicycle, this.restaurant, this.birthday, this.movie);

        //images "déplaçables" qui ont été placées automatiquement
        this.bicycle2 = new ImgBack(Math.round(321 * ratio), Math.round(90 * ratio), 38, 38, this.refGame.global.resources.getImage("bicycle").image.src, "bicycle");
        this.restaurant2 = new ImgBack(Math.round(364 * ratio), Math.round(245 * ratio), 38, 38, this.refGame.global.resources.getImage("restaurant").image.src, "restaurant");
        this.birthday2 = new ImgBack(Math.round(407 * ratio), Math.round(245 * ratio), 38, 38, this.refGame.global.resources.getImage("birthday").image.src, "birthday");
        this.movie2 = new ImgBack(Math.round(321 * ratio), Math.round(124 * ratio), 38, 38, this.refGame.global.resources.getImage("movie").image.src, "movie");
        this.elements.push(this.bicycle2, this.restaurant2, this.birthday2, this.movie2);

        //cacher les images plaçes au bon endroit car elles seront affichés dans la deuxième étape
        this.bicycle2.setVisible(false);
        this.restaurant2.setVisible(false);
        this.birthday2.setVisible(false);
        this.movie2.setVisible(false);

        //images grisées
        this.bicycle2Gray = new ImgBack(50, Math.round(350 * ratio), 38, 38, this.refGame.global.resources.getImage("bicycleGray").image.src, "bicycle");
        this.restaurant2Gray = new ImgBack(50, Math.round(390 * ratio), 38, 38, this.refGame.global.resources.getImage("restaurantGray").image.src, "restaurant");
        this.birthday2Gray = new ImgBack(50, Math.round(430 * ratio), 38, 38, this.refGame.global.resources.getImage("birthdayGrey").image.src, "birthday");
        this.movie2Gray = new ImgBack(50, Math.round(470 * ratio), 38, 38, this.refGame.global.resources.getImage("movieGray").image.src, "movie");
        this.elements.push(this.bicycle2Gray, this.restaurant2Gray, this.birthday2Gray, this.movie2Gray);

        //cacher les images grisées car elles seront affichés dans la deuxième étape
        this.bicycle2Gray.setVisible(false);
        this.restaurant2Gray.setVisible(false);
        this.birthday2Gray.setVisible(false);
        this.movie2Gray.setVisible(false);

        //Déclaration de pixi graphics
        this.graphics = new PIXI.Graphics();
        this.graphics.lineStyle(3, ROUGE);

        //Création d'un carré rouge
        this.rect = this.graphics.drawRect(30, 560, 40, 40);
        this.elements.push(this.rect);
        this.rect.visible = false;

        //image que l'utilisateur pourra bouger
        this.lapPool = new Shape(50, 590, 32, 32, 1, this.refGame.global.resources.getImage("lapPool"), 'lapPool', 0, 0, 0, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
        this.lapPool.setStopMove(true);
        this.elements.push(this.lapPool);

        this.init();
        //fin de show
    }





    onInfoClickGreen() {
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: justify"> ' + txtGreen + ' </div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic) {

    }

    /**
     * Cette méthode sera déclenchée quand on clique sur un des boutons Info, elle va générer une popup avec le texte et
     * une icône d'un hautparleur qui va déclencher le tts
     * @param info objet de l'image qui contient un id permettant d'identifier le texte qu'il faut afficher
     */
    onInfoClick(info) {
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td style="padding-right: 30px"><div><img src=' + this.refGame.global.resources.getImage(this.exercice.explorer.spriteToMove[info.getTextId()].sprite).image.src + ' alt="' + this.exercice.explorer.spriteToMove[info.getTextId()].sprite + '"></div></td>' +
                '<td><div id="activites" style="text-align: justify">' + this.exercice.explorer.spriteToMove[info.getTextId()].texte + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });

        console.log(this.exercice.explorer.spriteToMove[info.getTextId()]);

    }

    /**
     * Méthode qui est appelée quand une image est déplacée
     * @param shape
     */
    onShapeMove(shape) {
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.cells[i].setShape(null);
            }
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.cells[i].getShape() === shape || !this.cells[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.cells[i].setShape(shape);
            }
        }
    }


    /**
     * Méthode appelée quand on arrête de bouger une image
     * @param shape
     */
    onShapeEndMove(shape) {
        let ok = false;
        console.log(shape)
        if (!shape.getStopMove()) {
            for (let i = 0; i < this.cells.length; i++) {
                let cell = this.cells[i];
                let minX = cell.x - cell.width / 1.7;
                let maxX = cell.x + cell.width / 1.7;
                let minY = cell.y - cell.width / 1.7;
                let maxY = cell.y + cell.width / 1.7;
                if (cell.x == shape.getX() && cell.y == shape.getY()) {
                    ok = true
                }
                if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                    shape.setX(cell.x);
                    shape.setY(cell.y);
                    if (this.cells[i].getShape() != null) {
                        //Lancement de la méthode
                        if (this.cells[i].date !== undefined) {
                            let position = this.getpositionByName(shape);
                            this.datecell = this.cells[i].date;
                            this.position = position
                            this.cellInTheTab = this.cells[i]
                            this.refGame.global.util.showTutorialInputs('next')
                            this.handleNext()
                            this.generateJustificationPopup(this.cellInTheTab, this.position);
                        }
                        if (this.cells[i].getShape() !== shape) {
                            this.cells[i].getShape().setX(this.cells[i].getShape().getBaseX());
                            this.cells[i].getShape().setY(this.cells[i].getShape().getBaseY());
                        }
                    }
                    this.cells[i].setShape(shape);
                }
            }
            if (!ok) {
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
                this.elements.push(shape)
                this.init()
            }
        }
    }
    /**
     * Cette fonction retourne la position de la forme bougée.
     * @param shape la forme dont on cherche le nom
     * @returns  la position du nom de la forme dans le json. -1 si rien ne correspond 
     */
    getpositionByName(shape) {
        let position = -1;
        let count = 1;
        console.log(shape.name);
        for (let key in this.exercice.explorer.spriteToMove) {
            let name = this.exercice.explorer.spriteToMove[count].sprite;
            let areEqual = shape.name.toUpperCase() === name.toUpperCase();
            if (areEqual) {
                position = count;
                break;
            }
            count++;
        }
        return position;
    }

    /**
     * Cette méthode est en charge de générer la popup quand c'est nécessaire. La popup contiendra 4
     */
    popUp() {
        let count = 1;
        let htmlToInsertInPopup = "";
        for (let key in this.exercice.explorer.spriteToMove[5].popup.answers) {
            htmlToInsertInPopup += '<input type="radio" id="op' + count + '" name="op' + count + '" value="op' + count + '"'
            if (this.exercice.explorer.spriteToMove[5].popup.answers[count].ok == true) {
                htmlToInsertInPopup += 'checked="checked">'
            } else {
                htmlToInsertInPopup += 'disabled>'
            }
            '<label htmlFor="op' + count + '" style="padding-left: 3px">'//checked="checked"
            let txt = "";
            txt = this.exercice.explorer.spriteToMove[5].popup.answers[count].texte;
            htmlToInsertInPopup += txt + '</label>';
            htmlToInsertInPopup += '<br>';
            count++;
        }
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<h4>' + this.refGame.global.resources.getTutorialText('tutoPopupTitle') + '</h4>' +
                '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                '</div>'
        });

    }

    /**
     * Cette methode génére la popup de justification quand elle est appéllée (quand une image a été dépose dans un aimant)
     * @param cell cellule ou l'image a été déposée
     * @param i
     */
    generateJustificationPopup(cell, i) {
        //let i = 5
        //creation de l'html qui sera mis à l'interieur de la popup
        var shape = cell.getShape();
        let count = 1;
        let htmlToInsertInPopup = "";
        console.log(i);
        console.log(this.exercice.explorer.spriteToMove)
        for (let key in this.exercice.explorer.spriteToMove[i].popup.answers) {
            htmlToInsertInPopup += '<input type="radio" class="radiobtn"id="input_' + count + '" name="validate_' + i +
                '" value="op' + count + '" data-correct="' + this.exercice.explorer.spriteToMove[i].popup.answers[count].ok + '">' + ' <label htmlFor="op' + count + '" style="padding-left: 3px">';
            let txt = "";//AU SECOURS
            txt = this.exercice.explorer.spriteToMove[i].popup.answers[count].texte;
            htmlToInsertInPopup += txt + '</label>';
            htmlToInsertInPopup += '<br>';
            count++;
            //nécessaire pour que la popUp ne fasse pas n'importe quoi           
            console.log("taille : " + this.exercice.explorer.spriteToMove[i].popup.answers.length)
            if (count == this.exercice.explorer.spriteToMove[i].popup.answers.length) {
                this.countbtn = count - 1;
                break;
            }
        }
        this.countbtn - 1;
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            showConfirmButton: true,
            html:
                '<h4>' + 'Tu places la piscine le ' + this.datecell + '. Pourquoi ?' + '</h4>' +
                '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                '</div>',
            preConfirm: (login) => {
                let ischeck;
                for (let j = 1; j <= 4; j++) {
                    ischeck = document.getElementById("input_" + j).checked;
                    if (ischeck) {
                        break;
                    }
                }
                if (!ischeck) {
                    Swal.showValidationMessage(`Attention tu n'as rien s\u00e9lectionn\u00e9`);
                }
            }
        }).then((result) => {
            if (result.dismiss == 'cancel') {
                //Si l'utilisateur clique sur le bouton cancel on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else if (result.dismiss == 'backdrop') {
                //Si l'utilisateur clique dehors la popup on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else {
                //Si l'utilisateur clique sur le bouton valider on test si c'est ok ou pas, et ensuite on bloque l'image
                this.validateImgPositioning(shape.getX(), shape.getY(), shape.getvalidx(), shape.getvalidy());
                this.validateJustification(shape.getidValidRes(), i);
                this.generatePopupErrorJustification();
                //reesaayser
                this.refGame.global.util.hideTutorialInputs('btnReset', 'next');
                this.refGame.global.util.showTutorialInputs('btnValider');
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.EndGame.bind(this));
                shape.setStopMove(true);
            }
        });
    }

    /**
    * Cette méthode nous permet de tester que le positionement de l'image sur le calendrier est correct puis le stocker
    * @param imgX coordonées en x de l'image
    * @param imgY coordonées en y de l'image
    * @param correcontext coordonées correctes en x
    * @param corrextY coordonées correctes en y
    */
    validateImgPositioning(imgX, imgY, correcontext, corrextY) {
        if (imgX == correcontext && imgY == corrextY) {
            this.imgPositioning.push("ok");
        } else {

            this.imgPositioning.push(imgX + "/" + imgY);
        }
    }


    /**
 * Cette méthode nous permet de tester que la justification soie correcte
 * @param correctInput id de l'input qui faut vérifier si à été coche ou pas
 */
    validateJustification(correctInput, imgPos) {
        let ok = false;
        let count = 1;
        let selectedInput = 0;
        let solution;
        for (var key in this.exercice.explorer.spriteToMove[imgPos].popup.answers) {
            if (this.exercice.explorer.spriteToMove[imgPos].popup.answers) {
                if (this.exercice.explorer.spriteToMove[imgPos].popup.answers[count].ok) {
                    solution = this.exercice.explorer.spriteToMove[imgPos].popup.answers[count].texte;
                    if (document.getElementById("input_" + count).checked) {
                        ok = true;
                        break;
                    } else {
                        ok = false;
                    }
                }
                if (document.getElementById("input_" + count).checked) {
                    selectedInput = count;
                }
                count++;
            }
        }


        //ImgPos est la position de l'image dans la liste selected input est la justification sélectionnée
        if (!ok) {
            this.addErrorToList(false, this.refGame.global.resources.getImage("lapPool"), solution);
        }
        this.validation.push(imgPos + "/" + selectedInput);


    }

    /**
 * cette méthode va ajouter une variable d'erreur a la liste.
 * @param   justification Boolean qui nous indique si l'erreur est une justification ou un placement
 * @param   img l'image de l'activité
 * @param   solution String qui nous donne la solution
 */
    addErrorToList(justification, img, solution) {
        var error = {
            isJustification: justification,
            img: img,
            rightAnswser: solution
        };
        //ajout à la liste 
        this.listError.push(error);
    }


    generatePopupErrorJustification() {
        if (this.listError.length > 0) {
            let htmlToInsertInPopup = '';
            let i = 0;
            let that = this;
            htmlToInsertInPopup += "<p size='3'><img src=" + this.listError[i].img.image.src + " alt='oups....'>" + this.listError[i].rightAnswser + "<br>";
            let txtPopUp = "Tu as commis 1 erreur de justification. Tu aurais d\u00fb justifier comme ceci:";
            Swal.fire({
                width: 500,
                confirmButtonColor: BLEU,
                confirmButtonText: this.txtBtnOK,
                showConfirmButton: true,
                html: '<h4>' + txtPopUp + '</h4>' +
                    '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                    '</div>'
            }).then((result) => {
            });


        } else {
            Swal.fire({
                icon: 'success',
                title: 'Bravo',
                confirmButtonText: "ok",
                showConfirmButton: true,
                text: 'Tu as fait aucune erreur !',
            })

        }

    }


    /**
     * Cette méthode gère les clics qu'on fait sur le menu de gauche du canevas, selon le nombre de fois que le bouton a
     * été clique on affiche certains éléments ou pas
     */
    handleNext() {
        if (this.tutotxtNo === 1) {
            this.refGame.global.util.hideTutorialInputs('next');
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('tuto2'));

            //activer le deplacement pour l'icône "poolLap"
            this.lapPool.setStopMove(false);

            //cacher les icônes qui ont été déjà placées
            this.bicycle.setVisible(false);
            this.restaurant.setVisible(false);
            this.birthday.setVisible(false);
            this.movie.setVisible(false);

            //recréer les icônes mais cette fois dans le bon endroit du tableau
            this.bicycle2.setVisible(true);
            this.restaurant2.setVisible(true);
            this.birthday2.setVisible(true);
            this.movie2.setVisible(true);

            //afficher les images grises
            this.bicycle2Gray.setVisible(true);
            this.restaurant2Gray.setVisible(true);
            this.birthday2Gray.setVisible(true);
            this.movie2Gray.setVisible(true);

            //afficher le carré rouge
            this.rect.visible = true;

            //créer les rectangles verts pour mettre le texte en surbrillance
            this.validate1.visible = true;
            this.validate2.visible = true;
            this.validate3.visible = true;
            this.validate4.visible = true;
            this.validate5.visible = true;
            this.validate6.visible = true;
            this.validate7.visible = true;

            this.tutotxtNo = 2;
        } else if (this.tutotxtNo === 2) {
            //this.popUp(this.refGame.global.resources.getOtherText('titleTest'), "ceci est un test", this.cells[0].getShape());
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getTutorialText('tuto3'));
            //this.refGame.global.util.hideTutorialInputs('next');

            this.rect.visible = false;
            this.tutotxtNo = 3;
        }
    }
    refreshLang(lang) {

    }

    refreshFont(isOpenDyslexic) {

    }
    onShapeClick(shape) {
    }
    onDragEnd() {
    }
    EndGame() {
        this.elements = []
        this.init();
        this.show();
    }
}
class Play extends Interface {

    constructor(refGame) {

        super(refGame, "play");


    }

    show() {
        this.exeSelected = -1;
        this.clear();
        this.exercices = {};
        this.elements = {
            title: new PIXI.Text('', { fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left' }),
            scrollPane: new ScrollPane(0, 60, 600, 480),
            btn: new Button(300, 570, 'Jouer', 0x0088FF, 0x000000, true)
        };
        this.elements.btn.setOnClick(function () {

            if (this.exeSelected != -1) {
                console.log(this.exercice[this.exeSelected])
                this.refGame.trainMode.init(false, 'playMode');
                console.log(this.exercice[this.exeSelected].exercice)
                this.refGame.trainMode.setRefPlay(this);
                this.refGame.trainMode.show(this.exercice[this.exeSelected].exercice);
            }
        }.bind(this));
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 30;
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    init() {
        console.log("init")
        this.refGame.global.util.hideTutorialInputs('btnReset');
        super.init();
    }

    startExercices(lang) {
        //choose exe
        this.exercice = this.refGame.global.resources.getExercicesEleves();
        let tg = new ToggleGroup('single')
        if (this.exercice) {
            this.exercice = JSON.parse(this.exercice);
            console.log(Object.keys(this.exercice))
            console.log(this.exercice)
            for (let exID of Object.keys(this.exercice)) {
                let ex = this.exercice[exID];
                console.log("_------------------------------------_")
                console.log(ex)
                if (ex.exercice) {
                    let label = (ex.name);
                    let btn = new ToggleButton(0, 0, label, false, 580);
                    btn.enable();
                    tg.addButton(btn);
                    btn.setOnClick(function () {
                        this.exeSelected = exID;
                    }.bind(this));
                    this.elements.scrollPane.addElements(btn);
                }

            }
            this.elements.scrollPane.init();
        }

    }



    ///////////////////////
    refreshLang(lang) {
        this.startExercices(lang)
    }

}
const txtGreen = "la case en vert repr\u00e9sente le moment pr\u00e9sent";
class Train extends Interface {

    constructor(refGame) {
        super(refGame, "train");
        this.evaluate = false;
        this.exercice = null;
        //this.show(this.evaluate, this.exercice);
    }

    /**
     * Méthode qui sera appelée pour afficher les éléments dans le canevas
     * @param evaluate boolean pour savoir si on est dans le mode entrainer ou évaluer
     * @param exercice int qui indique l'id de l'exercice à afficher 0x001200
     */
    setRefPlay(refPlay) {
        this.refPlay = refPlay
    }
    show(evaluate, exercice) {
        this.clear();
        this.correctionY = -6;
        this.txtBtnOK = "Ok"
        this.elements = [];
        this.checkError = false;
        this.validatorPopup = false
        if (exercice) {
            this.exercice = exercice;
            this.newGame(-1)
        } else {
            this.refreshLang(this.refGame.global.resources.getLanguage());
            this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('startBtn'), 0x007bff, 0xFFFFFF, true);
            this.startButton.setOnClick(this.newGame.bind(this));
            for (let i = 0; i < this.elements.length; i++) {
                if (typeof this.elements[i].origin !== "undefined") {
                    this.elements[i].reset();
                }
            }
            this.clear();
            this.elements.push(this.startButton);
            this.evaluate = evaluate;
            this.refGame.global.util.hideTutorialInputs('btnResetTr', 'btnAudioGame', 'next', 'btnReset');
            this.init();
        }

    }
    /**
     * Cette méthode va afficher un nouvel exercice
     * @param isReset
     */
    newGame(isReset) {
        this.clear();
        this.imgPositioning = [];
        this.validation = [];
        if (isReset != -1) {
            //MODE TRAIN
            if (forced) {
                console.log("forced : " + forced);
                this.exercice = this.refGame.global.resources.getExerciceById(forced);
            }
            else {
                console.log("Not forced");
                this.exercice = this.refGame.global.resources.getExercice();
            }
            if (this.evaluate) {
                // On initialise la partie évaluation
                this.refGame.global.statistics.addStats(2);
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('evaluateMode'));
            } else {
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('trainMode'));
            }
            this.elements.unshift(this.startButton);
            this.startButton.setVisible(false);
            if (typeof this.exercice == "undefined") {
                this.exercice = this.refGame.global.resources.getExercice();
                this.exercice = JSON.parse(this.exercice.exercice);
            }

            this.refGame.global.util.hideTutorialInputs('btnResetTr');
            //json Sélectionner un exercice sauf celui de base
            //this.exercice = this.refGame.global.resources.getExercice();
            //this.exercice = JSON.parse(this.exercice);
            //this.exercice = this.refGame.global.resources.getExercice();
            //console.log(this.exercice)
            this.exercice = JSON.parse(this.exercice.exercice);
            this.id = this.exercice.id;


        }
        //console.log("__________________________________________________________")
        //console.log(this.exercice)
        //insertion du background
        this.back = this.exercice.difficulty;
        this.backImg = this.refGame.global.resources.getImage(this.back.name).image.src;


        let insererBack = new ImgBack(this.back.locationx, this.back.locationy, this.back.x, this.back.y, this.backImg, 'background');
        this.elements.push(insererBack);

        //insertion des images dans le tableau
        var count = 1;
        for (let key in this.exercice.elementsInTab.sprites) {
            var img = this.exercice.elementsInTab.sprites[count];
            if (typeof this.refGame.global.resources.getImage(img.nomFichier).image.src !== "undefined") {
                let imageTab = new ImgBack(img.x, img.y, img.width, 32, this.refGame.global.resources.getImage(img.nomFichier).image.src, img.nomFichier);
                //let imageTab = new ImgBack(0, 0, img.width, 32, this.refGame.global.resources.getImage(img.nomFichier).image.src, img.nomFichier);
                this.elements.push(imageTab);
            }
            count++;
        }
        count = 1;
        //this.nbFrome = this.exercice.spriteToMove;
        this.nbFrome = 0;
        for (let key in this.exercice.spriteToMove) {
            this.nbFrome++;
        }
        this.cells = [];
        this.listError = [];
        this.nbBouger = 0;
        //insertion des aimants
        for (let key in this.exercice.elementsInTab.magnets) {
            var magnetInTab = this.exercice.elementsInTab.magnets[count];
            let x = magnetInTab.x;
            let y = magnetInTab.y;
            let isPlaced = false;
            let magnet = new Guide(parseInt(x), parseInt(y), 20, magnetInTab.text,);
            this.cells.push(magnet);
            this.elements.push(magnet);
            count++;
        }

        //Insertion des images que l'utilisateur pourra bouger
        let startXgreen;
        let startYgreen;

        if (this.exercice.difficulty.name == DIFFICULTEHAUTE) {
            var startX = 400;
            var startY = 30;
            startXgreen = 400;
            startYgreen = 40;
            this.correctionY = -2;
        } else if (this.exercice.difficulty.name == "grid2weeks") {
            var startX = 50;
            var startY = 430;
            startXgreen = 50;
            startYgreen = 370;
        } else if (this.exercice.difficulty.name == "grid3days") {
            var startX = 50;
            var startY = 350;
            startXgreen = 50;
            startYgreen = 290;
        } else if (this.exercice.difficulty.name == "grid1week") {
            var startX = 50;
            var startY = 350;
            startXgreen = 50;
            startYgreen = 285;
        }

        count = 1
        //affichage du txt explicatif du
        this.textVerte = new PIXI.Text(txtGreen, {
            // fontFamily: 'Arial',
            fontSize: 18,
            fontWeight: "bolder",
            fill: 0x000000,
            align: 'right',
            //wordWrap: true,
            wordWrapWidth: 380
        });

        this.textVerte.x = startXgreen + 50;
        this.textVerte.y = startYgreen - 10;

        let sizeIcon;
        if (this.exercice.difficulty.name == DIFFICULTEHAUTE) {
            sizeIcon = 32;
        } else if (this.exercice.difficulty.name == "grid2weeks") {
            sizeIcon = 37;
        } else if (this.exercice.difficulty.name == "grid3days") {
            sizeIcon = 48;
        } else if (this.exercice.difficulty.name == "grid1week") {
            sizeIcon = 46;
        }

        let imggreen = new Shape(startXgreen, startYgreen, 32, 32, count, this.refGame.global.resources.getImage("greenFace"), "greenFace", idOK, true);
        if (this.exercice.difficulty.name == DIFFICULTEHAUTE) {
            //var info = new InfoImg(550, startY + 8, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            var info = new InfoImg(450, 40, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClickGreen.bind());//,count,this.onShapeClick.bind(this)
            this.elements.push(info);
        } else {
            this.elements.push(this.textVerte);
            var info = new InfoImg(550, startYgreen, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClickGreen.bind());//,count,this.onShapeClick.bind(this)
            this.elements.push(info);
        }


        let nb = 1
        let startX2 = startX
        let startY2 = startY
        this.elements.push(imggreen);

        for (let key in this.exercice.spriteToMove) {
            var txt = this.exercice.spriteToMove[count].texte;
            var img = this.exercice.spriteToMove[count].sprite;
            var i = 1;
            var idOK = 0;
            //Ici on récupère quelle est la bonne réponse, on va utiliser la variable idOk plus tard, l'hors de la validation de l'exercice
            for (let key in this.exercice.spriteToMove[count].popup.answers) {
                if (this.exercice.spriteToMove[count].popup.answers) {
                    if (this.exercice.spriteToMove[count].popup.answers[i].ok) {
                        idOK = i;
                    }
                    i++;
                }
            }
            //insertion des images qui pourront être bougées
            let imageTab;
            let imageTab2;
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                startX2 = startX2 + 60
                imageTab = new Shape(startX2, startY, sizeIcon, sizeIcon, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2 = new Shape(startX2, startY, sizeIcon, sizeIcon, count, this.refGame.global.resources.getImage(img));
                imageTab2.setStopMove(true);

            } else {
                this,
                    imageTab = new Shape(startX2, startY2 + 80, sizeIcon, sizeIcon, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2 = new Shape(startX2, startY2 + 80, sizeIcon, sizeIcon, count, this.refGame.global.resources.getImage(img), img, this.exercice.spriteToMove[count].verif.x, this.exercice.spriteToMove[count].verif.y, idOK, true, this.onShapeClick.bind(this), this.onShapeMove.bind(this), this.onShapeEndMove.bind(this));
                imageTab2.setStopMove(true);
            }
            imageTab2.setAplha(0.3)
            this.elements.push(imageTab2);
            this.elements.push(imageTab);
            //insertion du texte
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                this.texte = new PIXI.Text(txt, {
                    // fontFamily: 'Arial',
                    fontSize: 12,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.texte.x = 100;
                this.texte.y = startY;
                //this.elements.push(this.texte);
            }

            //insertion des "i" a gauche du text / des images
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                var info = new InfoImg(startX2, startY + 30, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            } else {
                var info = new InfoImg(startX2 + 50, startY2 + 80, 16, 16, this.refGame.global.resources.getImage("info").image.src, "info", count, this.onInfoClick.bind(this));//,count,this.onShapeClick.bind(this)
            }
            this.elements.push(info);
            //startY = startY + 40; 
            startY2 = startY2 + 35
            if (this.exercice.difficulty.name != DIFFICULTEHAUTE) {
                if (nb % 8 == 0) {
                    startX2 = startX
                    startY = startY + 70
                }
            } else {
                if (nb % 14 == 0) {
                    startX2 = startX2 + 100
                    startY2 = startY
                }

                //startY = startY + 30
            }
            count++;
            nb++;
        }

        //this.elements.push(texte1);
        this.refGame.global.util.showTutorialInputs('btnValider');
        this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.validateWholeExercice.bind(this));
        //écrire 


        this.init();


    }

    /**
     * Cette méthode sera déclenchée quand on clique sur un des boutons Info, elle va générer une popup avec le texte et
     * une icône d'un hautparleur qui va déclencher le tts
     * @param info objet de l'image qui contient un id permettant d'identifier le texte qu'il faut afficher
     */
    onInfoClickGreen(info) {
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: justify"> ' + txtGreen + ' </div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    /**
     * Cette méthode sera déclenchée quand on clique sur un des boutons Info, elle va générer une popup avec le texte et
     * une icône d'un hautparleur qui va déclencher le tts
     * @param info objet de l'image qui contient un id permettant d'identifier le texte qu'il faut afficher
     */
    onInfoClick(info) {
        let txt;
        if (!this.exercice.spriteToMove[info.getTextId()].text) {
            txt = this.exercice.spriteToMove[info.getTextId()].texte
        } else {
            txt = this.exercice.spriteToMove[info.getTextId()].text
        }

        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td style="padding-right: 30px"><div><img src=' + this.refGame.global.resources.getImage(this.exercice.spriteToMove[info.getTextId()].sprite).image.src + ' alt="' + this.exercice.spriteToMove[info.getTextId()].sprite + '"></div></td>' +
                '<td><div id="activites" style="text-align: justify">' + txt + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    /**
     * Méthode appelée quand l'utilisateur bouge une des images
     * @param shape
     */
    onShapeMove(shape) {
        for (let i = 0; i < this.cells.length; i++) {
            let cell = this.cells[i];
            let minX = cell.x - cell.width / 1.7;
            let maxX = cell.x + cell.width / 1.7;
            let minY = cell.y - cell.width / 1.7;
            let maxY = cell.y + cell.width / 1.7;
            if (cell.getShape() === shape && !(minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY())) {
                this.cells[i].setShape(null);
            }
            if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY() && (this.cells[i].getShape() === shape || !this.cells[i].getShape())) {
                shape.setX(cell.x);
                shape.setY(cell.y);
                this.cells[i].setShape(shape);
            }
        }
    }

    /**
     * Methode appéllée quand l'utilisateur arrete de bouger une image
     * @param shape
     */
    onShapeEndMove(shape) {
        let ok = false;
        if (!shape.getStopMove()) {
            for (let i = 0; i < this.cells.length; i++) {
                let cell = this.cells[i];
                let minX = cell.x - cell.width / 1.7;
                let maxX = cell.x + cell.width / 1.7;
                let minY = cell.y - cell.width / 1.7;
                let maxY = cell.y + cell.width / 1.7;
                if (cell.x == shape.getX() && cell.y == shape.getY()) {
                    ok = true
                }
                if (minX < shape.getX() && maxX > shape.getX() && minY < shape.getY() && maxY > shape.getY()) {
                    shape.setX(cell.x);
                    shape.setY(cell.y);
                    if (this.cells[i].getShape() != null) {
                        this.generateJustificationPopup(this.cells[i], shape.getposition());
                        if (this.cells[i].getShape() !== shape) {
                            this.cells[i].getShape().setX(this.cells[i].getShape().getBaseX());
                            this.cells[i].getShape().setY(this.cells[i].getShape().getBaseY());
                        }
                    }
                    this.cells[i].setShape(shape);
                }

            }
            if (!ok) {
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
                this.elements.push(shape)
                this.init()
            }
        }
    }



    /**
     * Cette methode génére la popup de justification quand elle est appéllée (quand une image a été dépose dans un aimant)
     * @param cell cellule ou l'image a été déposée
     * @param i
     */
    generateJustificationPopup(cell, i) {
        //creation de l'html qui sera mis à l'interieur de la popup
        var shape = cell.getShape();
        let count = 1;
        let htmlToInsertInPopup = "";
        let k = 1
        let sprit = this.exercice.spriteToMove[i];
        for (let key in this.exercice.spriteToMove[i].popup.answers) {
            htmlToInsertInPopup += '<input type="radio" class="radiobtn"id="input_' + count + '" name="validate_' + i +
                '" value="op' + count + '" data-correct="' + this.exercice.spriteToMove[i].popup.answers[k].ok + '">' +
                ' <label htmlFor="op' + count + '" style="padding-left: 3px">';
            let txt = "";
            txt = this.exercice.spriteToMove[i].popup.answers[k].text;
            htmlToInsertInPopup += txt + '</label>';
            htmlToInsertInPopup += '<br>';
            count++;
            k++
        }
        Swal.fire({
            width: 500,
            confirmButtonColor: BLEU,
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            html:
                '<h4>' + 'Tu places ' + this.refGame.global.resources.getOtherText(shape.name) + ' le ' + cell.getDate() + '. Pourquoi ?' + '</h4>' +
                '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                '</div>',
            preConfirm: (login) => {
                //check si les btns radio sont sélectionnés
                let ischeck;
                for (let j = 1; j <= 4; j++) {
                    ischeck = document.getElementById("input_" + j).checked;
                    if (ischeck) {
                        break;
                    }
                }
                if (!ischeck) {
                    Swal.showValidationMessage(`Attention tu n'as rien s\u00e9lectionn\u00e9`);
                    //si aucun btn n'est sélectionné -> popup de Warning
                }
            },
            showConfirmButton: true,
        }).then((result) => {
            if (result.dismiss == 'cancel') {
                //Si l'utilisateur clique sur le bouton cancel on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else if (result.dismiss == 'backdrop') {
                //Si l'utilisateur clique dehors la popup on remet l'image dans son endroit initial.
                shape.setX(shape.getBaseX());
                shape.setY(shape.getBaseY());
            } else {
                //Si l'utilisateur clique sur le bouton valider on test si c'est ok ou pas, et ensuite on bloque l'image
                this.validateImgPositioning(shape.getX(), shape.getY(), shape.getvalidx(), shape.getvalidy(), shape.name);
                //Si l'emplacement est faux ont cree une varible d'erreur qu'on place dans une liste
                this.validateJustification(shape, i, sprit);
                shape.setStopMove(true);
                //ischeck = true;
                //input++;
            }
        });
    }

    /**
     * Méthode qui affiche le bouton de validation.
     */
    onEndGame() {
        let res = false;
        //si le nombre de sprite to move est égale au nombre d'image bougé
        if (this.nbBouger == this.nbFrome) {
            res = true;
        }
        return res;
    }
    /**
     * Méthode qui permet de générer une popup affichant les solutions des erreur de justification.
     * 
     */
    generatePopupErrorJustification() {
        if (this.listError.length > 0) {
            let htmlToInsertInPopup = '';
            let i = 0;
            let nbWrong = 0;
            let that = this;
            for (let key in this.listError) {
                if (this.listError[i].isJustification) {
                    htmlToInsertInPopup += "<p size='3'><img src=" + this.listError[i].img.image.src + " alt='oups....'>" + this.listError[i].rightAnswser + "<br>";
                    nbWrong++;
                }
                i++;
            }
            if (nbWrong > 0) {
                // pour le "s" de erreur
                let txtPopUp = "";
                if (this.listError.length == 1) {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreur de justification. Tu aurais d\u00fb justifier comme ceci:";
                } else {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreurs de justification. Tu aurais d\u00fb justifier comme ceci:";
                }
                Swal.fire({
                    width: 500,
                    confirmButtonColor: BLEU,
                    confirmButtonText: this.txtBtnOK,
                    showConfirmButton: true,
                    html: '<h4>' + txtPopUp + '</h4>' +
                        '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                        '</div>'
                }).then((result) => {
                    this.reStart();
                });
            }

        }

    }
    reStart() {
        if (this.refPlay != null) {
            this.refPlay.show();
        } else {
            this.clear();
            this.elements = [];
            this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('startBtn'), 0x007bff, 0xFFFFFF, true);
            this.startButton.setOnClick(this.newGame.bind(this));
            this.elements.push(this.startButton);
            this.show(this.evaluate);
        }
    }
    /**
     * Méthode qui permet de générer une popup affichant les solutions des erreur de placement.
     * 
     */
    generatePopupErrorPlacement() {
        if (this.listError.length > 0) {
            var that = this;
            let htmlToInsertInPopup = '';
            // i = le nombre d'erreur total
            let i = 0;
            //nbWrong = le nombre d'erreur de placement 
            let nbWrong = 0;
            for (let key in this.listError) {
                if (!this.listError[i].isJustification) {
                    htmlToInsertInPopup += "<p size='3'><img src=" + this.listError[i].img.image.src + " alt='oups....'>" + this.listError[i].rightAnswser + "<br>";
                    //+ une erreur de placement
                    nbWrong++;
                }
                //+ une erreur
                i++;
            }
            //check si il y a des erreur de placement
            if (nbWrong > 0) {
                let txtPopUp = "";
                //pour le "s" de erreur
                if (nbWrong == 1) {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreur de placement. Tu aurais d\u00fb le mettre :";
                } else {
                    txtPopUp = "Tu as commis  " + nbWrong + " erreurs de placement. Tu aurais d\u00fb le mettre : ";
                }
                Swal.fire({
                    width: 500,
                    confirmButtonColor: BLEU,
                    showCancelButton: false,
                    confirmButtonText: this.txtBtnOK,
                    showConfirmButton: true,
                    html: '<h4>' + txtPopUp + '</h4>' +
                        '<div style="text-align: left; margin-top: 5px">' + htmlToInsertInPopup +
                        '</div>'
                }).then((result) => {
                    if (that.validatorPopup) {
                        that.generatePopupErrorJustification();
                    } else {
                        that.reStart()
                    }

                })
            }
        }
    }

    /**
     * Cette méthode nous permet de tester que le positionement de l'image sur le calendrier est correct puis le stocker
     * @param imgX coordonées en x de l'image
     * @param imgY coordonées en y de l'image
     * @param correctX coordonées correctes en x
     * @param corrextY coordonées correctes en y
     */
    validateImgPositioning(imgX, imgY, correctX, corrextY, name) {
        let c
        if (this.exercice.difficulty.name != DIFFICULTEHAUTE) { c = 0 } else { c = 80 }

        let solution;
        let image;
        if (imgX == correctX && imgY == corrextY) {
            this.imgPositioning.push("ok");
            this.checkError = true;
        } else {
            this.imgPositioning.push(imgX + "/" + imgY);
            this.checkError = false;
        }
        this.nbBouger++;
        if (!this.checkError) {
            let i = 1;
            let xJson;
            let yJson;
            for (let key in this.exercice.elementsInTab.magnets) {
                xJson = this.exercice.elementsInTab.magnets[i].x;
                yJson = this.exercice.elementsInTab.magnets[i].y;
                if (xJson != correctX && yJson + c != corrextY) {
                    solution = this.exercice.elementsInTab.magnets[i].text;
                    image = this.getImageForPopUpWithName(name);

                }
                i++;
            }
            this.addErrorToList(false, image, solution, null);
        }
        //this.onEndGame();
    }

    /**
     * Cette méthode nous permet de tester que la justification soie correcte
     * @param correctInput id de l'input qui faut vérifier si à été coche ou pas
     */
    //                this.validateJustification(shape, i, sprit);

    validateJustification(shape, imgPos, sprit) {
        let solution = "";
        //obtention de la solution
        let k = 1;
        for (var key in sprit.popup.answers) {
            if (sprit.popup.answers[k].ok) {
                solution = sprit.popup.answers[k].text;
            }
            k++;
        }
        let img = this.getImageForPopUpWithCount(imgPos);
        var ok = document.getElementById("input_" + shape.getidValidRes()).checked;
        if (ok) {
            this.validation.push("ok" + " " + imgPos);
        } else {
            let count = 1;
            let selectedInput = 0;
            for (var key in this.exercice.spriteToMove[imgPos].popup.answers) {
                if (document.getElementById("input_" + count).checked) {
                    selectedInput = count;
                }
                count++;
            }
            //ImgPos est la position de l'image dans la liste selected input est la justification sélectionnée
            this.validation.push(imgPos + "/" + selectedInput);
            //ajout de la mauvaise réponse dans la liste d'erreur
            this.addErrorToList(true, img, solution, null);
        }

    }
    /**
     * Cette méthode va me permettre valider l'exercice au complet quand on appuie sur le bouton valider à gauche, si on
     * est dans le mode évaluer on va calculer la note et on enverra les résultats de l'exercice à la BD
     */
    validateWholeExercice() {
        if (this.onEndGame()) {
            var that = this;
            var htmlToPutInTheErrorPopup = [];
            //si tous les éléments ont été placés on teste si sont tous ok ou pas, si ils sont ok on les compte
            var count = 0;
            var countValidation = 0;
            var countImgPositioning = 0;
            var reponseEleve = "";
            for (var key in this.validation) {
                //ici on récupère les résultats de l'élève et on génère le texte qui sera mis dans la BD au mode évaluer
                var ligneCourrante = "";
                if (this.validation[count].substr(0, 2) == "ok") {
                    countValidation++;
                    var elmsOK = this.validation[count].split(" ");
                    ligneCourrante += this.exercice.spriteToMove[elmsOK[1]].sprite + ":" + "Val=OK//";
                } else {
                    var elmsKO = this.validation[count].split("/");
                    let i = 1;
                    let juste = "";
                    //récuperer la bonne réponse
                    while (this.exercice.spriteToMove[elmsKO[0]].popup.answers[i].ok != true) {
                        i++
                    }
                    juste = this.exercice.spriteToMove[elmsKO[0]].popup.answers[i].text;

                    ligneCourrante += this.exercice.spriteToMove[elmsKO[0]].sprite + ":";
                    if (elmsKO[1] == 0) {
                        ligneCourrante += "Val=KO-//";
                        htmlToPutInTheErrorPopup.push("<p>" + this.refGame.global.resources.getOtherText('selected') + ": <span style='color: red'>" + this.refGame.global.resources.getOtherText('noResponse') + "</span></p>");
                    } else {
                        ligneCourrante += "Val=KO-" + elmsKO[1] + "//";
                        htmlToPutInTheErrorPopup.push("<p>" + this.refGame.global.resources.getOtherText('selected') + ": <span style='color: red'>" + this.exercice.spriteToMove[elmsKO[0]].popup.answers[elmsKO[1]].text + "</span></p>");
                    }
                    htmlToPutInTheErrorPopup.push("<p>Juste: <span style='color: green'>" + juste + "</span></p>");
                }
                if (this.imgPositioning[count] == "ok") {
                    countImgPositioning++;
                    ligneCourrante += "Pos=OK;";
                } else {
                    ligneCourrante += "Pos=KO;";
                }
                reponseEleve += ligneCourrante;
                count++;
            }
            //si c'est le mode évaluer on teste les erreurs, on donne une note et on ajoute le resumé de l'exercice
            if (this.evaluate) {
                let pointsExe = -1;
                if (countImgPositioning == count && countValidation == count){
                    pointsExe = 2;
                } else if ((countImgPositioning + countValidation) >= count) { // On teste directement sur la variable count car c'est déjà le 50% des points faisables en tout
                    pointsExe = 1;
                } else {
                    pointsExe = 0;
                }
                this.refGame.global.statistics.addStats(pointsExe);
                let resultSend = pointsExe + "," + reponseEleve + "," + this.id;
                this.refGame.global.statistics.updateStats(pointsExe);
                // this.refGame.global.statistics.updateStats(resultSend);
                if (pointsExe === 2) {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('allGood'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'success',
                        showConfirmButton: true
                    });
                } else {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('errorsMade'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'error',
                        showConfirmButton: true
                    });
                }
                this.clear();
                this.elements = [];
                this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('startBtn'), 0x007bff, 0xFFFFFF, true);
                this.startButton.setOnClick(this.newGame.bind(this));
                this.elements.push(this.startButton);
                this.show(this.evaluate);
            } else {

                // si on a 5 pour la validation et pour le positionning on affiche une popup qui indique que tout est juste
                // sinon on affiche les erreurs
                //if (countValidation == 5 && countImgPositioning == 5) {
                if (this.listError.length == 0) {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('allGood'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'success',
                        showConfirmButton: true
                    }).then((result) => {
                        that.generatePopupErrorPlacement();
                        this.clear();
                        this.elements = [];
                        this.startButton = new Button(300, 300, this.refGame.global.resources.getOtherText('startBtn'), 0x007bff, 0xFFFFFF, true);
                        this.startButton.setOnClick(this.newGame.bind(this));
                        this.elements.push(this.startButton);
                        this.reStart();
                        // this.show(false, null);
                    });
                } else {
                    Swal.fire({
                        title: this.refGame.global.resources.getOtherText('errorsMade'),
                        width: 500,
                        confirmButtonColor: BLEU,
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        icon: 'error',
                        showConfirmButton: true,
                    }).then((result) => {
                        that.generatePopupErrorPlacement();
                        //  this.refGame.global.util.showTutorialInputs('btnResetTr');
                        // this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.resetExercice.bind(this));

                    });
                }
                /*
*/
            }
        } else {
            swal.fire({
                title: "Tu n'as pas fini !",
                text: "Tu pourras valider ton exercice lorsque tu auras plac\u00e9 toutes les images ;)",
                icon: "warning",
            });
        }

    }

    /**
     * cette méthode va ajouter une variable d'erreur a la liste.
     * @param   justification Boolean qui nous indique si l'erreur est une justification ou un placement
     * @param   img l'image de l'activité
     * @param   solution String qui nous donne la solution
     */
    addErrorToList(justification, img, solution, rep) {
        if (justification) {
            this.validatorPopup = justification
        }
        var error = {
            isJustification: justification,
            falseAnswser: rep,
            img: img,
            rightAnswser: solution
        };
        //ajout à la liste 
        this.listError.push(error);

    }

    /**
     * Méthode qui se charge de récupérer de l'image grace au numéro du "priteToMove"
     * @param count le numéro du "spriteToMove" ou il faut aller chercher le nom
     * @returns une image sous format "base64"
     */
    getImageForPopUpWithCount(count) {
        let img = this.exercice.spriteToMove[count].sprite;
        return this.getImageForPopUpWithName(img);
    }
    /**
     *  Méthode pour obtenir l'image avec le nom du "sprite"
     * @param name le nom du sprite
     * @returns une image sous format "base64"
     */
    getImageForPopUpWithName(name) {
        return this.refGame.global.resources.getImage(name);
    }

    /**
     * Cette méthode sere appelée quand l'utilisateur aura appuie sur le bouton "btnResetTr" elle va récharger le même
     * exercice.
     */
    resetExercice() {
        this.clear();
        this.elements = [];
        this.newGame(this.id);
        // this.listError = [];
    }
    //    refreshLang(lang) {
    //    }
    refreshFont(isOpenDyslexic) {
    }
    onShapeClick(shape) {
    }
    onDragEnd() {
    }
}
/**
 * @classdesc Représente un mode de jeu
 * @author Vincent Audergon
 * @version 1.0
 */
class Mode {

    /**
     * Constructeur d'un mode de jeu
     * @param {string} name le nom du mode de jeu (référencé dans le JSON de langues)
     */
    constructor(name) {
        this.name = name;
        this.interfaces = [];
        this.texts = [];
    }

    /**
     * Défini la liste des interfaces utilisées par le mode de jeu
     * @param {Interface[]} interfaces la liste des interfaces
     */
    setInterfaces(interfaces) {
        this.interfaces = interfaces;
    }

    /**
     * Ajoute une interface au mode de jeu
     * @param {string} name le nom de l'interface
     * @param {Interface} inter l'interface
     */
    addInterface(name, inter) {
        this.interfaces[name] = inter;
    }

    /**
     * Retourne un texte dans la langue courrante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key){
        return this.texts[key];
    }

    /**
     * Méthode de callback appelée lors d'un changement de langue.
     * Indique à toutes les interfaces du mode de jeu de changer les textes en fonction de la nouvelle langue
     * @param {string} lang la langue
     */
    onLanguageChanged(lang) {
        this.texts = this.refGame.global.resources.getOtherText(this.name)
        for (let i in this.interfaces) {
            let inter = this.interfaces[i];
            if (inter instanceof Interface) {
                inter.setTexts(this.texts);
                inter.refreshLang(lang);
            }
        }
    }

}

class CreateMode extends Mode{

    constructor(refGame){
        super('create');
        this.refGame = refGame;
        this.setInterfaces({
            create: new Create(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.create.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.create.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.create.refreshFont(isOpendyslexic)
    }
}

class ExploreMode extends Mode{

    constructor(refGame){
        super('explore');
        this.refGame = refGame;
        this.setInterfaces({
            explore: new Explore(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.explore.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.explore.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.explore.refreshFont(isOpendyslexic)
    }
}

class PlayMode extends Mode{

    constructor(refGame){
        super('play');
        this.refGame = refGame;
        this.setInterfaces({
            play: new Play(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.play.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.play.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.play.refreshFont(isOpendyslexic)
    }
}

class TrainMode extends Mode{

    constructor(refGame){
        super('train');
        this.refGame = refGame;
        this.evaluate = false;
        this.setInterfaces({
            train: new Train(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(evaluate){
        this.evaluate = evaluate;
    }

    /**
     * Affiche la page de garde de la création
     */
    show(exercice = undefined){
        this.interfaces.train.show(this.evaluate,exercice);
    }

    setRefPlay(refPlay){
        this.interfaces.train.setRefPlay(refPlay);
    }
    
    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.train.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.train.refreshFont(isOpendyslexic)
    }
}
/**
 * @classdesc Classe principale du jeu
 * @author Vincent Audergon
 * @version 1.0
 */

const VERSION = 1.0;

class Game {

    /**
     * Constructeur du jeu
     * @param {Object} global Un objet contenant les différents objets du Framework
     */
    constructor(global) {
        this.global = global;
        this.global.util.callOnGamemodeChange(this.onGamemodeChanged.bind(this));
        this.global.resources.callOnLanguageChange(this.onLanguageChanged.bind(this));
        this.global.resources.callOnFontChange(this.onFontChange.bind(this));
        this.scenes = {
            explore:new PIXI.Container(),
            train:new PIXI.Container(),
            create:new PIXI.Container(),
            play:new PIXI.Container()
        };
        this.exploreMode = new ExploreMode(this);
        this.trainMode = new TrainMode(this);
        this.createMode = new CreateMode(this);
        this.playMode = new PlayMode(this);
        this.oldGamemode = undefined;
    }

    /**
     * Affiche une scène sur le canvas
     * @param {PIXI.Container} scene La scène à afficher
     */
    showScene(scene) {
        this.reset();
        this.global.pixiApp.stage.addChild(scene);
    }

    /**
     * Retire toutes les scènes du stage PIXI
     */
    reset() {
        for (let scene in this.scenes) {
            this.global.pixiApp.stage.removeChild(this.scenes[scene])
        }
    }

    /**
     * Affiche du texte sur la page (à gauche du canvas)
     * @param {string} text Le texte à afficher
     */
    showText(text) {
        this.global.util.setTutorialText(text);
    }

    /**
     * Fonction de callback appelée lors d'un chagement de langue.
     * Indiques à tous les modes de jeu le changement de langue
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.exploreMode.onLanguageChanged(lang);
        this.trainMode.onLanguageChanged(lang);
        this.createMode.onLanguageChanged(lang);
        this.playMode.onLanguageChanged(lang);
    }

    onFontChange(isOpendyslexic) {
        this.createMode.onFontChange(isOpendyslexic);
        this.exploreMode.onFontChange(isOpendyslexic);
        this.trainMode.onFontChange(isOpendyslexic);
        this.playMode.onFontChange(isOpendyslexic);
    }

    /**
     * Listener appelé par le Framework lorsque le mode de jeu change.
     * Démarre le bon mode de jeu en fonction de celui qui est choisi
     * @async
     */
    async onGamemodeChanged() {
        let gamemode = this.global.util.getGamemode();
        switch (gamemode) {
            case this.global.Gamemode.Evaluate:
                this.trainMode.init(true);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Train:
                this.trainMode.init(false);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Explore:
                this.exploreMode.init();
                this.exploreMode.show();
                break;
            case this.global.Gamemode.Create:
                this.createMode.init();
                this.createMode.show();
                break;
            case this.global.Gamemode.Play:
                this.playMode.init();
                this.playMode.show();
                break;
        }
        this.oldGamemode = gamemode;
    }
}
const global = {};
global.canvas = Canvas.getInstance();
global.Log = Log;
global.util = Util.getInstance();
global.pixiApp = new PixiFramework().getApp();
global.resources = Resources;
global.statistics = Statistics;
global.Gamemode = Gamemode;
global.listenerManager = ListenerManager.getInstance();
const game = new Game(global);
