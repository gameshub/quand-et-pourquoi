define(["require", "exports"], function (require, exports) {
    "use strict";
    var Canvas = (function () {
        function Canvas() {
            this.canvas = document.getElementById('canvas');
            this.CTX = this.canvas.getContext('2d');
            this.WIDTH = this.canvas.width;
            this.HEIGHT = this.canvas.height;
            this.OFFSETLEFT = this.canvas.offsetLeft;
            this.OFFSETTOP = this.canvas.offsetTop;
        }
        Canvas.getInstance = function () {
            if (!this.instance) {
                this.instance = new Canvas();
            }
            return this.instance;
        };
        return Canvas;
    }());
    return Canvas;
});
